import React, { useState } from "react";
import { Modal, ModalBody, ModalFooter } from "../components/modal";
import { CSSTransitionGroup } from 'react-transition-group';
import Button from "../components/button";
import CardList from '../components/cardList';
import { FiFilter, FiArrowUpCircle } from 'react-icons/fi';
import List from '../services/list';

export default function Home(props) {
    const times = ['Under 15 minutes', '15-45 minutes', '45 minutes & more'];
    const diff_lev = ['Easy', 'Intermediate', 'expert'];
    List.forEach((item, index) => {
        item.prep_time = times[Math.floor(Math.random() * 3)];
        item.cook_time = times[Math.floor(Math.random() * 3)];
        item.difficulty = diff_lev[Math.floor(Math.random() * 3)];;
    })
    const [list, setList] = useState(List);
    const [showFilters, setShowFilters] = useState(false);
    console.log(List);
    return (
        <>
            <div id="instagram-feed">
                {list && list.map((item, index) => {
                    return <CardList key={index} index={index} data={item} />;
                })}
            </div>
            <div className='bottom-nav-holder'>
                <div className='bottom-nav-item' onClick={() => setShowFilters(!showFilters)} >
                    <FiFilter />
                    <p>Filters</p>
                </div>
                <div className='bottom-nav-item' onClick={() => setShowFilters(!showFilters)} >
                    <FiArrowUpCircle />
                    <p>Sort</p>
                </div>
            </div>
            <CSSTransitionGroup transitionAppear={true} transitionAppearTimeout={500} transitionName="dialog" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
                <Modal isVisible={showFilters} style={showFilters ? { height: 300, backgroundColor: '#cccccc' } : {}}>
                    <ModalBody>
                        <div style={{ display: 'inline-block', padding: 10 }}>
                            <label style={{ marginRight: 10 }} htmlFor="prepTime">Preperation Time</label>
                            <select id="prepTime">
                                <option value=""></option>
                                <option value="Under 15 minutes">Under 15 minutes</option>
                                <option value="15-45 minutes">15-45 minutes</option>
                                <option value="45 minutes & more">45 minutes & more</option>
                            </select>
                        </div>
                        <div style={{ display: 'inline-block', padding: 10 }}>
                            <label style={{ marginRight: 10 }} htmlFor="cookTime">Cooking Time</label>
                            <select id="cookTime">
                                <option value=""></option>
                                <option value="Under 15 minutes">Under 15 minutes</option>
                                <option value="15-45 minutes">15-45 minutes</option>
                                <option value="45 minutes & more">45 minutes & more</option>
                            </select>
                        </div>
                        <div style={{ display: 'inline-block', padding: 10 }}>
                            <label style={{ marginRight: 10 }} htmlFor="diffLevel">Difficulty Level</label>
                            <select id="diffLevel">
                                <option value=""></option>
                                <option value="Easy">Easy</option>
                                <option value="Intermediate">Intermediate</option>
                                <option value="Expert">Expert</option>
                            </select>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <div style={{ display: 'inline-block', padding: 10 }}>
                            <Button title="Apply" style={{ fontSize: '0.8em', display: 'inline-block', margin: 10, backgroundColor: '#7b67d6e6', border: 'none' }} />
                            <Button title="Clear" style={{ fontSize: '0.8em', display: 'inline-block', margin: 10, backgroundColor: '#ccc', color: 'black', border: 'none' }} />
                        </div>
                    </ModalFooter>
                </Modal>
            </CSSTransitionGroup>
        </>
    );
}