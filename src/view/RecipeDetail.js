import React from 'react';
import { RiArrowGoBackLine } from 'react-icons/ri';
// import "../App.css";

class RecipeDetail extends React.Component {
    state = {
        details: {}
    };
    componentDidMount() {
        const data = JSON.parse(localStorage.getItem('data'));
        this.setState({
            details: data
        });
    }
    render() {
        console.log(this.state.details);
        return (
            <div style={{ position: 'relative' }}>
                <header className="header">
                    <RiArrowGoBackLine onClick={() => this.props.history.push('/')} style={{ float: 'left', padding: 10, cursor: 'pointer', marginTop: 5 }} size={30} />
                    <h3 style={{ textAlign: 'center', margin: '0 auto', padding: 20, textDecoration: 'underline', fontSize: 25 }}>
                        {this.state.details.name ? this.state.details.name : ''}
                    </h3>
                </header>
                <div style={{ textAlign: 'center', maxWidth: 600, margin: '0 auto', marginTop: 100 }}>
                    <div style={{ display: 'inline-block', boxShadow: '#b5b5b5 4px 8px 6px 0px' }}>
                        <img src={this.state.details.thumbnail_url} style={{ width: '100%' }} />
                    </div>
                    <div>
                        {/* <p>{this.state.details}</p> */}
                    </div>
                    <div>
                        <strong>
                            <p style={{ textAlign: 'left' }}>INGREDIENTS :</p>
                        </strong>
                        {(this.state.details && this.state.details.sections.length > 0) ?
                            this.state.details.sections.map((item, index) => {
                                return (
                                    <div key={index}>
                                        <h5 style={{ textAlign: 'left' }}>{index + 1}) {item.name}</h5>
                                        {item.components.map((item2, index2) => {
                                            return (
                                                <label key={index2} style={{ display: 'inline-block', margin: 10, backgroundColor: 'grey', padding: 5, color: 'white', borderRadius: 5 }}>
                                                    {item2.raw_text}
                                                </label>
                                            );
                                        })}
                                    </div>
                                );
                            }) : null}
                    </div>
                    <h3 style={{ textAlign: 'left' }}>METHOD:</h3>
                    <div>
                        <ol style={{ textAlign: 'left', lineHeight: 2 }}>
                            {this.state.details.instructions.map((item, index) => {
                                return (
                                    <li key={index}>
                                        {item.display_text}
                                    </li>
                                );
                            })}
                        </ol>
                    </div>
                </div>
            </div>
        );
    }
}

export default RecipeDetail;