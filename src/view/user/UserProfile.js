import React from 'react';
import { RiUserSettingsLine } from 'react-icons/ri';

const UserProfile = () => {
    return (
        <div className='container d-f f-c'>
            <div className='d-f f-j-c f-ai-c' style={{ height: 80, width: 80, borderRadius: 80, border: '1px solid grey' }}>
                <RiUserSettingsLine size={50} />
            </div>
            <div className='d-f f-j-c f-c user-info-list'>
                <p className='user-info-list-item'>
                    <label><strong>Full Name: </strong></label><span>Robin Vashisht</span>
                </p>
                <p className='user-info-list-item'>
                    <label><strong>Email-ID: </strong></label><span>vashisht88robin@gmail.com</span>
                </p>
                <p className='user-info-list-item'>
                    <label><strong>Phone No: </strong></label><span>9930206758</span>
                </p>
            </div>
        </div>
    );
}

export default UserProfile;