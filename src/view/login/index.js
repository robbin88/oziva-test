import { initializeApp } from 'firebase/app';
import { getAuth, signInWithEmailAndPassword, signInWithPopup, GoogleAuthProvider } from "firebase/auth";
import { useEffect, useState } from 'react';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBcFEDC2njM3e-sGygJ061GQ6FcFZrBMNk",
    authDomain: "recipe-378919.firebaseapp.com",
    projectId: "recipe-378919",
    storageBucket: "recipe-378919.appspot.com",
    messagingSenderId: "716557337069",
    appId: "1:716557337069:web:23d5ec931d473e18bee7e4",
    measurementId: "G-PVVP3DQCW0"
};

const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

const provider = new GoogleAuthProvider();


const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLoginClick = () => {
        console.log("Hellooo", auth);
        // signInWithEmailAndPassword(auth, email, password).then((userCredential) => {
        //     // Signed in 
        //     console.log(userCredential);
        //     const user = userCredential.user;
        //     // ...
        // })
        // .catch((error) => {
        //     const errorCode = error.code;
        //     const errorMessage = error.message;
        // });


        signInWithPopup(auth, provider).then((result) => {
            // This gives you a Google Access Token. You can use it to access the Google API.
            console.log(result, "Results")
            const credential = GoogleAuthProvider.credentialFromResult(result);
            const token = credential.accessToken;
            // The signed-in user info.
            const user = result.user;
            // IdP data available using getAdditionalUserInfo(result)
            // ...
        }).catch((error) => {
            // Handle Errors here.
            console.log(error, "Error");
            const errorCode = error.code;
            const errorMessage = error.message;
            // The email of the user's account used.
            const email = error.customData.email;
            // The AuthCredential type that was used.
            const credential = GoogleAuthProvider.credentialFromError(error);
            // ...
        });
    }
    return(
        <div>
            Login
            <button onClick={() => handleLoginClick()}>
                LOGIN
            </button>
        </div>
    );
}

export default Login;