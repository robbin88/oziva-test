import React from 'react';
import './input.css';

export default function Input(props) {
    return(
        <div style={props.contentcontainer} className="input-box">
            <input className="input" {...props}/>
        </div>
    );
}