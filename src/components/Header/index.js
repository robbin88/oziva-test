import React, { useState } from "react";
import { FiSearch } from 'react-icons/fi';
import { HamburgerMenuIcon } from '@radix-ui/react-icons';
import Input from "../input";
import { Link } from 'react-router-dom';
import {
	BrowserRouter as Router,
	Switch,
	Route
} from "react-router-dom";
import RecipeDetail from "../../view/RecipeDetail";
import RecipiesList from "../../view/RecipiesList";
import UserFavourites from "../../view/user/UserFavourites";
import UserProfile from "../../view/user/UserProfile";
import Login from "../../view/login/";
import Home from "../../view/Home";

export default function HeaderNav(props) {
	const [sideNav, setSideNav] = useState(false);
	const [searchVisible, setSearchVisible] = useState(false);

	return (
		<>
			<Router>
				<header className="header">
					<div className='header-menu-list'>
						<div className="ham-icon" style={{ padding: 10 }} onClick={() => {
							setSideNav(!sideNav);
							props.showOverLay(!sideNav);
						}}>
							<HamburgerMenuIcon />
						</div>
						<h3 className="top-header-logo" style={{ margin: '0 auto' }}>
						<Link onClick={() => {
									setSideNav(false)
									props.showOverLay(false);
								}} to={'/'}>
							RECIPE
						</Link>
						</h3>
					</div>
					<div className='sidenav header-menu-list' style={sideNav ? { left: '0' } : { left: '-200px' }}>
						<div className='header-menu-list search'>
							<FiSearch size={25} onClick={() => setSearchVisible(!searchVisible)} style={{ lineHeight: 4, cursor: 'pointer', verticalAlign: 'middle', paddingRight: 10 }} />
							{searchVisible ?
								<div style={{ display: 'inline-block' }}>
									<Input placeholder="Enter Recipe Name" />
								</div>
								: null}
						</div>
						<ul style={sideNav ? { boxShadow: '5px 10px 7px 0px #585858a6' } : { boxShadow: 'none' }}>
							<li className="header-nav">
								<Link onClick={() => {
									setSideNav(false)
									props.showOverLay(false);
								}} to={'/profile'}>
									Account
								</Link>
							</li>
							<li className="header-nav">
								<Link onClick={() => {
									setSideNav(false)
									props.showOverLay(false);
								}} to={'/favourites'}>
									Favourites
								</Link>
							</li>
							<li className="header-nav">
								<Link onClick={() => {
									setSideNav(false)
									props.showOverLay(false);
								}} to={'/recipes'}>
									My Recipies
								</Link>
							</li>
							<li className="header-nav">
								<Link onClick={() => {
									setSideNav(false)
									props.showOverLay(false);
								}} to={'/login'}>
									Login/Logout
								</Link>
							</li>
						</ul>
					</div>
				</header>
				<main className="container">
					<Switch>
						{/* <Route location={'/recipes/:id'} component={RecipeDetail} /> */}
						<Route path={'/recipes'} component={RecipiesList} />
						<Route path={'/favourites'} component={UserFavourites} />
						<Route path={'/profile'} component={UserProfile} />
						<Route path={'/login'} component={Login} />
						<Route path={'/'} component={Home} />
					</Switch>
				</main>
			</Router>

		</>

	);
}