import React from 'react';
import { IoIosAddCircleOutline } from "react-icons/io";
import Input from "./input";
import Button from "./button";
import './dialog.css';

class Dialog extends React.Component {
    state = {
        name: '',
        image: null,
        thumbnail_url: '',
        sections: [{components: []}],
        ingredients: [{raw_text: ''}],
        instructions: [{display_text: ''}],
        prep_time: '',
        cooking_time: '',
        is_shoppable: true,
        difficulty_level: ''
    };
    handleNewIngredient = () => {
        let old_ingredients = this.state.ingredients;
        let new_one = {raw_text: ''};
        old_ingredients.push(new_one);
        this.setState({
            ingredients: old_ingredients
        });
    }
    handleMethodAddition = () => {
        let prev_method = this.state.instructions;
        let new_one = {display_text: ''};
        prev_method.push(new_one);
        this.setState({
            instructions: prev_method
        });
    }
    handleIngredientName = (e, data) => {
        let value = e.target.value;
        let list = this.state.ingredients;
        list[data]['raw_text'] = value;
        this.setState({
            ingredients: list
        });
    }
    handleInstructions = (e, data) => {
        let value = e.target.value;
        let list = this.state.instructions;
        list[data]['display_text'] = value;
        this.setState({
            instructions: list
        });
    }
    render() {
        return(
            <div className="dialog-wrapper">
                <div className="dialog-content">
                    <div className="dialog-header">
                        <h4>ADD/EDIT NEW RECIPE</h4>
                    </div>
                    <div className="dialog-body">
                        <div style={{padding: 10}}>
                            <Input onChange={(name) => this.setState({name: name.target.value})} placeholder="Enter Recipe Name"/>
                        </div>
                        <div style={{padding: 10}}>
                            <Input onChange={(image) => this.setState({image: image.target.files[0], thumbnail_url: URL.createObjectURL(image.target.files[0])})} type="file" placeholder="Select File"/>
                        </div>
                        <div style={{textAlign: 'left'}}>
                            <h4 style={{padding: 0}}>Ingredients</h4>
                            {this.state.ingredients.map((item, index) => {
                                return(
                                    <div key={index} style={{padding: 10, display: 'inline-block'}}>
                                        <Input value={item.raw_text} contentcontainer={{width: 'auto'}} onChange={(e) => this.handleIngredientName(e, index)} placeholder="Ingredients Name"/>
                                    </div>
                                );
                            })}
                            <IoIosAddCircleOutline onClick={this.handleNewIngredient} style={{verticalAlign: 'middle', cursor: 'pointer'}}/>
                        </div>
                        <div style={{textAlign: 'left'}}>
                            <h4 style={{padding: 0}}>Method</h4>
                            {this.state.instructions.map((item, index) => {
                                return(
                                    <div key={index} style={{padding: 10, display: 'inline-block'}}>
                                        <Input value={item.display_text} onChange={(e) => this.handleInstructions(e, index)} placeholder="Method to follow"/>
                                    </div>
                                );
                            })}
                            <IoIosAddCircleOutline onClick={this.handleMethodAddition} style={{verticalAlign: 'middle', cursor: 'pointer'}}/>
                        </div>
                        <div style={{padding: 10}}>
                            <Input onChange={(prep_time) => this.setState({prep_time: prep_time.target.value})} placeholder="Prepration Time"/>
                        </div>
                        <div style={{padding: 10}}>
                            <Input onChange={(cooking_time) => this.setState({cooking_time: cooking_time.target.value})} placeholder="Cooking Time"/>
                        </div>
                        <div style={{padding: 10}}>
                            <Input onChange={(difficulty_level) => this.setState({difficulty_level: difficulty_level.target.value})} placeholder="Difficulty Level"/>
                        </div>
                        <div className="dialog-footer">
                            <Button onClick={() => this.props.addRecipe(this.state)} title="ADD RECIPE" style={{display: 'inline-block', backgroundColor: '#7845d6', border: 'none', fontSize: '0.9em', width: 180, margin: 10}}/>
                            <Button onClick={() => this.props.closeModal()} title="CLOSE" style={{display: 'inline-block', backgroundColor: '#777777', border: 'none', fontSize: '0.9em', width: 180, margin: 10}}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Dialog;