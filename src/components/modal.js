import React from 'react';
import './modal.css';


const Modal = (props) => {
    return (
        <div className='modal-container' style={props.isVisible ? { top: '10px' } : { top: '150%' }}>
            <div className='modal-content'>

                {props.children}
            </div>
        </div>
    );
}
const ModalHeader = (props) => {
    return (
        <div className='modal-header'>
            {props.children}
        </div>
    );
}
const ModalFooter = (props) => {
    return (
        <div className='modal-footer'>
            {props.children}
        </div>
    );
}
const ModalBody = (props) => {
    return (
        <div className='modal-body'>
            {props.children}
        </div>
    );
}

export { Modal, ModalHeader, ModalBody, ModalFooter };