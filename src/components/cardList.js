import React from 'react';
import './card.css';


export default class CardList extends React.Component {
    componentWillMount() {
        let data = this.props.data;
        this.setState({
            data: data
        });
    }
    render() {
        let { data } = this.state;
        if (!data.is_shoppable) {
            data.recipes.map((item, index) => {
                return (
                    <>
                        <div className="instagram-post" {...this.props} onClick={() => {
                            localStorage.setItem('data', JSON.stringify(item));
                            this.props.history.push('/recipe/' + item.slug);
                        }}>
                            <img src={item.thumbnail_url} alt="Some Thing about it" />
                            <div onClick={() => this.props.removeThis(index)} className="card-remove">X</div>
                            <div className="post-details">
                                <p className="post-username">{item.name}</p>
                                <div className="post-caption">
                                    <p style={{ fontSize: '0.8em' }}>
                                        <strong>Prep Time:</strong> {item.prep_time}
                                    </p>
                                    <p style={{ fontSize: '0.8em' }}>
                                        <strong>Cook Time:</strong> {item.cook_time}
                                    </p>
                                    <p style={{ fontSize: '0.8em' }}>
                                        <strong>Difficulty:</strong> {item.difficulty}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </>
                );
            });
        } else {
            return (
                <div className="instagram-post" {...this.props} onClick={() => {
                    localStorage.setItem('data', JSON.stringify(data));
                    this.props.history.push('/recipe/' + data.slug);
                }}>
                    <img src={data.thumbnail_url} alt="Some Thing about it" />
                    <div className="card-remove">X</div>
                    <div onClick={() => this.props.openDetail(data)} className="post-details">
                        <p className="post-username">{data.name}</p>
                        <div className="post-caption">
                            <p style={{ fontSize: '0.8em' }}>
                                <strong>Prep Time:</strong> {data.prep_time}
                            </p>
                            <p style={{ fontSize: '0.8em' }}>
                                <strong>Cook Time:</strong> {data.cook_time}
                            </p>
                            <p style={{ fontSize: '0.8em' }}>
                                <strong>Difficulty:</strong> {data.difficulty}
                            </p>
                        </div>
                    </div>
                </div>
            );
        }
        return '';
    }
}