const List = [
    {
        "cook_time_minutes": null,
        "inspired_by_url": null,
        "language": "eng",
        "promotion": "full",
        "total_time_minutes": null,
        "compilations": [],
        "video_ad_content": null,
        "keywords": "",
        "renditions": [],
        "is_shoppable": true,
        "num_servings": 3,
        "prep_time_minutes": null,
        "thumbnail_url": "https://img.buzzfeed.com/tasty-app-user-assets-prod-us-east-1/recipes/a4e58bb16b1c4246a76d3462b1df253d.jpeg",
        "video_id": null,
        "facebook_posts": [],
        "draft_status": "published",
        "name": "Easy Sweet & Spicy Air Fryer Salmon",
        "show_id": 17,
        "slug": "easy-sweet-spicy-air-fryer-salmon",
        "updated_at": 1591206906,
        "canonical_id": "recipe:6229",
        "buzz_id": null,
        "servings_noun_plural": "servings",
        "tags": [
            {
                "display_name": "Healthy",
                "type": "dietary",
                "id": 64466,
                "name": "healthy"
            },
            {
                "id": 3801552,
                "name": "pescatarian",
                "display_name": "Pescatarian",
                "type": "dietary"
            },
            {
                "type": "dietary",
                "id": 64467,
                "name": "low_carb",
                "display_name": "Low-Carb"
            },
            {
                "id": 64465,
                "name": "gluten_free",
                "display_name": "Gluten-Free",
                "type": "dietary"
            },
            {
                "id": 64471,
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty"
            },
            {
                "type": "difficulty",
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes"
            },
            {
                "id": 64486,
                "name": "dinner",
                "display_name": "Dinner",
                "type": "meal"
            },
            {
                "display_name": "Weeknight",
                "type": "occasion",
                "id": 64505,
                "name": "weeknight"
            }
        ],
        "original_video_url": null,
        "approved_at": 1591206905,
        "sections": [
            {
                "components": [
                    {
                        "position": 1,
                        "measurements": [
                            {
                                "unit": {
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": ""
                                },
                                "id": 568736,
                                "quantity": "3"
                            }
                        ],
                        "ingredient": {
                            "name": "salmon fillet",
                            "display_singular": "salmon fillet",
                            "display_plural": "salmon fillets",
                            "created_at": 1496670664,
                            "updated_at": 1509035197,
                            "id": 1282
                        },
                        "id": 68488,
                        "raw_text": "3 1-inch thick salmon fillets",
                        "extra_comment": "1 in (2.5 cm) thick"
                    },
                    {
                        "ingredient": {
                            "id": 184,
                            "name": "coriander",
                            "display_singular": "coriander",
                            "display_plural": "corianders",
                            "created_at": 1494120557,
                            "updated_at": 1509035281
                        },
                        "id": 68489,
                        "raw_text": "1 1/2 tsp coriander",
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "id": 568733,
                                "quantity": "1 ½",
                                "unit": {
                                    "system": "imperial",
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons"
                                }
                            }
                        ]
                    },
                    {
                        "measurements": [
                            {
                                "quantity": "½",
                                "unit": {
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons",
                                    "system": "imperial"
                                },
                                "id": 568731
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035250,
                            "id": 571,
                            "name": "turmeric",
                            "display_singular": "turmeric",
                            "display_plural": "turmerics",
                            "created_at": 1495313837
                        },
                        "id": 68490,
                        "raw_text": "1/2 tsp turmeric",
                        "extra_comment": "",
                        "position": 3
                    },
                    {
                        "position": 4,
                        "measurements": [
                            {
                                "id": 568738,
                                "quantity": "½",
                                "unit": {
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons",
                                    "system": "imperial",
                                    "name": "teaspoon"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "chili powder",
                            "display_singular": "chili powder",
                            "display_plural": "chili powders",
                            "created_at": 1493307101,
                            "updated_at": 1509035289,
                            "id": 7
                        },
                        "id": 68491,
                        "raw_text": "1/2 tsp chili powder",
                        "extra_comment": ""
                    },
                    {
                        "extra_comment": "",
                        "position": 5,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                },
                                "id": 568732,
                                "quantity": "1"
                            }
                        ],
                        "ingredient": {
                            "id": 572,
                            "name": "chili flakes",
                            "display_singular": "chili flake",
                            "display_plural": "chili flakes",
                            "created_at": 1495313864,
                            "updated_at": 1509035250
                        },
                        "id": 68492,
                        "raw_text": "1 tbsp chili flakes"
                    },
                    {
                        "id": 68493,
                        "raw_text": "1/4 cup honey",
                        "extra_comment": "",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 568737,
                                "quantity": "¼",
                                "unit": {
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup",
                                    "abbreviation": "c"
                                }
                            },
                            {
                                "unit": {
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g"
                                },
                                "id": 568734,
                                "quantity": "85"
                            }
                        ],
                        "ingredient": {
                            "display_singular": "honey",
                            "display_plural": "honeys",
                            "created_at": 1493430363,
                            "updated_at": 1509035286,
                            "id": 52,
                            "name": "honey"
                        }
                    },
                    {
                        "raw_text": "1/8 tsp each salt and pepper",
                        "extra_comment": "",
                        "position": 7,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons",
                                    "system": "imperial"
                                },
                                "id": 568735,
                                "quantity": "⅛"
                            }
                        ],
                        "ingredient": {
                            "id": 6483,
                            "name": "salt and pepper",
                            "display_singular": "salt and pepper",
                            "display_plural": "salt and peppers",
                            "created_at": 1591200156,
                            "updated_at": 1591200156
                        },
                        "id": 68494
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "beauty_url": null,
        "brand_id": null,
        "created_at": 1591130227,
        "id": 6229,
        "seo_title": "",
        "nutrition_visibility": "auto",
        "brand": null,
        "credits": [
            {
                "name": "Shashi Charles",
                "type": "community"
            }
        ],
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "aspect_ratio": "16:9",
        "country": "US",
        "description": "",
        "is_one_top": false,
        "servings_noun_singular": "serving",
        "tips_and_ratings_enabled": true,
        "video_url": null,
        "yields": "Servings: 3",
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "instructions": [
            {
                "id": 55813,
                "display_text": "Place honey in a microwave safe dish and heat for 10 seconds until slightly warm.",
                "position": 1,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "id": 55815,
                "display_text": "Add in the turmeric, coriander, chili powder, salt, pepper, and chili flakes into the warmed honey and mix very well until all are incorporated.",
                "position": 2,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55817,
                "display_text": "Wash and pat dry the salmon fillets. When salmon fillets are dry, spoon on the honey-chili mixture onto each, making sure to coat the sides as well (this is messy and you will have to wash your air-fryer pan).",
                "position": 3,
                "start_time": 0,
                "end_time": 0
            },
            {
                "id": 55820,
                "display_text": "Then, place the salmon fillets in an air fryer, set temperature to 400°F and set time to 12 minutes. When time is up, remove salmon fillets and enjoy with your favorite sides.",
                "position": 4,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            }
        ],
        "user_ratings": {
            "count_positive": 0,
            "count_negative": 0,
            "score": null
        },
        "nutrition": {
            "calories": 379,
            "sugar": 29,
            "carbohydrates": 31,
            "fiber": 1,
            "updated_at": "2020-06-04T08:01:04+02:00",
            "protein": 25,
            "fat": 16
        }
    },
    {
        "brand_id": null,
        "buzz_id": null,
        "inspired_by_url": null,
        "seo_title": "",
        "total_time_minutes": null,
        "video_id": null,
        "compilations": [],
        "facebook_posts": [],
        "country": "US",
        "num_servings": 1,
        "tips_and_ratings_enabled": true,
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "aspect_ratio": "16:9",
        "prep_time_minutes": null,
        "show_id": 17,
        "video_url": null,
        "nutrition_visibility": "auto",
        "sections": [
            {
                "components": [
                    {
                        "id": 68423,
                        "raw_text": "1 package of spaghetti",
                        "extra_comment": "",
                        "position": 1,
                        "measurements": [
                            {
                                "id": 568647,
                                "quantity": "1",
                                "unit": {
                                    "name": "package",
                                    "abbreviation": "package",
                                    "display_singular": "package",
                                    "display_plural": "packages",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 589,
                            "name": "spaghetti",
                            "display_singular": "spaghetti",
                            "display_plural": "spaghettis",
                            "created_at": 1495414450,
                            "updated_at": 1509035249
                        }
                    },
                    {
                        "id": 68424,
                        "raw_text": "4 roma tomatoes",
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "quantity": "4",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                },
                                "id": 568648
                            }
                        ],
                        "ingredient": {
                            "display_plural": "roma tomatoes",
                            "created_at": 1496689259,
                            "updated_at": 1509035193,
                            "id": 1350,
                            "name": "roma tomato",
                            "display_singular": "roma tomato"
                        }
                    },
                    {
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 568652,
                                "quantity": "½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            },
                            {
                                "quantity": "120",
                                "unit": {
                                    "abbreviation": "mL",
                                    "display_singular": "mL",
                                    "display_plural": "mL",
                                    "system": "metric",
                                    "name": "milliliter"
                                },
                                "id": 568649
                            }
                        ],
                        "ingredient": {
                            "id": 4,
                            "name": "olive oil",
                            "display_singular": "olive oil",
                            "display_plural": "olive oils",
                            "created_at": 1493306183,
                            "updated_at": 1509035290
                        },
                        "id": 68425,
                        "raw_text": "1/2 cup olive oil"
                    },
                    {
                        "extra_comment": "",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 568650,
                                "quantity": "⅓",
                                "unit": {
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "fresh basils",
                            "created_at": 1494014468,
                            "updated_at": 1509035281,
                            "id": 175,
                            "name": "fresh basil",
                            "display_singular": "fresh basil"
                        },
                        "id": 68426,
                        "raw_text": "1/3 fresh basil"
                    },
                    {
                        "id": 68427,
                        "raw_text": "1/8 cup fresh garlic",
                        "extra_comment": "",
                        "position": 5,
                        "measurements": [
                            {
                                "id": 568654,
                                "quantity": "⅛",
                                "unit": {
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup",
                                    "abbreviation": "c"
                                }
                            },
                            {
                                "id": 568653,
                                "quantity": "20",
                                "unit": {
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 2084,
                            "name": "fresh garlic",
                            "display_singular": "fresh garlic",
                            "display_plural": "fresh garlics",
                            "created_at": 1499981187,
                            "updated_at": 1509035145
                        }
                    },
                    {
                        "position": 6,
                        "measurements": [
                            {
                                "id": 568651,
                                "quantity": "0",
                                "unit": {
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 22,
                            "name": "salt",
                            "display_singular": "salt",
                            "display_plural": "salts",
                            "created_at": 1493314644,
                            "updated_at": 1509035288
                        },
                        "id": 68428,
                        "raw_text": "salt to taste",
                        "extra_comment": "to taste"
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "video_ad_content": null,
        "id": 6223,
        "is_shoppable": true,
        "promotion": "full",
        "slug": "spaghetti-alla-chittara",
        "original_video_url": null,
        "user_ratings": {
            "score": null,
            "count_positive": 0,
            "count_negative": 0
        },
        "beauty_url": null,
        "yields": "Servings: 1",
        "tags": [
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "id": 64471,
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty"
            },
            {
                "display_name": "Vegan",
                "type": "dietary",
                "id": 64468,
                "name": "vegan"
            },
            {
                "id": 64469,
                "name": "vegetarian",
                "display_name": "Vegetarian",
                "type": "dietary"
            },
            {
                "name": "pescatarian",
                "display_name": "Pescatarian",
                "type": "dietary",
                "id": 3801552
            },
            {
                "id": 64463,
                "name": "dairy_free",
                "display_name": "Dairy-Free",
                "type": "dietary"
            },
            {
                "name": "dinner",
                "display_name": "Dinner",
                "type": "meal",
                "id": 64486
            },
            {
                "id": 64453,
                "name": "italian",
                "display_name": "Italian",
                "type": "cuisine"
            }
        ],
        "credits": [
            {
                "name": "Maya Richard-Craven",
                "type": "community"
            }
        ],
        "description": "",
        "draft_status": "published",
        "servings_noun_singular": "serving",
        "updated_at": 1591206702,
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "renditions": [],
        "nutrition": {},
        "approved_at": 1591206701,
        "cook_time_minutes": null,
        "created_at": 1591127648,
        "is_one_top": false,
        "keywords": "",
        "language": "eng",
        "name": "Spaghetti Alla Chittara",
        "servings_noun_plural": "servings",
        "thumbnail_url": "https://img.buzzfeed.com/tasty-app-user-assets-prod-us-east-1/recipes/f7c5cfc1141a4a798b201da0472ea986.jpeg",
        "brand": null,
        "instructions": [
            {
                "temperature": null,
                "appliance": null,
                "id": 55748,
                "display_text": "Boil water for pasta.",
                "position": 1,
                "start_time": 0,
                "end_time": 0
            },
            {
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55749,
                "display_text": "Cook for 7-9 minutes. Drain pasta and set aside.",
                "position": 2,
                "start_time": 0
            },
            {
                "appliance": null,
                "id": 55750,
                "display_text": "Dice tomatoes and thinly slice basil.",
                "position": 3,
                "start_time": 0,
                "end_time": 0,
                "temperature": null
            },
            {
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55751,
                "display_text": "Heat sauce pan for 3 minutes. Pour olive oil in saucepan.",
                "position": 4,
                "start_time": 0
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55752,
                "display_text": "Add tomatoes and fresh basil.",
                "position": 5,
                "start_time": 0,
                "end_time": 0
            },
            {
                "id": 55753,
                "display_text": "Stir for 5 minutes. Add garlic.",
                "position": 6,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "id": 55754,
                "display_text": "Mix in pasta. Add salt to taste.",
                "position": 7,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "id": 55885,
                "display_text": "Enjoy!",
                "position": 8,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            }
        ],
        "canonical_id": "recipe:6223"
    },
    {
        "seo_title": "",
        "total_time_tier": {
            "display_tier": "Under 30 minutes",
            "tier": "under_30_minutes"
        },
        "compilations": [],
        "cook_time_minutes": null,
        "description": "",
        "name": "Quick Kale Chips",
        "servings_noun_singular": "serving",
        "video_url": null,
        "nutrition_visibility": "auto",
        "is_one_top": false,
        "show_id": 17,
        "brand": null,
        "sections": [
            {
                "components": [
                    {
                        "extra_comment": "",
                        "position": 1,
                        "measurements": [
                            {
                                "id": 568642,
                                "quantity": "1",
                                "unit": {
                                    "display_singular": "bunch",
                                    "display_plural": "bunches",
                                    "system": "none",
                                    "name": "bunch",
                                    "abbreviation": "bunch"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 78,
                            "name": "kale",
                            "display_singular": "kale",
                            "display_plural": "kales",
                            "created_at": 1493743774,
                            "updated_at": 1509035285
                        },
                        "id": 68429,
                        "raw_text": "A bunch of kale"
                    },
                    {
                        "ingredient": {
                            "updated_at": 1568944191,
                            "id": 5741,
                            "name": "himalayan sea salt",
                            "display_singular": "himalayan sea salt",
                            "display_plural": "himalayan sea salts",
                            "created_at": 1568944191
                        },
                        "id": 68430,
                        "raw_text": "1/2 teaspoon Himalayan sea salt",
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "id": 568643,
                                "quantity": "½",
                                "unit": {
                                    "display_plural": "teaspoons",
                                    "system": "imperial",
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon"
                                }
                            }
                        ]
                    },
                    {
                        "measurements": [
                            {
                                "id": 568646,
                                "quantity": "½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            },
                            {
                                "id": 568645,
                                "quantity": "120",
                                "unit": {
                                    "name": "milliliter",
                                    "abbreviation": "mL",
                                    "display_singular": "mL",
                                    "display_plural": "mL",
                                    "system": "metric"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "olive oil",
                            "display_plural": "olive oils",
                            "created_at": 1493306183,
                            "updated_at": 1509035290,
                            "id": 4,
                            "name": "olive oil"
                        },
                        "id": 68431,
                        "raw_text": "1/2 cup pressed olive oil",
                        "extra_comment": "pressed",
                        "position": 3
                    },
                    {
                        "extra_comment": "drizzled",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 568644,
                                "quantity": "0",
                                "unit": {
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1591201992,
                            "updated_at": 1591201992,
                            "id": 6484,
                            "name": "clover honey",
                            "display_singular": "clover honey",
                            "display_plural": "clover honeys"
                        },
                        "id": 68432,
                        "raw_text": "Drizzle of clover honey"
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "facebook_posts": [],
        "approved_at": 1591206649,
        "aspect_ratio": "16:9",
        "servings_noun_plural": "servings",
        "total_time_minutes": null,
        "tags": [
            {
                "name": "5_ingredients_or_less",
                "display_name": "5 Ingredients or Less",
                "type": "difficulty",
                "id": 64470
            },
            {
                "id": 64471,
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty"
            },
            {
                "type": "difficulty",
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes"
            },
            {
                "id": 64469,
                "name": "vegetarian",
                "display_name": "Vegetarian",
                "type": "dietary"
            },
            {
                "id": 64467,
                "name": "low_carb",
                "display_name": "Low-Carb",
                "type": "dietary"
            },
            {
                "display_name": "Pescatarian",
                "type": "dietary",
                "id": 3801552,
                "name": "pescatarian"
            },
            {
                "id": 64466,
                "name": "healthy",
                "display_name": "Healthy",
                "type": "dietary"
            },
            {
                "id": 64465,
                "name": "gluten_free",
                "display_name": "Gluten-Free",
                "type": "dietary"
            },
            {
                "id": 64491,
                "name": "snacks",
                "display_name": "Snacks",
                "type": "meal"
            },
            {
                "display_name": "Bake",
                "type": "method",
                "id": 64492,
                "name": "bake"
            }
        ],
        "original_video_url": null,
        "user_ratings": {
            "count_positive": 0,
            "count_negative": 0,
            "score": null
        },
        "id": 6224,
        "is_shoppable": true,
        "keywords": "",
        "promotion": "full",
        "tips_and_ratings_enabled": true,
        "updated_at": 1591206649,
        "instructions": [
            {
                "temperature": 350,
                "appliance": "oven",
                "id": 55755,
                "display_text": "Preheat the oven to 350°F.",
                "position": 1,
                "start_time": 0,
                "end_time": 0
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55756,
                "display_text": "Wash and cut a bunch of kale.",
                "position": 2,
                "start_time": 0,
                "end_time": 0
            },
            {
                "id": 55757,
                "display_text": "Separate pieces onto a cookie sheet.",
                "position": 3,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "position": 4,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55758,
                "display_text": "Combine olive oil, honey and sea salt."
            },
            {
                "appliance": null,
                "id": 55759,
                "display_text": "Drizzle onto kale leaves.",
                "position": 5,
                "start_time": 0,
                "end_time": 0,
                "temperature": null
            },
            {
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55760,
                "display_text": "Put in the oven for 12-15 minutes.",
                "position": 6,
                "start_time": 0
            },
            {
                "position": 7,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55886,
                "display_text": "Enjoy!"
            }
        ],
        "renditions": [],
        "buzz_id": null,
        "country": "US",
        "language": "eng",
        "num_servings": 1,
        "thumbnail_url": "https://img.buzzfeed.com/tasty-app-user-assets-prod-us-east-1/recipes/adadc61a88144c93988bbbabb6c4c1bf.jpeg",
        "yields": "Servings: 1",
        "nutrition": {},
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "created_at": 1591128050,
        "inspired_by_url": null,
        "slug": "quick-kale-chips",
        "video_id": null,
        "credits": [
            {
                "name": "Maya Richard-Craven",
                "type": "community"
            }
        ],
        "video_ad_content": null,
        "beauty_url": null,
        "brand_id": null,
        "draft_status": "published",
        "prep_time_minutes": null,
        "canonical_id": "recipe:6224"
    },
    {
        "id": 6226,
        "servings_noun_singular": "serving",
        "updated_at": 1591206598,
        "brand": null,
        "tags": [
            {
                "id": 65850,
                "name": "indulgent_sweets",
                "display_name": "Indulgent Sweets",
                "type": "dietary"
            },
            {
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty",
                "id": 64472
            },
            {
                "id": 65857,
                "name": "bakery_goods",
                "display_name": "Bakery Goods",
                "type": "meal"
            },
            {
                "id": 64485,
                "name": "desserts",
                "display_name": "Desserts",
                "type": "meal"
            },
            {
                "type": "method",
                "id": 64492,
                "name": "bake",
                "display_name": "Bake"
            }
        ],
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "aspect_ratio": "16:9",
        "description": "",
        "nutrition": {
            "fat": 11,
            "calories": 215,
            "sugar": 17,
            "carbohydrates": 26,
            "fiber": 0,
            "updated_at": "2020-06-04T08:01:04+02:00",
            "protein": 1
        },
        "total_time_minutes": null,
        "video_id": null,
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "canonical_id": "recipe:6226",
        "brand_id": null,
        "tips_and_ratings_enabled": true,
        "keywords": "",
        "promotion": "full",
        "servings_noun_plural": "servings",
        "instructions": [
            {
                "start_time": 0,
                "end_time": 0,
                "temperature": 350,
                "appliance": "oven",
                "id": 55773,
                "display_text": "Preheat oven to 350°F",
                "position": 1
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55774,
                "display_text": "Mix sugars, butter, egg, and vanilla extract in a medium bowl.",
                "position": 2,
                "start_time": 0,
                "end_time": 0
            },
            {
                "position": 3,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55775,
                "display_text": "Stir in flour, baking powder, salt, white chocolate and butterscotch until dough."
            },
            {
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55777,
                "display_text": "Scoop onto a greased baking sheet and place in the oven for 10 to 12 minutes.",
                "position": 4
            },
            {
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55778,
                "display_text": "Let cool for 10 minutes.",
                "position": 5,
                "start_time": 0
            },
            {
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55888,
                "display_text": "Enjoy!",
                "position": 6
            }
        ],
        "user_ratings": {
            "score": null,
            "count_positive": 0,
            "count_negative": 0
        },
        "inspired_by_url": null,
        "is_one_top": false,
        "country": "US",
        "prep_time_minutes": null,
        "slug": "white-chocolate-butterscotch-cookies",
        "nutrition_visibility": "auto",
        "sections": [
            {
                "components": [
                    {
                        "measurements": [
                            {
                                "id": 568638,
                                "quantity": "½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            },
                            {
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                },
                                "id": 568637,
                                "quantity": "115"
                            }
                        ],
                        "ingredient": {
                            "display_singular": "butter",
                            "display_plural": "butters",
                            "created_at": 1493314940,
                            "updated_at": 1509035287,
                            "id": 30,
                            "name": "butter"
                        },
                        "id": 68440,
                        "raw_text": "1⁄2 cup butter, softened",
                        "extra_comment": "softened",
                        "position": 1
                    },
                    {
                        "id": 68441,
                        "raw_text": "1⁄2 cup sugar",
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "id": 568633,
                                "quantity": "½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            },
                            {
                                "id": 568631,
                                "quantity": "100",
                                "unit": {
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "sugars",
                            "created_at": 1493314650,
                            "updated_at": 1509035288,
                            "id": 24,
                            "name": "sugar",
                            "display_singular": "sugar"
                        }
                    },
                    {
                        "id": 68442,
                        "raw_text": "1⁄4 cup brown sugar",
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 568628,
                                "quantity": "¼",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            },
                            {
                                "id": 568627,
                                "quantity": "55",
                                "unit": {
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 6,
                            "name": "brown sugar",
                            "display_singular": "brown sugar",
                            "display_plural": "brown sugars",
                            "created_at": 1493307081,
                            "updated_at": 1509035289
                        }
                    },
                    {
                        "ingredient": {
                            "id": 3393,
                            "name": "all purpose flour",
                            "display_singular": "all purpose flour",
                            "display_plural": "all purpose flours",
                            "created_at": 1513187920,
                            "updated_at": 1513187920
                        },
                        "id": 68443,
                        "raw_text": "1 1⁄2 cup all purpose flour",
                        "extra_comment": "",
                        "position": 4,
                        "measurements": [
                            {
                                "quantity": "1 ½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                },
                                "id": 568640
                            },
                            {
                                "id": 568639,
                                "quantity": "185",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            }
                        ]
                    },
                    {
                        "id": 68444,
                        "raw_text": "1 tsp baking soda",
                        "extra_comment": "",
                        "position": 5,
                        "measurements": [
                            {
                                "id": 568634,
                                "quantity": "1",
                                "unit": {
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 247,
                            "name": "baking soda",
                            "display_singular": "baking soda",
                            "display_plural": "baking sodas",
                            "created_at": 1494297371,
                            "updated_at": 1509035276
                        }
                    },
                    {
                        "id": 68445,
                        "raw_text": "1⁄2 cup butterscotch chips",
                        "extra_comment": "",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 568636,
                                "quantity": "½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            },
                            {
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                },
                                "id": 568635,
                                "quantity": "85"
                            }
                        ],
                        "ingredient": {
                            "id": 4966,
                            "name": "butterscotch chips",
                            "display_singular": "butterscotch chip",
                            "display_plural": "butterscotch chips",
                            "created_at": 1543686800,
                            "updated_at": 1543686800
                        }
                    },
                    {
                        "id": 68446,
                        "raw_text": "1⁄2 cup white chocolate chips",
                        "extra_comment": "",
                        "position": 7,
                        "measurements": [
                            {
                                "id": 568632,
                                "quantity": "½",
                                "unit": {
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup"
                                }
                            },
                            {
                                "quantity": "85",
                                "unit": {
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g"
                                },
                                "id": 568629
                            }
                        ],
                        "ingredient": {
                            "created_at": 1496505406,
                            "updated_at": 1509035204,
                            "id": 1162,
                            "name": "white chocolate chip",
                            "display_singular": "white chocolate chip",
                            "display_plural": "white chocolate chips"
                        }
                    },
                    {
                        "measurements": [
                            {
                                "id": 568630,
                                "quantity": "¼",
                                "unit": {
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1493314644,
                            "updated_at": 1509035288,
                            "id": 22,
                            "name": "salt",
                            "display_singular": "salt",
                            "display_plural": "salts"
                        },
                        "id": 68447,
                        "raw_text": "1⁄4 teaspoon salt",
                        "extra_comment": "",
                        "position": 8
                    },
                    {
                        "measurements": [
                            {
                                "id": 568641,
                                "quantity": "2",
                                "unit": {
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1493745620,
                            "updated_at": 1509035284,
                            "id": 103,
                            "name": "vanilla extract",
                            "display_singular": "vanilla extract",
                            "display_plural": "vanilla extracts"
                        },
                        "id": 68448,
                        "raw_text": "2 teaspoons vanilla extract",
                        "extra_comment": "",
                        "position": 9
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "approved_at": 1591206598,
        "beauty_url": null,
        "language": "eng",
        "facebook_posts": [],
        "renditions": [],
        "cook_time_minutes": null,
        "is_shoppable": true,
        "video_url": null,
        "yields": "Servings: 12",
        "compilations": [],
        "credits": [
            {
                "name": "Bryce Walker",
                "type": "community"
            }
        ],
        "seo_title": "",
        "show_id": 17,
        "draft_status": "published",
        "name": "White Chocolate Butterscotch Cookies",
        "num_servings": 12,
        "thumbnail_url": "https://img.buzzfeed.com/tasty-app-user-assets-prod-us-east-1/recipes/b9756fa37795430ea4365ddd272de843.jpeg",
        "original_video_url": null,
        "video_ad_content": null,
        "buzz_id": null,
        "created_at": 1591128991
    },
    {
        "num_servings": 1,
        "show_id": 17,
        "slug": "english-muffin-pizzas",
        "thumbnail_url": "https://img.buzzfeed.com/tasty-app-user-assets-prod-us-east-1/recipes/37feba9904d94bfa9dac95c2a0749725.jpeg",
        "updated_at": 1591206559,
        "nutrition_visibility": "auto",
        "is_one_top": false,
        "name": "English Muffin Pizzas",
        "nutrition": {
            "fat": 1,
            "calories": 142,
            "sugar": 2,
            "carbohydrates": 27,
            "fiber": 1,
            "updated_at": "2020-06-04T08:01:05+02:00",
            "protein": 5
        },
        "compilations": [],
        "credits": [
            {
                "name": "Mckenna Hoffman",
                "type": "community"
            }
        ],
        "keywords": "",
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "aspect_ratio": "16:9",
        "inspired_by_url": null,
        "yields": "Servings: 1",
        "tags": [
            {
                "type": "cuisine",
                "id": 64444,
                "name": "american",
                "display_name": "American"
            },
            {
                "id": 64462,
                "name": "comfort_food",
                "display_name": "Comfort Food",
                "type": "dietary"
            },
            {
                "display_name": "Kid-Friendly",
                "type": "dietary",
                "id": 64488,
                "name": "kid_friendly"
            },
            {
                "name": "vegetarian",
                "display_name": "Vegetarian",
                "type": "dietary",
                "id": 64469
            },
            {
                "id": 64470,
                "name": "5_ingredients_or_less",
                "display_name": "5 Ingredients or Less",
                "type": "difficulty"
            },
            {
                "type": "difficulty",
                "id": 64471,
                "name": "easy",
                "display_name": "Easy"
            },
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "id": 64491,
                "name": "snacks",
                "display_name": "Snacks",
                "type": "meal"
            },
            {
                "id": 64503,
                "name": "casual_party",
                "display_name": "Casual Party",
                "type": "occasion"
            }
        ],
        "user_ratings": {
            "count_positive": 0,
            "count_negative": 0,
            "score": null
        },
        "renditions": [],
        "brand_id": null,
        "video_id": null,
        "is_shoppable": true,
        "prep_time_minutes": null,
        "brand": null,
        "sections": [
            {
                "components": [
                    {
                        "id": 68509,
                        "raw_text": "1 English muffin",
                        "extra_comment": "",
                        "position": 1,
                        "measurements": [
                            {
                                "id": 568623,
                                "quantity": "1",
                                "unit": {
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 1080,
                            "name": "english muffin",
                            "display_singular": "english muffin",
                            "display_plural": "english muffins",
                            "created_at": 1496289153,
                            "updated_at": 1509035210
                        }
                    },
                    {
                        "measurements": [
                            {
                                "id": 568624,
                                "quantity": "2",
                                "unit": {
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp"
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035279,
                            "id": 201,
                            "name": "marinara sauce",
                            "display_singular": "marinara sauce",
                            "display_plural": "marinara sauces",
                            "created_at": 1494208809
                        },
                        "id": 68510,
                        "raw_text": "2 tbsp marinara sauce",
                        "extra_comment": "",
                        "position": 2
                    },
                    {
                        "ingredient": {
                            "updated_at": 1591203361,
                            "id": 6486,
                            "name": "cheese of choice",
                            "display_singular": "cheese of choice",
                            "display_plural": "cheeses of choice",
                            "created_at": 1591203361
                        },
                        "id": 68511,
                        "raw_text": "2 tbsp cheese of choice",
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 568625,
                                "quantity": "2",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ]
                    },
                    {
                        "id": 68512,
                        "raw_text": "Topping of choice (optional)",
                        "extra_comment": "optional",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 568626,
                                "quantity": "0",
                                "unit": {
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 6487,
                            "name": "topping of choice",
                            "display_singular": "topping of choice",
                            "display_plural": "toppings of choice",
                            "created_at": 1591203367,
                            "updated_at": 1591203367
                        }
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "canonical_id": "recipe:6232",
        "facebook_posts": [],
        "beauty_url": null,
        "created_at": 1591148807,
        "language": "eng",
        "video_ad_content": null,
        "country": "US",
        "servings_noun_plural": "servings",
        "tips_and_ratings_enabled": true,
        "total_time_minutes": null,
        "original_video_url": null,
        "buzz_id": null,
        "cook_time_minutes": null,
        "seo_title": "",
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "instructions": [
            {
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55835,
                "display_text": "Separate and toast the English muffin. It should be crispy.",
                "position": 1,
                "start_time": 0
            },
            {
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55836,
                "display_text": "Place half of the marinara sauce on each half of the English muffin.",
                "position": 2,
                "start_time": 0
            },
            {
                "id": 55837,
                "display_text": "Add half the cheese to each half of the English muffin.",
                "position": 3,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55838,
                "display_text": "Add toppings. (If you are not using toppings, skip this step.)",
                "position": 4,
                "start_time": 0
            },
            {
                "position": 5,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55839,
                "display_text": "Microwave for 30 seconds or until the cheese is melted and the toppings are warm."
            },
            {
                "display_text": "Enjoy!",
                "position": 6,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55891
            }
        ],
        "description": "",
        "promotion": "full",
        "id": 6232,
        "servings_noun_singular": "serving",
        "video_url": null,
        "approved_at": 1591206559,
        "draft_status": "published"
    },
    {
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/267181.jpg",
        "video_id": 105482,
        "recipes": [
            {
                "is_one_top": false,
                "language": "eng",
                "promotion": "full",
                "slug": "creamy-kimchi-bacon-spaghetti",
                "total_time_minutes": 35,
                "compilations": [
                    {
                        "approved_at": 1590763287,
                        "id": 1495,
                        "keywords": null,
                        "name": "Cafemaddy Shows You 3 Creative Ways Of Using Kimchi",
                        "promotion": "full",
                        "show": [
                            {
                                "name": "Tasty",
                                "id": 17
                            }
                        ],
                        "beauty_url": null,
                        "country": "US",
                        "created_at": 1590515886,
                        "video_id": 105482,
                        "canonical_id": "compilation:1495",
                        "facebook_posts": [],
                        "aspect_ratio": "16:9",
                        "buzz_id": null,
                        "draft_status": "published",
                        "is_shoppable": false,
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/267181.jpg",
                        "description": null,
                        "language": "eng",
                        "slug": "cafemaddy-shows-you-3-creative-ways-of-using-kimchi",
                        "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8"
                    }
                ],
                "beauty_url": null,
                "seo_title": "",
                "show_id": 17,
                "yields": "Servings: 2",
                "credits": [
                    {
                        "name": "Matt Ciampa",
                        "type": "internal"
                    }
                ],
                "show": {
                    "id": 17,
                    "name": "Tasty"
                },
                "approved_at": 1590763279,
                "created_at": 1590515948,
                "prep_time_minutes": 10,
                "updated_at": 1590763279,
                "cook_time_minutes": 25,
                "servings_noun_plural": "servings",
                "video_id": 105482,
                "aspect_ratio": "16:9",
                "brand_id": null,
                "tips_and_ratings_enabled": true,
                "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                "total_time_tier": {
                    "tier": "under_45_minutes",
                    "display_tier": "Under 45 minutes"
                },
                "tags": [
                    {
                        "type": "equipment",
                        "id": 1247769,
                        "name": "cheese_grater",
                        "display_name": "Cheese Grater"
                    },
                    {
                        "id": 1280503,
                        "name": "cutting_board",
                        "display_name": "Cutting Board",
                        "type": "equipment"
                    },
                    {
                        "id": 64462,
                        "name": "comfort_food",
                        "display_name": "Comfort Food",
                        "type": "dietary"
                    },
                    {
                        "name": "italian",
                        "display_name": "Italian",
                        "type": "cuisine",
                        "id": 64453
                    },
                    {
                        "id": 1247770,
                        "name": "colander",
                        "display_name": "Colander",
                        "type": "equipment"
                    },
                    {
                        "type": "appliance",
                        "id": 65848,
                        "name": "stove_top",
                        "display_name": "Stove Top"
                    },
                    {
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty"
                    },
                    {
                        "id": 64455,
                        "name": "korean",
                        "display_name": "Korean",
                        "type": "cuisine"
                    },
                    {
                        "id": 64471,
                        "name": "easy",
                        "display_name": "Easy",
                        "type": "difficulty"
                    },
                    {
                        "id": 1247789,
                        "name": "strainer",
                        "display_name": "Strainer",
                        "type": "equipment"
                    },
                    {
                        "display_name": "Dinner",
                        "type": "meal",
                        "id": 64486,
                        "name": "dinner"
                    },
                    {
                        "id": 3802077,
                        "name": "asian_pacific_american_heritage_month",
                        "display_name": "Asian Pacific American Heritage Month",
                        "type": "holiday"
                    }
                ],
                "canonical_id": "recipe:6203",
                "nutrition": {
                    "fiber": 3,
                    "updated_at": "2020-05-28T08:06:28+02:00",
                    "calories": 915,
                    "carbohydrates": 61,
                    "fat": 71,
                    "protein": 23,
                    "sugar": 7
                },
                "country": "US",
                "keywords": "3 ways, apahm, asian american recipes, at home, cabbage, cafe maddy, cafemaddy, carbonara, cooking, corona virus, covid, creamy bacon spaghetti, delicious, easy, grilled cheese, heritage, instagram, kimchi, korea, korean, korean american, koreatown, napa cabbage, pasta, recipes, scramble, simply, spam and egg scramble, tik tok",
                "num_servings": 2,
                "renditions": [
                    {
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/168350/square_720/1590516270",
                        "height": 404,
                        "duration": 424461,
                        "bit_rate": 1215,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "name": "mp4_720x404",
                        "width": 720,
                        "file_size": 64433716,
                        "content_type": "video/mp4",
                        "aspect": "landscape",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_720/1590516270_00001.png"
                    },
                    {
                        "url": "https://vid.tasty.co/output/168350/square_320/1590516270",
                        "duration": 424461,
                        "file_size": 23303407,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_320/1590516270_00001.png",
                        "name": "mp4_320x180",
                        "height": 180,
                        "width": 320,
                        "bit_rate": 440,
                        "content_type": "video/mp4",
                        "aspect": "landscape"
                    },
                    {
                        "duration": 424461,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_720/1590516270_00001.png",
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/168350/landscape_720/1590516270",
                        "name": "mp4_1280x720",
                        "height": 720,
                        "width": 1280,
                        "file_size": 150190774,
                        "bit_rate": 2831,
                        "content_type": "video/mp4"
                    },
                    {
                        "duration": 424461,
                        "file_size": 54369542,
                        "bit_rate": 1025,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "landscape",
                        "height": 360,
                        "width": 640,
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_480/1590516270_00001.png",
                        "name": "mp4_640x360",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/168350/landscape_480/1590516270",
                        "minimum_bit_rate": null
                    },
                    {
                        "content_type": "application/vnd.apple.mpegurl",
                        "aspect": "landscape",
                        "name": "low",
                        "width": 1920,
                        "duration": 424469,
                        "bit_rate": null,
                        "maximum_bit_rate": 5564,
                        "minimum_bit_rate": 269,
                        "container": "ts",
                        "url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/1445289064805-h2exzu/1590516270_00001.png",
                        "height": 1080,
                        "file_size": null
                    }
                ],
                "draft_status": "published",
                "is_shoppable": true,
                "name": "Creamy Kimchi Bacon Spaghetti",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/a859957c483f4dcd886e1799025b5693/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.jpg",
                "instructions": [
                    {
                        "end_time": 275666,
                        "temperature": null,
                        "appliance": null,
                        "id": 55569,
                        "display_text": "Dice the bacon, mince the shallots, and grate the garlic on a microplane.",
                        "position": 1,
                        "start_time": 269833
                    },
                    {
                        "position": 2,
                        "start_time": 277000,
                        "end_time": 285000,
                        "temperature": null,
                        "appliance": null,
                        "id": 55570,
                        "display_text": "Bring a large pot of salted water to a boil. Add the pasta and cook according to the package instructions until al dente. Reserve 1 cup of the cooking water, then drain the pasta."
                    },
                    {
                        "start_time": 286000,
                        "end_time": 292166,
                        "temperature": null,
                        "appliance": null,
                        "id": 55571,
                        "display_text": "Heat a large skillet over medium-high heat. Add the bacon and cook until crispy, 5–7 minutes. Using a slotted spoon, transfer the bacon to a paper towel-lined plate to drain. Discard all but 1 tablespoon of the rendered bacon fat.",
                        "position": 3
                    },
                    {
                        "end_time": 311333,
                        "temperature": null,
                        "appliance": null,
                        "id": 55572,
                        "display_text": "Reduce the heat to medium and add the shallot to the pan with the bacon fat. Cook until softened, 3–5 minutes. Add the garlic and kimchi and sauté until fragrant, about 2 minutes.",
                        "position": 4,
                        "start_time": 298000
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 55573,
                        "display_text": "Add the butter and heavy cream to the pan. Season with salt. Bring to a low simmer and cook until bubbling. Add the cooked pasta and toss until well coated.",
                        "position": 5,
                        "start_time": 314000,
                        "end_time": 331833
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 55574,
                        "display_text": "While stirring constantly, gradually add the Parmesan cheese, mixing until melted. If the sauce gets too thick, add a little bit of the reserved pasta water as needed. Continue stirring until all of the cheese has been added and the sauce is creamy. Add some of the crispy bacon, reserving some for garnish, and stir to incorporate.",
                        "position": 6,
                        "start_time": 333000,
                        "end_time": 344166
                    },
                    {
                        "start_time": 345000,
                        "end_time": 355500,
                        "temperature": null,
                        "appliance": null,
                        "id": 55575,
                        "display_text": "Divide the pasta between 2 serving bowls and top with the reserved bacon, scallions, and furikake, if using.",
                        "position": 7
                    },
                    {
                        "display_text": "Enjoy!",
                        "position": 8,
                        "start_time": 356000,
                        "end_time": 367500,
                        "temperature": null,
                        "appliance": null,
                        "id": 55576
                    }
                ],
                "sections": [
                    {
                        "components": [
                            {
                                "extra_comment": "",
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 567071,
                                        "quantity": "4",
                                        "unit": {
                                            "name": "slice",
                                            "abbreviation": "slice",
                                            "display_singular": "slice",
                                            "display_plural": "slices",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "bacons",
                                    "created_at": 1494212643,
                                    "updated_at": 1509035279,
                                    "id": 214,
                                    "name": "bacon",
                                    "display_singular": "bacon"
                                },
                                "id": 68182,
                                "raw_text": "4 slices of bacon"
                            },
                            {
                                "position": 2,
                                "measurements": [
                                    {
                                        "id": 567072,
                                        "quantity": "1",
                                        "unit": {
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "medium shallots",
                                    "created_at": 1540620139,
                                    "updated_at": 1540620139,
                                    "id": 4857,
                                    "name": "medium shallot",
                                    "display_singular": "medium shallot"
                                },
                                "id": 68183,
                                "raw_text": "1 medium shallot",
                                "extra_comment": ""
                            },
                            {
                                "id": 68184,
                                "raw_text": "1 clove garlic",
                                "extra_comment": "",
                                "position": 3,
                                "measurements": [
                                    {
                                        "id": 567074,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "clove",
                                            "abbreviation": "clove",
                                            "display_singular": "clove",
                                            "display_plural": "cloves",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1493744766,
                                    "updated_at": 1509035285,
                                    "id": 95,
                                    "name": "garlic",
                                    "display_singular": "garlic",
                                    "display_plural": "garlics"
                                }
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 567073,
                                        "quantity": "0",
                                        "unit": {
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 11,
                                    "name": "kosher salt",
                                    "display_singular": "kosher salt",
                                    "display_plural": "kosher salts",
                                    "created_at": 1493307153,
                                    "updated_at": 1509035289
                                },
                                "id": 68185,
                                "raw_text": "Kosher salt, to taste",
                                "extra_comment": "to taste",
                                "position": 4
                            },
                            {
                                "extra_comment": "or linguine",
                                "position": 5,
                                "measurements": [
                                    {
                                        "id": 567076,
                                        "quantity": "5",
                                        "unit": {
                                            "abbreviation": "oz",
                                            "display_singular": "oz",
                                            "display_plural": "oz",
                                            "system": "imperial",
                                            "name": "ounce"
                                        }
                                    },
                                    {
                                        "id": 567075,
                                        "quantity": "140",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "dried spathetti",
                                    "display_plural": "dried spathettis",
                                    "created_at": 1526070693,
                                    "updated_at": 1526070693,
                                    "id": 4073,
                                    "name": "dried spathetti"
                                },
                                "id": 68186,
                                "raw_text": "5 ounces dried spaghetti or linguine"
                            },
                            {
                                "raw_text": "¼ cup kimchi, diced",
                                "extra_comment": "diced",
                                "position": 6,
                                "measurements": [
                                    {
                                        "id": 567078,
                                        "quantity": "¼",
                                        "unit": {
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "quantity": "15",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        },
                                        "id": 567077
                                    }
                                ],
                                "ingredient": {
                                    "id": 973,
                                    "name": "kimchi",
                                    "display_singular": "kimchi",
                                    "display_plural": "kimchis",
                                    "created_at": 1496170737,
                                    "updated_at": 1509035217
                                },
                                "id": 68187
                            },
                            {
                                "id": 68188,
                                "raw_text": "2 tablespoons unsalted butter",
                                "extra_comment": "",
                                "position": 7,
                                "measurements": [
                                    {
                                        "id": 567080,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 291,
                                    "name": "unsalted butter",
                                    "display_singular": "unsalted butter",
                                    "display_plural": "unsalted butters",
                                    "created_at": 1494806355,
                                    "updated_at": 1509035272
                                }
                            },
                            {
                                "extra_comment": "",
                                "position": 8,
                                "measurements": [
                                    {
                                        "id": 567084,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "id": 567083,
                                        "quantity": "240",
                                        "unit": {
                                            "name": "milliliter",
                                            "abbreviation": "mL",
                                            "display_singular": "mL",
                                            "display_plural": "mL",
                                            "system": "metric"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "heavy cream",
                                    "display_plural": "heavy creams",
                                    "created_at": 1494214054,
                                    "updated_at": 1509035278,
                                    "id": 221,
                                    "name": "heavy cream"
                                },
                                "id": 68189,
                                "raw_text": "1 cup heavy cream"
                            },
                            {
                                "id": 68190,
                                "raw_text": "½ cup grated Parmesan cheese",
                                "extra_comment": "",
                                "position": 9,
                                "measurements": [
                                    {
                                        "quantity": "½",
                                        "unit": {
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial"
                                        },
                                        "id": 567082
                                    },
                                    {
                                        "id": 567081,
                                        "quantity": "55",
                                        "unit": {
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 1869,
                                    "name": "grated parmesan cheese",
                                    "display_singular": "grated parmesan cheese",
                                    "display_plural": "grated parmesan cheeses",
                                    "created_at": 1497741203,
                                    "updated_at": 1509035159
                                }
                            },
                            {
                                "position": 10,
                                "measurements": [
                                    {
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        },
                                        "id": 567079
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "scallions",
                                    "created_at": 1494803890,
                                    "updated_at": 1509035273,
                                    "id": 276,
                                    "name": "scallions",
                                    "display_singular": "scallion"
                                },
                                "id": 68191,
                                "raw_text": "2 tablespoons thinly sliced scallions, for garnish",
                                "extra_comment": "thinly sliced, for garnish"
                            },
                            {
                                "extra_comment": "for garnish - optional",
                                "position": 11,
                                "measurements": [
                                    {
                                        "id": 567085,
                                        "quantity": "0",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "furikake",
                                    "display_singular": "furikake",
                                    "display_plural": "furikakes",
                                    "created_at": 1590592254,
                                    "updated_at": 1590592254,
                                    "id": 6461
                                },
                                "id": 68192,
                                "raw_text": "Furikake, for garnish (optional)"
                            }
                        ],
                        "name": null,
                        "position": 1
                    }
                ],
                "user_ratings": {
                    "count_positive": 0,
                    "count_negative": 0,
                    "score": null
                },
                "buzz_id": null,
                "description": "Kimchi is the secret ingredient you never knew your creamy pasta dishes were missing. It adds acidity to balance the heaviness of the cream and Parmesan, and a delightful burst of tangy flavor. Give it a try, you won’t be sorry!",
                "id": 6203,
                "inspired_by_url": null,
                "servings_noun_singular": "serving",
                "nutrition_visibility": "auto",
                "brand": null,
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/52033da23b824b20a416cdd6b3277e91/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.mp4",
                "video_ad_content": "none",
                "facebook_posts": []
            },
            {
                "show": {
                    "id": 17,
                    "name": "Tasty"
                },
                "draft_status": "published",
                "language": "eng",
                "sections": [
                    {
                        "components": [
                            {
                                "id": 68167,
                                "raw_text": "½ cup shredded sharp cheddar cheese, divided",
                                "extra_comment": "divided",
                                "position": 1,
                                "measurements": [
                                    {
                                        "unit": {
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial"
                                        },
                                        "id": 567057,
                                        "quantity": "½"
                                    },
                                    {
                                        "id": 567053,
                                        "quantity": "50",
                                        "unit": {
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035098,
                                    "id": 2952,
                                    "name": "shredded sharp cheddar cheese",
                                    "display_singular": "shredded sharp cheddar cheese",
                                    "display_plural": "shredded sharp cheddar cheeses",
                                    "created_at": 1504664792
                                }
                            },
                            {
                                "id": 68168,
                                "raw_text": "¼ cup kimchi, drained",
                                "extra_comment": "drained",
                                "position": 2,
                                "measurements": [
                                    {
                                        "id": 567059,
                                        "quantity": "¼",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups"
                                        }
                                    },
                                    {
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        },
                                        "id": 567058,
                                        "quantity": "15"
                                    }
                                ],
                                "ingredient": {
                                    "id": 973,
                                    "name": "kimchi",
                                    "display_singular": "kimchi",
                                    "display_plural": "kimchis",
                                    "created_at": 1496170737,
                                    "updated_at": 1509035217
                                }
                            },
                            {
                                "raw_text": "2 pieces of Italian bread",
                                "extra_comment": "",
                                "position": 3,
                                "measurements": [
                                    {
                                        "id": 567054,
                                        "quantity": "2",
                                        "unit": {
                                            "display_plural": "pieces",
                                            "system": "none",
                                            "name": "piece",
                                            "abbreviation": "piece",
                                            "display_singular": "piece"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1500491791,
                                    "updated_at": 1509035132,
                                    "id": 2291,
                                    "name": "italian bread",
                                    "display_singular": "italian bread",
                                    "display_plural": "italian breads"
                                },
                                "id": 68169
                            },
                            {
                                "id": 68170,
                                "raw_text": "4 tablespoons (½ stick) unsalted butter",
                                "extra_comment": "1/2 stick",
                                "position": 4,
                                "measurements": [
                                    {
                                        "id": 567052,
                                        "quantity": "4",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 291,
                                    "name": "unsalted butter",
                                    "display_singular": "unsalted butter",
                                    "display_plural": "unsalted butters",
                                    "created_at": 1494806355,
                                    "updated_at": 1509035272
                                }
                            },
                            {
                                "position": 5,
                                "measurements": [
                                    {
                                        "id": 567055,
                                        "quantity": "1",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "teaspoon",
                                            "abbreviation": "tsp",
                                            "display_singular": "teaspoon",
                                            "display_plural": "teaspoons"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "olive oil",
                                    "display_singular": "olive oil",
                                    "display_plural": "olive oils",
                                    "created_at": 1493306183,
                                    "updated_at": 1509035290,
                                    "id": 4
                                },
                                "id": 68171,
                                "raw_text": "1 teaspoon olive oil",
                                "extra_comment": ""
                            },
                            {
                                "id": 68172,
                                "raw_text": "1 teaspoon sugar",
                                "extra_comment": "",
                                "position": 6,
                                "measurements": [
                                    {
                                        "id": 567056,
                                        "quantity": "1",
                                        "unit": {
                                            "display_singular": "teaspoon",
                                            "display_plural": "teaspoons",
                                            "system": "imperial",
                                            "name": "teaspoon",
                                            "abbreviation": "tsp"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "sugar",
                                    "display_singular": "sugar",
                                    "display_plural": "sugars",
                                    "created_at": 1493314650,
                                    "updated_at": 1509035288,
                                    "id": 24
                                }
                            },
                            {
                                "extra_comment": "",
                                "position": 7,
                                "measurements": [
                                    {
                                        "unit": {
                                            "display_singular": "slice",
                                            "display_plural": "slices",
                                            "system": "none",
                                            "name": "slice",
                                            "abbreviation": "slice"
                                        },
                                        "id": 567060,
                                        "quantity": "2"
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035240,
                                    "id": 697,
                                    "name": "american cheese",
                                    "display_singular": "american cheese",
                                    "display_plural": "american cheeses",
                                    "created_at": 1495665885
                                },
                                "id": 68173,
                                "raw_text": "2 slices of American cheese"
                            }
                        ],
                        "name": null,
                        "position": 1
                    }
                ],
                "credits": [
                    {
                        "name": "Matt Ciampa",
                        "type": "internal"
                    }
                ],
                "video_ad_content": "none",
                "approved_at": 1590763265,
                "servings_noun_singular": "serving",
                "compilations": [
                    {
                        "created_at": 1590515886,
                        "promotion": "full",
                        "canonical_id": "compilation:1495",
                        "facebook_posts": [],
                        "show": [
                            {
                                "id": 17,
                                "name": "Tasty"
                            }
                        ],
                        "country": "US",
                        "is_shoppable": false,
                        "name": "Cafemaddy Shows You 3 Creative Ways Of Using Kimchi",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/267181.jpg",
                        "id": 1495,
                        "aspect_ratio": "16:9",
                        "draft_status": "published",
                        "video_id": 105482,
                        "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                        "approved_at": 1590763287,
                        "buzz_id": null,
                        "description": null,
                        "keywords": null,
                        "language": "eng",
                        "slug": "cafemaddy-shows-you-3-creative-ways-of-using-kimchi",
                        "beauty_url": null
                    }
                ],
                "canonical_id": "recipe:6201",
                "renditions": [
                    {
                        "width": 720,
                        "duration": 424461,
                        "bit_rate": 1215,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "container": "mp4",
                        "name": "mp4_720x404",
                        "height": 404,
                        "file_size": 64433716,
                        "maximum_bit_rate": null,
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/168350/square_720/1590516270",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_720/1590516270_00001.png"
                    },
                    {
                        "width": 320,
                        "duration": 424461,
                        "minimum_bit_rate": null,
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/168350/square_320/1590516270",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_320/1590516270_00001.png",
                        "height": 180,
                        "file_size": 23303407,
                        "bit_rate": 440,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "container": "mp4",
                        "name": "mp4_320x180"
                    },
                    {
                        "bit_rate": 2831,
                        "maximum_bit_rate": null,
                        "aspect": "landscape",
                        "container": "mp4",
                        "height": 720,
                        "width": 1280,
                        "duration": 424461,
                        "file_size": 150190774,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "url": "https://vid.tasty.co/output/168350/landscape_720/1590516270",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_720/1590516270_00001.png",
                        "name": "mp4_1280x720"
                    },
                    {
                        "minimum_bit_rate": null,
                        "aspect": "landscape",
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_480/1590516270_00001.png",
                        "width": 640,
                        "duration": 424461,
                        "file_size": 54369542,
                        "bit_rate": 1025,
                        "name": "mp4_640x360",
                        "height": 360,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "url": "https://vid.tasty.co/output/168350/landscape_480/1590516270"
                    },
                    {
                        "url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/1445289064805-h2exzu/1590516270_00001.png",
                        "name": "low",
                        "width": 1920,
                        "file_size": null,
                        "minimum_bit_rate": 269,
                        "aspect": "landscape",
                        "content_type": "application/vnd.apple.mpegurl",
                        "container": "ts",
                        "height": 1080,
                        "duration": 424469,
                        "bit_rate": null,
                        "maximum_bit_rate": 5564
                    }
                ],
                "user_ratings": {
                    "count_positive": 0,
                    "count_negative": 0,
                    "score": null
                },
                "facebook_posts": [],
                "is_shoppable": true,
                "prep_time_minutes": 5,
                "promotion": "full",
                "seo_title": "",
                "instructions": [
                    {
                        "id": 55554,
                        "display_text": "Shred the cheddar cheese on the large holes of a box grater.",
                        "position": 1,
                        "start_time": 49000,
                        "end_time": 53333,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "appliance": null,
                        "id": 55555,
                        "display_text": "Add the kimchi to a small bowl and use kitchen shears to cut into small pieces.",
                        "position": 2,
                        "start_time": 61000,
                        "end_time": 64333,
                        "temperature": null
                    },
                    {
                        "start_time": 65000,
                        "end_time": 68000,
                        "temperature": null,
                        "appliance": null,
                        "id": 55556,
                        "display_text": "Butter both sides of each slice of bread.",
                        "position": 3
                    },
                    {
                        "position": 4,
                        "start_time": 70000,
                        "end_time": 92666,
                        "temperature": null,
                        "appliance": null,
                        "id": 55557,
                        "display_text": "Heat the olive oil in a medium nonstick skillet over medium-high heat. Add the kimchi and cook until softened, about 2–3 minutes. Add the sugar and cook for an additional minute, stirring to incorporate. Transfer the kimchi to a bowl and wipe out the skillet."
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 55558,
                        "display_text": "Turn the heat to medium and add both slices of butter bread to the skillet. Cook until the bottom is golden brown, 3–4 minutes. Flip the bread, then add the American cheese to one slice and all but 2 tablespoons of the cheddar cheese to the other slice. Top the American cheese with the kimchi and remaining 2 tablespoons cheddar. Cook until the cheese starts to melt and the underside of the bread begins to turn golden, 3–4 minutes.",
                        "position": 5,
                        "start_time": 94000,
                        "end_time": 117500
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 55559,
                        "display_text": "Sandwich the slices of bread together and cook, flipping once, for another 2–3 minutes, until the cheese is gooey and fully melted and the bread is well-toasted.",
                        "position": 6,
                        "start_time": 119000,
                        "end_time": 125000
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 55560,
                        "display_text": "Transfer to a cutting board and cut in half.",
                        "position": 7,
                        "start_time": 130000,
                        "end_time": 136166
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 55561,
                        "display_text": "Enjoy!",
                        "position": 8,
                        "start_time": 137000,
                        "end_time": 140000
                    }
                ],
                "keywords": "3 ways, apahm, asian american recipes, at home, cabbage, cafe maddy, cafemaddy, carbonara, cooking, corona virus, covid, creamy bacon spaghetti, delicious, easy, grilled cheese, heritage, instagram, kimchi, korea, korean, korean american, koreatown, napa cabbage, pasta, recipes, scramble, simply, spam and egg scramble, tik tok",
                "num_servings": 1,
                "servings_noun_plural": "servings",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/10942a0a6c214b649c1405a4fc00c5d2/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.jpg",
                "aspect_ratio": "16:9",
                "beauty_url": null,
                "brand_id": null,
                "country": "US",
                "created_at": 1590515831,
                "tips_and_ratings_enabled": true,
                "total_time_minutes": 15,
                "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                "nutrition": {},
                "cook_time_minutes": 10,
                "name": "Kimchi Grilled Cheese",
                "tags": [
                    {
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty"
                    },
                    {
                        "id": 1280505,
                        "name": "kitchen_shears",
                        "display_name": "Kitchen Shears",
                        "type": "equipment"
                    },
                    {
                        "id": 64471,
                        "name": "easy",
                        "display_name": "Easy",
                        "type": "difficulty"
                    },
                    {
                        "id": 65848,
                        "name": "stove_top",
                        "display_name": "Stove Top",
                        "type": "appliance"
                    },
                    {
                        "id": 64455,
                        "name": "korean",
                        "display_name": "Korean",
                        "type": "cuisine"
                    },
                    {
                        "id": 64462,
                        "name": "comfort_food",
                        "display_name": "Comfort Food",
                        "type": "dietary"
                    },
                    {
                        "id": 1280503,
                        "name": "cutting_board",
                        "display_name": "Cutting Board",
                        "type": "equipment"
                    },
                    {
                        "id": 64444,
                        "name": "american",
                        "display_name": "American",
                        "type": "cuisine"
                    },
                    {
                        "id": 1247769,
                        "name": "cheese_grater",
                        "display_name": "Cheese Grater",
                        "type": "equipment"
                    },
                    {
                        "type": "meal",
                        "id": 64489,
                        "name": "lunch",
                        "display_name": "Lunch"
                    },
                    {
                        "display_name": "Pan Fry",
                        "type": "method",
                        "id": 65859,
                        "name": "pan_fry"
                    },
                    {
                        "id": 64486,
                        "name": "dinner",
                        "display_name": "Dinner",
                        "type": "meal"
                    },
                    {
                        "id": 1247788,
                        "name": "spatula",
                        "display_name": "Spatula",
                        "type": "equipment"
                    },
                    {
                        "id": 3802077,
                        "name": "asian_pacific_american_heritage_month",
                        "display_name": "Asian Pacific American Heritage Month",
                        "type": "holiday"
                    }
                ],
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/52033da23b824b20a416cdd6b3277e91/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.mp4",
                "inspired_by_url": null,
                "is_one_top": false,
                "slug": "kimchi-grilled-cheese",
                "brand": null,
                "total_time_tier": {
                    "tier": "under_15_minutes",
                    "display_tier": "Under 15 minutes"
                },
                "video_id": 105482,
                "yields": "Servings: 1",
                "nutrition_visibility": "auto",
                "buzz_id": null,
                "description": "Give your grilled cheese a fiery update by sneaking in tangy, charred kimchi between layers of gooey American cheese and sharp cheddar cheese.",
                "id": 6201,
                "show_id": 17,
                "updated_at": 1590763265
            },
            {
                "promotion": "full",
                "tips_and_ratings_enabled": true,
                "total_time_minutes": 15,
                "updated_at": 1590763274,
                "aspect_ratio": "16:9",
                "buzz_id": null,
                "draft_status": "published",
                "id": 6202,
                "nutrition": {
                    "calories": 531,
                    "carbohydrates": 61,
                    "fat": 22,
                    "protein": 18,
                    "sugar": 0,
                    "fiber": 1,
                    "updated_at": "2020-05-28T08:06:28+02:00"
                },
                "canonical_id": "recipe:6202",
                "renditions": [
                    {
                        "width": 720,
                        "bit_rate": 1215,
                        "maximum_bit_rate": null,
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/168350/square_720/1590516270",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_720/1590516270_00001.png",
                        "name": "mp4_720x404",
                        "height": 404,
                        "duration": 424461,
                        "file_size": 64433716,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "container": "mp4"
                    },
                    {
                        "width": 320,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_320/1590516270_00001.png",
                        "name": "mp4_320x180",
                        "height": 180,
                        "duration": 424461,
                        "file_size": 23303407,
                        "bit_rate": 440,
                        "minimum_bit_rate": null,
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/168350/square_320/1590516270"
                    },
                    {
                        "height": 720,
                        "url": "https://vid.tasty.co/output/168350/landscape_720/1590516270",
                        "name": "mp4_1280x720",
                        "width": 1280,
                        "duration": 424461,
                        "file_size": 150190774,
                        "bit_rate": 2831,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "landscape",
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_720/1590516270_00001.png"
                    },
                    {
                        "duration": 424461,
                        "file_size": 54369542,
                        "bit_rate": 1025,
                        "aspect": "landscape",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/168350/landscape_480/1590516270",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_480/1590516270_00001.png",
                        "height": 360,
                        "width": 640,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "name": "mp4_640x360"
                    },
                    {
                        "width": 1920,
                        "duration": 424469,
                        "file_size": null,
                        "maximum_bit_rate": 5564,
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                        "height": 1080,
                        "bit_rate": null,
                        "minimum_bit_rate": 269,
                        "content_type": "application/vnd.apple.mpegurl",
                        "container": "ts",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/1445289064805-h2exzu/1590516270_00001.png",
                        "name": "low"
                    }
                ],
                "approved_at": 1590763274,
                "language": "eng",
                "name": "Kimchi, Spam, And Egg Scramble",
                "servings_noun_plural": "servings",
                "servings_noun_singular": "serving",
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/52033da23b824b20a416cdd6b3277e91/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.mp4",
                "yields": "Servings: 2",
                "beauty_url": null,
                "cook_time_minutes": 10,
                "keywords": "3 ways, apahm, asian american recipes, at home, cabbage, cafe maddy, cafemaddy, carbonara, cooking, corona virus, covid, creamy bacon spaghetti, delicious, easy, grilled cheese, heritage, instagram, kimchi, korea, korean, korean american, koreatown, napa cabbage, pasta, recipes, scramble, simply, spam and egg scramble, tik tok",
                "video_id": 105482,
                "is_one_top": false,
                "seo_title": "",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/fca25552e2a5422db63f912beed87335/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.jpg",
                "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                "nutrition_visibility": "auto",
                "tags": [
                    {
                        "id": 65848,
                        "name": "stove_top",
                        "display_name": "Stove Top",
                        "type": "appliance"
                    },
                    {
                        "id": 1280501,
                        "name": "chefs_knife",
                        "display_name": "Chef's Knife",
                        "type": "equipment"
                    },
                    {
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty"
                    },
                    {
                        "id": 64455,
                        "name": "korean",
                        "display_name": "Korean",
                        "type": "cuisine"
                    },
                    {
                        "id": 3802077,
                        "name": "asian_pacific_american_heritage_month",
                        "display_name": "Asian Pacific American Heritage Month",
                        "type": "holiday"
                    },
                    {
                        "name": "comfort_food",
                        "display_name": "Comfort Food",
                        "type": "dietary",
                        "id": 64462
                    },
                    {
                        "id": 64471,
                        "name": "easy",
                        "display_name": "Easy",
                        "type": "difficulty"
                    },
                    {
                        "id": 1280505,
                        "name": "kitchen_shears",
                        "display_name": "Kitchen Shears",
                        "type": "equipment"
                    },
                    {
                        "name": "cutting_board",
                        "display_name": "Cutting Board",
                        "type": "equipment",
                        "id": 1280503
                    },
                    {
                        "id": 64491,
                        "name": "snacks",
                        "display_name": "Snacks",
                        "type": "meal"
                    },
                    {
                        "id": 64486,
                        "name": "dinner",
                        "display_name": "Dinner",
                        "type": "meal"
                    },
                    {
                        "name": "pan_fry",
                        "display_name": "Pan Fry",
                        "type": "method",
                        "id": 65859
                    }
                ],
                "credits": [
                    {
                        "name": "Matt Ciampa",
                        "type": "internal"
                    }
                ],
                "inspired_by_url": null,
                "is_shoppable": true,
                "prep_time_minutes": 5,
                "slug": "kimchi-spam-and-egg-scramble",
                "video_ad_content": "none",
                "show": {
                    "id": 17,
                    "name": "Tasty"
                },
                "facebook_posts": [],
                "instructions": [
                    {
                        "appliance": null,
                        "id": 55562,
                        "display_text": "Dice the Spam into small cubes.",
                        "position": 1,
                        "start_time": 175000,
                        "end_time": 179166,
                        "temperature": null
                    },
                    {
                        "end_time": 186500,
                        "temperature": null,
                        "appliance": null,
                        "id": 55563,
                        "display_text": "Crack the eggs into a small bowl and beat to combine.",
                        "position": 2,
                        "start_time": 181000
                    },
                    {
                        "display_text": "Heat the olive oil in a medium nonstick skillet over medium-high heat. Add the Spam and cook until golden brown and crispy, stirring often, 3–4 minutes.",
                        "position": 3,
                        "start_time": 189000,
                        "end_time": 197166,
                        "temperature": null,
                        "appliance": null,
                        "id": 55564
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 55565,
                        "display_text": "Reduce the heat to medium-low, add the kimchi, and cook until fragrant, 2–3 minutes. Push mixture to one side of the pan and melt the butter in the empty side. Reduce the heat to low, add the eggs, and cook, stirring frequently, until scrambled to your desired doneness. Stir in the kimchi and spam until well combined.",
                        "position": 4,
                        "start_time": 198000,
                        "end_time": 224000
                    },
                    {
                        "display_text": "Add the rice to 2 serving bowls and divide the scrambled egg mixture between the bowls. Top with furikake, if desired.",
                        "position": 5,
                        "start_time": 225000,
                        "end_time": 234833,
                        "temperature": null,
                        "appliance": null,
                        "id": 55566
                    },
                    {
                        "display_text": "Enjoy!",
                        "position": 6,
                        "start_time": 229000,
                        "end_time": 231833,
                        "temperature": null,
                        "appliance": null,
                        "id": 55567
                    },
                    {
                        "id": 55568,
                        "display_text": "Recipe By: Madeline Park",
                        "position": 7,
                        "start_time": 0,
                        "end_time": 0,
                        "temperature": null,
                        "appliance": null
                    }
                ],
                "user_ratings": {
                    "count_negative": 0,
                    "score": null,
                    "count_positive": 0
                },
                "country": "US",
                "created_at": 1590515886,
                "num_servings": 2,
                "brand": null,
                "total_time_tier": {
                    "tier": "under_15_minutes",
                    "display_tier": "Under 15 minutes"
                },
                "sections": [
                    {
                        "components": [
                            {
                                "position": 1,
                                "measurements": [
                                    {
                                        "quantity": "½",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups"
                                        },
                                        "id": 567061
                                    }
                                ],
                                "ingredient": {
                                    "name": "Spam®",
                                    "display_singular": "Spam®",
                                    "display_plural": "Spam®s",
                                    "created_at": 1587047880,
                                    "updated_at": 1587047880,
                                    "id": 6384
                                },
                                "id": 68174,
                                "raw_text": "½ cup diced Spam®",
                                "extra_comment": "diced"
                            },
                            {
                                "id": 68175,
                                "raw_text": "3 large eggs",
                                "extra_comment": "",
                                "position": 2,
                                "measurements": [
                                    {
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        },
                                        "id": 567065,
                                        "quantity": "3"
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "large eggs",
                                    "created_at": 1494382414,
                                    "updated_at": 1509035275,
                                    "id": 253,
                                    "name": "large egg",
                                    "display_singular": "large egg"
                                }
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 567063,
                                        "quantity": "1",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "olive oils",
                                    "created_at": 1493306183,
                                    "updated_at": 1509035290,
                                    "id": 4,
                                    "name": "olive oil",
                                    "display_singular": "olive oil"
                                },
                                "id": 68176,
                                "raw_text": "1 tablespoon olive oil",
                                "extra_comment": "",
                                "position": 3
                            },
                            {
                                "id": 68177,
                                "raw_text": "¼ cup kimchi, diced",
                                "extra_comment": "diced",
                                "position": 4,
                                "measurements": [
                                    {
                                        "id": 567064,
                                        "quantity": "¼",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups"
                                        }
                                    },
                                    {
                                        "id": 567062,
                                        "quantity": "15",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 973,
                                    "name": "kimchi",
                                    "display_singular": "kimchi",
                                    "display_plural": "kimchis",
                                    "created_at": 1496170737,
                                    "updated_at": 1509035217
                                }
                            },
                            {
                                "id": 68178,
                                "raw_text": "1 tablespoon unsalted butter",
                                "extra_comment": "",
                                "position": 5,
                                "measurements": [
                                    {
                                        "id": 567067,
                                        "quantity": "1",
                                        "unit": {
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 291,
                                    "name": "unsalted butter",
                                    "display_singular": "unsalted butter",
                                    "display_plural": "unsalted butters",
                                    "created_at": 1494806355,
                                    "updated_at": 1509035272
                                }
                            },
                            {
                                "ingredient": {
                                    "display_singular": "white rice",
                                    "display_plural": "white rices",
                                    "created_at": 1495761680,
                                    "updated_at": 1509035230,
                                    "id": 819,
                                    "name": "white rice"
                                },
                                "id": 68179,
                                "raw_text": "2 cups steamed white rice",
                                "extra_comment": "steamed",
                                "position": 6,
                                "measurements": [
                                    {
                                        "id": 567070,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "id": 567069,
                                        "quantity": "400",
                                        "unit": {
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g"
                                        }
                                    }
                                ]
                            },
                            {
                                "id": 68180,
                                "raw_text": "Kosher salt, to taste",
                                "extra_comment": "to taste",
                                "position": 7,
                                "measurements": [
                                    {
                                        "id": 567068,
                                        "quantity": "0",
                                        "unit": {
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 11,
                                    "name": "kosher salt",
                                    "display_singular": "kosher salt",
                                    "display_plural": "kosher salts",
                                    "created_at": 1493307153,
                                    "updated_at": 1509035289
                                }
                            },
                            {
                                "id": 68181,
                                "raw_text": "Furikake, for garnish (optional)",
                                "extra_comment": "for garnish - optional",
                                "position": 8,
                                "measurements": [
                                    {
                                        "id": 567066,
                                        "quantity": "0",
                                        "unit": {
                                            "display_plural": "",
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1590592254,
                                    "id": 6461,
                                    "name": "furikake",
                                    "display_singular": "furikake",
                                    "display_plural": "furikakes",
                                    "created_at": 1590592254
                                }
                            }
                        ],
                        "name": null,
                        "position": 1
                    }
                ],
                "brand_id": null,
                "description": "This was a breakfast staple in my house growing up. My dad fries up Spam until crispy, adds some kimchi for flavor, and scrambles in a few eggs. We serve it over rice with seaweed seasoning, but salt and pepper work great too.",
                "show_id": 17,
                "compilations": [
                    {
                        "is_shoppable": false,
                        "show": [
                            {
                                "name": "Tasty",
                                "id": 17
                            }
                        ],
                        "draft_status": "published",
                        "keywords": null,
                        "video_id": 105482,
                        "canonical_id": "compilation:1495",
                        "facebook_posts": [],
                        "buzz_id": null,
                        "country": "US",
                        "language": "eng",
                        "name": "Cafemaddy Shows You 3 Creative Ways Of Using Kimchi",
                        "promotion": "full",
                        "slug": "cafemaddy-shows-you-3-creative-ways-of-using-kimchi",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/267181.jpg",
                        "created_at": 1590515886,
                        "id": 1495,
                        "beauty_url": null,
                        "description": null,
                        "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                        "approved_at": 1590763287,
                        "aspect_ratio": "16:9"
                    }
                ]
            }
        ],
        "updated_at": 1590763287,
        "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
        "show_id": 17,
        "canonical_id": "compilation:1495",
        "credits": [
            {
                "name": "Matt Ciampa",
                "type": "internal"
            }
        ],
        "is_shoppable": false,
        "keywords": "3 ways, apahm, asian american recipes, at home, cabbage, cafe maddy, cafemaddy, carbonara, cooking, corona virus, covid, creamy bacon spaghetti, delicious, easy, grilled cheese, heritage, instagram, kimchi, korea, korean, korean american, koreatown, napa cabbage, pasta, recipes, scramble, simply, spam and egg scramble, tik tok",
        "renditions": [
            {
                "width": 720,
                "duration": 424461,
                "file_size": 64433716,
                "bit_rate": 1215,
                "aspect": "landscape",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_720/1590516270_00001.png",
                "height": 404,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/square_720/1590516270",
                "name": "mp4_720x404",
                "maximum_bit_rate": null
            },
            {
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/168350/square_320/1590516270",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_320/1590516270_00001.png",
                "duration": 424461,
                "bit_rate": 440,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4",
                "name": "mp4_320x180",
                "height": 180,
                "width": 320,
                "file_size": 23303407,
                "minimum_bit_rate": null
            },
            {
                "height": 720,
                "file_size": 150190774,
                "bit_rate": 2831,
                "minimum_bit_rate": null,
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_720/1590516270_00001.png",
                "name": "mp4_1280x720",
                "width": 1280,
                "duration": 424461,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/168350/landscape_720/1590516270"
            },
            {
                "name": "mp4_640x360",
                "bit_rate": 1025,
                "maximum_bit_rate": null,
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_480/1590516270_00001.png",
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/168350/landscape_480/1590516270",
                "height": 360,
                "width": 640,
                "duration": 424461,
                "file_size": 54369542
            },
            {
                "maximum_bit_rate": 5564,
                "minimum_bit_rate": 269,
                "container": "ts",
                "url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                "height": 1080,
                "width": 1920,
                "duration": 424469,
                "bit_rate": null,
                "name": "low",
                "file_size": null,
                "content_type": "application/vnd.apple.mpegurl",
                "aspect": "landscape",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/1445289064805-h2exzu/1590516270_00001.png"
            }
        ],
        "beauty_url": null,
        "created_at": 1590515886,
        "name": "Cafemaddy Shows You 3 Creative Ways Of Using Kimchi",
        "slug": "cafemaddy-shows-you-3-creative-ways-of-using-kimchi",
        "tags": [
            {
                "id": 64471,
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty"
            },
            {
                "name": "cutting_board",
                "display_name": "Cutting Board",
                "type": "equipment",
                "id": 1280503
            },
            {
                "display_name": "Dinner",
                "type": "meal",
                "id": 64486,
                "name": "dinner"
            },
            {
                "id": 3802077,
                "name": "asian_pacific_american_heritage_month",
                "display_name": "Asian Pacific American Heritage Month",
                "type": "holiday"
            },
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "id": 65848,
                "name": "stove_top",
                "display_name": "Stove Top",
                "type": "appliance"
            },
            {
                "id": 64455,
                "name": "korean",
                "display_name": "Korean",
                "type": "cuisine"
            },
            {
                "type": "dietary",
                "id": 64462,
                "name": "comfort_food",
                "display_name": "Comfort Food"
            }
        ],
        "facebook_posts": [],
        "buzz_id": null,
        "description": null,
        "id": 1495,
        "promotion": "full",
        "approved_at": 1590763287,
        "aspect_ratio": "16:9",
        "draft_status": "published",
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "country": "US",
        "language": "eng"
    },
    {
        "sections": [
            {
                "components": [
                    {
                        "raw_text": "4 slices of bacon",
                        "extra_comment": "",
                        "position": 1,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "slice",
                                    "abbreviation": "slice",
                                    "display_singular": "slice",
                                    "display_plural": "slices",
                                    "system": "none"
                                },
                                "id": 567071,
                                "quantity": "4"
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035279,
                            "id": 214,
                            "name": "bacon",
                            "display_singular": "bacon",
                            "display_plural": "bacons",
                            "created_at": 1494212643
                        },
                        "id": 68182
                    },
                    {
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "unit": {
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": ""
                                },
                                "id": 567072,
                                "quantity": "1"
                            }
                        ],
                        "ingredient": {
                            "name": "medium shallot",
                            "display_singular": "medium shallot",
                            "display_plural": "medium shallots",
                            "created_at": 1540620139,
                            "updated_at": 1540620139,
                            "id": 4857
                        },
                        "id": 68183,
                        "raw_text": "1 medium shallot"
                    },
                    {
                        "id": 68184,
                        "raw_text": "1 clove garlic",
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 567074,
                                "quantity": "1",
                                "unit": {
                                    "name": "clove",
                                    "abbreviation": "clove",
                                    "display_singular": "clove",
                                    "display_plural": "cloves",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "garlics",
                            "created_at": 1493744766,
                            "updated_at": 1509035285,
                            "id": 95,
                            "name": "garlic",
                            "display_singular": "garlic"
                        }
                    },
                    {
                        "id": 68185,
                        "raw_text": "Kosher salt, to taste",
                        "extra_comment": "to taste",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 567073,
                                "quantity": "0",
                                "unit": {
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1493307153,
                            "updated_at": 1509035289,
                            "id": 11,
                            "name": "kosher salt",
                            "display_singular": "kosher salt",
                            "display_plural": "kosher salts"
                        }
                    },
                    {
                        "measurements": [
                            {
                                "quantity": "5",
                                "unit": {
                                    "name": "ounce",
                                    "abbreviation": "oz",
                                    "display_singular": "oz",
                                    "display_plural": "oz",
                                    "system": "imperial"
                                },
                                "id": 567076
                            },
                            {
                                "id": 567075,
                                "quantity": "140",
                                "unit": {
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "dried spathettis",
                            "created_at": 1526070693,
                            "updated_at": 1526070693,
                            "id": 4073,
                            "name": "dried spathetti",
                            "display_singular": "dried spathetti"
                        },
                        "id": 68186,
                        "raw_text": "5 ounces dried spaghetti or linguine",
                        "extra_comment": "or linguine",
                        "position": 5
                    },
                    {
                        "ingredient": {
                            "display_plural": "kimchis",
                            "created_at": 1496170737,
                            "updated_at": 1509035217,
                            "id": 973,
                            "name": "kimchi",
                            "display_singular": "kimchi"
                        },
                        "id": 68187,
                        "raw_text": "¼ cup kimchi, diced",
                        "extra_comment": "diced",
                        "position": 6,
                        "measurements": [
                            {
                                "quantity": "¼",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                },
                                "id": 567078
                            },
                            {
                                "quantity": "15",
                                "unit": {
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g"
                                },
                                "id": 567077
                            }
                        ]
                    },
                    {
                        "id": 68188,
                        "raw_text": "2 tablespoons unsalted butter",
                        "extra_comment": "",
                        "position": 7,
                        "measurements": [
                            {
                                "id": 567080,
                                "quantity": "2",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "unsalted butter",
                            "display_singular": "unsalted butter",
                            "display_plural": "unsalted butters",
                            "created_at": 1494806355,
                            "updated_at": 1509035272,
                            "id": 291
                        }
                    },
                    {
                        "extra_comment": "",
                        "position": 8,
                        "measurements": [
                            {
                                "id": 567084,
                                "quantity": "1",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            },
                            {
                                "id": 567083,
                                "quantity": "240",
                                "unit": {
                                    "display_plural": "mL",
                                    "system": "metric",
                                    "name": "milliliter",
                                    "abbreviation": "mL",
                                    "display_singular": "mL"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 221,
                            "name": "heavy cream",
                            "display_singular": "heavy cream",
                            "display_plural": "heavy creams",
                            "created_at": 1494214054,
                            "updated_at": 1509035278
                        },
                        "id": 68189,
                        "raw_text": "1 cup heavy cream"
                    },
                    {
                        "position": 9,
                        "measurements": [
                            {
                                "quantity": "½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                },
                                "id": 567082
                            },
                            {
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                },
                                "id": 567081,
                                "quantity": "55"
                            }
                        ],
                        "ingredient": {
                            "id": 1869,
                            "name": "grated parmesan cheese",
                            "display_singular": "grated parmesan cheese",
                            "display_plural": "grated parmesan cheeses",
                            "created_at": 1497741203,
                            "updated_at": 1509035159
                        },
                        "id": 68190,
                        "raw_text": "½ cup grated Parmesan cheese",
                        "extra_comment": ""
                    },
                    {
                        "raw_text": "2 tablespoons thinly sliced scallions, for garnish",
                        "extra_comment": "thinly sliced, for garnish",
                        "position": 10,
                        "measurements": [
                            {
                                "id": 567079,
                                "quantity": "2",
                                "unit": {
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "scallions",
                            "display_singular": "scallion",
                            "display_plural": "scallions",
                            "created_at": 1494803890,
                            "updated_at": 1509035273,
                            "id": 276
                        },
                        "id": 68191
                    },
                    {
                        "position": 11,
                        "measurements": [
                            {
                                "id": 567085,
                                "quantity": "0",
                                "unit": {
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1590592254,
                            "id": 6461,
                            "name": "furikake",
                            "display_singular": "furikake",
                            "display_plural": "furikakes",
                            "created_at": 1590592254
                        },
                        "id": 68192,
                        "raw_text": "Furikake, for garnish (optional)",
                        "extra_comment": "for garnish - optional"
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "facebook_posts": [],
        "is_shoppable": true,
        "promotion": "full",
        "tips_and_ratings_enabled": true,
        "aspect_ratio": "16:9",
        "beauty_url": null,
        "cook_time_minutes": 25,
        "id": 6203,
        "inspired_by_url": null,
        "instructions": [
            {
                "position": 1,
                "start_time": 269833,
                "end_time": 275666,
                "temperature": null,
                "appliance": null,
                "id": 55569,
                "display_text": "Dice the bacon, mince the shallots, and grate the garlic on a microplane."
            },
            {
                "start_time": 277000,
                "end_time": 285000,
                "temperature": null,
                "appliance": null,
                "id": 55570,
                "display_text": "Bring a large pot of salted water to a boil. Add the pasta and cook according to the package instructions until al dente. Reserve 1 cup of the cooking water, then drain the pasta.",
                "position": 2
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55571,
                "display_text": "Heat a large skillet over medium-high heat. Add the bacon and cook until crispy, 5–7 minutes. Using a slotted spoon, transfer the bacon to a paper towel-lined plate to drain. Discard all but 1 tablespoon of the rendered bacon fat.",
                "position": 3,
                "start_time": 286000,
                "end_time": 292166
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55572,
                "display_text": "Reduce the heat to medium and add the shallot to the pan with the bacon fat. Cook until softened, 3–5 minutes. Add the garlic and kimchi and sauté until fragrant, about 2 minutes.",
                "position": 4,
                "start_time": 298000,
                "end_time": 311333
            },
            {
                "display_text": "Add the butter and heavy cream to the pan. Season with salt. Bring to a low simmer and cook until bubbling. Add the cooked pasta and toss until well coated.",
                "position": 5,
                "start_time": 314000,
                "end_time": 331833,
                "temperature": null,
                "appliance": null,
                "id": 55573
            },
            {
                "position": 6,
                "start_time": 333000,
                "end_time": 344166,
                "temperature": null,
                "appliance": null,
                "id": 55574,
                "display_text": "While stirring constantly, gradually add the Parmesan cheese, mixing until melted. If the sauce gets too thick, add a little bit of the reserved pasta water as needed. Continue stirring until all of the cheese has been added and the sauce is creamy. Add some of the crispy bacon, reserving some for garnish, and stir to incorporate."
            },
            {
                "display_text": "Divide the pasta between 2 serving bowls and top with the reserved bacon, scallions, and furikake, if using.",
                "position": 7,
                "start_time": 345000,
                "end_time": 355500,
                "temperature": null,
                "appliance": null,
                "id": 55575
            },
            {
                "start_time": 356000,
                "end_time": 367500,
                "temperature": null,
                "appliance": null,
                "id": 55576,
                "display_text": "Enjoy!",
                "position": 8
            }
        ],
        "canonical_id": "recipe:6203",
        "renditions": [
            {
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/square_720/1590516270",
                "width": 720,
                "file_size": 64433716,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "name": "mp4_720x404",
                "height": 404,
                "duration": 424461,
                "bit_rate": 1215,
                "aspect": "landscape",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_720/1590516270_00001.png"
            },
            {
                "height": 180,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/square_320/1590516270",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_320/1590516270_00001.png",
                "name": "mp4_320x180",
                "width": 320,
                "duration": 424461,
                "file_size": 23303407,
                "bit_rate": 440,
                "content_type": "video/mp4",
                "aspect": "landscape"
            },
            {
                "height": 720,
                "duration": 424461,
                "bit_rate": 2831,
                "container": "mp4",
                "name": "mp4_1280x720",
                "url": "https://vid.tasty.co/output/168350/landscape_720/1590516270",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_720/1590516270_00001.png",
                "width": 1280,
                "file_size": 150190774,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape"
            },
            {
                "file_size": 54369542,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "aspect": "landscape",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_480/1590516270_00001.png",
                "width": 640,
                "duration": 424461,
                "bit_rate": 1025,
                "content_type": "video/mp4",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/landscape_480/1590516270",
                "name": "mp4_640x360",
                "height": 360
            },
            {
                "width": 1920,
                "duration": 424469,
                "bit_rate": null,
                "minimum_bit_rate": 269,
                "container": "ts",
                "name": "low",
                "height": 1080,
                "file_size": null,
                "maximum_bit_rate": 5564,
                "content_type": "application/vnd.apple.mpegurl",
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/1445289064805-h2exzu/1590516270_00001.png"
            }
        ],
        "created_at": 1590515948,
        "seo_title": "",
        "total_time_minutes": 35,
        "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
        "video_ad_content": "none",
        "total_time_tier": {
            "tier": "under_45_minutes",
            "display_tier": "Under 45 minutes"
        },
        "tags": [
            {
                "name": "cheese_grater",
                "display_name": "Cheese Grater",
                "type": "equipment",
                "id": 1247769
            },
            {
                "id": 1280503,
                "name": "cutting_board",
                "display_name": "Cutting Board",
                "type": "equipment"
            },
            {
                "id": 64462,
                "name": "comfort_food",
                "display_name": "Comfort Food",
                "type": "dietary"
            },
            {
                "display_name": "Italian",
                "type": "cuisine",
                "id": 64453,
                "name": "italian"
            },
            {
                "display_name": "Colander",
                "type": "equipment",
                "id": 1247770,
                "name": "colander"
            },
            {
                "id": 65848,
                "name": "stove_top",
                "display_name": "Stove Top",
                "type": "appliance"
            },
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "type": "cuisine",
                "id": 64455,
                "name": "korean",
                "display_name": "Korean"
            },
            {
                "id": 64471,
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty"
            },
            {
                "name": "strainer",
                "display_name": "Strainer",
                "type": "equipment",
                "id": 1247789
            },
            {
                "display_name": "Dinner",
                "type": "meal",
                "id": 64486,
                "name": "dinner"
            },
            {
                "id": 3802077,
                "name": "asian_pacific_american_heritage_month",
                "display_name": "Asian Pacific American Heritage Month",
                "type": "holiday"
            }
        ],
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "description": "Kimchi is the secret ingredient you never knew your creamy pasta dishes were missing. It adds acidity to balance the heaviness of the cream and Parmesan, and a delightful burst of tangy flavor. Give it a try, you won’t be sorry!",
        "num_servings": 2,
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/a859957c483f4dcd886e1799025b5693/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.jpg",
        "updated_at": 1590763279,
        "compilations": [
            {
                "created_at": 1590515886,
                "is_shoppable": false,
                "language": "eng",
                "promotion": "full",
                "beauty_url": null,
                "country": "US",
                "description": null,
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/267181.jpg",
                "canonical_id": "compilation:1495",
                "approved_at": 1590763287,
                "draft_status": "published",
                "keywords": null,
                "slug": "cafemaddy-shows-you-3-creative-ways-of-using-kimchi",
                "video_id": 105482,
                "facebook_posts": [],
                "buzz_id": null,
                "id": 1495,
                "name": "Cafemaddy Shows You 3 Creative Ways Of Using Kimchi",
                "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                "show": [
                    {
                        "id": 17,
                        "name": "Tasty"
                    }
                ],
                "aspect_ratio": "16:9"
            }
        ],
        "nutrition": {
            "sugar": 7,
            "fiber": 3,
            "updated_at": "2020-05-28T08:06:28+02:00",
            "calories": 915,
            "carbohydrates": 61,
            "fat": 71,
            "protein": 23
        },
        "slug": "creamy-kimchi-bacon-spaghetti",
        "video_id": 105482,
        "approved_at": 1590763279,
        "country": "US",
        "is_one_top": false,
        "servings_noun_plural": "servings",
        "servings_noun_singular": "serving",
        "nutrition_visibility": "auto",
        "brand_id": null,
        "buzz_id": null,
        "draft_status": "published",
        "language": "eng",
        "name": "Creamy Kimchi Bacon Spaghetti",
        "credits": [
            {
                "name": "Matt Ciampa",
                "type": "internal"
            }
        ],
        "user_ratings": {
            "count_positive": 33,
            "count_negative": 10,
            "score": 0.767442
        },
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/52033da23b824b20a416cdd6b3277e91/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.mp4",
        "keywords": "3 ways, apahm, asian american recipes, at home, cabbage, cafe maddy, cafemaddy, carbonara, cooking, corona virus, covid, creamy bacon spaghetti, delicious, easy, grilled cheese, heritage, instagram, kimchi, korea, korean, korean american, koreatown, napa cabbage, pasta, recipes, scramble, simply, spam and egg scramble, tik tok",
        "prep_time_minutes": 10,
        "show_id": 17,
        "yields": "Servings: 2",
        "brand": null
    },
    {
        "seo_title": "",
        "total_time_tier": {
            "tier": "under_15_minutes",
            "display_tier": "Under 15 minutes"
        },
        "aspect_ratio": "16:9",
        "description": "This was a breakfast staple in my house growing up. My dad fries up Spam until crispy, adds some kimchi for flavor, and scrambles in a few eggs. We serve it over rice with seaweed seasoning, but salt and pepper work great too.",
        "inspired_by_url": null,
        "name": "Kimchi, Spam, And Egg Scramble",
        "country": "US",
        "id": 6202,
        "is_one_top": false,
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/52033da23b824b20a416cdd6b3277e91/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.mp4",
        "video_id": 105482,
        "tags": [
            {
                "name": "stove_top",
                "display_name": "Stove Top",
                "type": "appliance",
                "id": 65848
            },
            {
                "id": 1280501,
                "name": "chefs_knife",
                "display_name": "Chef's Knife",
                "type": "equipment"
            },
            {
                "display_name": "Under 30 Minutes",
                "type": "difficulty",
                "id": 64472,
                "name": "under_30_minutes"
            },
            {
                "id": 64455,
                "name": "korean",
                "display_name": "Korean",
                "type": "cuisine"
            },
            {
                "id": 3802077,
                "name": "asian_pacific_american_heritage_month",
                "display_name": "Asian Pacific American Heritage Month",
                "type": "holiday"
            },
            {
                "name": "comfort_food",
                "display_name": "Comfort Food",
                "type": "dietary",
                "id": 64462
            },
            {
                "id": 64471,
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty"
            },
            {
                "id": 1280505,
                "name": "kitchen_shears",
                "display_name": "Kitchen Shears",
                "type": "equipment"
            },
            {
                "id": 1280503,
                "name": "cutting_board",
                "display_name": "Cutting Board",
                "type": "equipment"
            },
            {
                "id": 64491,
                "name": "snacks",
                "display_name": "Snacks",
                "type": "meal"
            },
            {
                "id": 64486,
                "name": "dinner",
                "display_name": "Dinner",
                "type": "meal"
            },
            {
                "id": 65859,
                "name": "pan_fry",
                "display_name": "Pan Fry",
                "type": "method"
            }
        ],
        "draft_status": "published",
        "promotion": "full",
        "servings_noun_plural": "servings",
        "show_id": 17,
        "num_servings": 2,
        "prep_time_minutes": 5,
        "total_time_minutes": 15,
        "updated_at": 1590763274,
        "beauty_url": null,
        "brand_id": null,
        "is_shoppable": true,
        "keywords": "3 ways, apahm, asian american recipes, at home, cabbage, cafe maddy, cafemaddy, carbonara, cooking, corona virus, covid, creamy bacon spaghetti, delicious, easy, grilled cheese, heritage, instagram, kimchi, korea, korean, korean american, koreatown, napa cabbage, pasta, recipes, scramble, simply, spam and egg scramble, tik tok",
        "facebook_posts": [],
        "nutrition_visibility": "auto",
        "compilations": [
            {
                "show": [
                    {
                        "name": "Tasty",
                        "id": 17
                    }
                ],
                "beauty_url": null,
                "canonical_id": "compilation:1495",
                "facebook_posts": [],
                "is_shoppable": false,
                "keywords": null,
                "language": "eng",
                "promotion": "full",
                "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                "country": "US",
                "created_at": 1590515886,
                "draft_status": "published",
                "approved_at": 1590763287,
                "id": 1495,
                "name": "Cafemaddy Shows You 3 Creative Ways Of Using Kimchi",
                "slug": "cafemaddy-shows-you-3-creative-ways-of-using-kimchi",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/267181.jpg",
                "video_id": 105482,
                "aspect_ratio": "16:9",
                "buzz_id": null,
                "description": null
            }
        ],
        "instructions": [
            {
                "position": 1,
                "start_time": 175000,
                "end_time": 179166,
                "temperature": null,
                "appliance": null,
                "id": 55562,
                "display_text": "Dice the Spam into small cubes."
            },
            {
                "display_text": "Crack the eggs into a small bowl and beat to combine.",
                "position": 2,
                "start_time": 181000,
                "end_time": 186500,
                "temperature": null,
                "appliance": null,
                "id": 55563
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55564,
                "display_text": "Heat the olive oil in a medium nonstick skillet over medium-high heat. Add the Spam and cook until golden brown and crispy, stirring often, 3–4 minutes.",
                "position": 3,
                "start_time": 189000,
                "end_time": 197166
            },
            {
                "end_time": 224000,
                "temperature": null,
                "appliance": null,
                "id": 55565,
                "display_text": "Reduce the heat to medium-low, add the kimchi, and cook until fragrant, 2–3 minutes. Push mixture to one side of the pan and melt the butter in the empty side. Reduce the heat to low, add the eggs, and cook, stirring frequently, until scrambled to your desired doneness. Stir in the kimchi and spam until well combined.",
                "position": 4,
                "start_time": 198000
            },
            {
                "id": 55566,
                "display_text": "Add the rice to 2 serving bowls and divide the scrambled egg mixture between the bowls. Top with furikake, if desired.",
                "position": 5,
                "start_time": 225000,
                "end_time": 234833,
                "temperature": null,
                "appliance": null
            },
            {
                "appliance": null,
                "id": 55567,
                "display_text": "Enjoy!",
                "position": 6,
                "start_time": 229000,
                "end_time": 231833,
                "temperature": null
            },
            {
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55568,
                "display_text": "Recipe By: Madeline Park",
                "position": 7
            }
        ],
        "user_ratings": {
            "count_positive": 32,
            "count_negative": 6,
            "score": 0.842105
        },
        "nutrition": {
            "protein": 18,
            "sugar": 0,
            "fiber": 1,
            "updated_at": "2020-05-28T08:06:28+02:00",
            "calories": 531,
            "carbohydrates": 61,
            "fat": 22
        },
        "created_at": 1590515886,
        "tips_and_ratings_enabled": true,
        "video_ad_content": "none",
        "renditions": [
            {
                "bit_rate": 1215,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_720/1590516270_00001.png",
                "height": 404,
                "file_size": 64433716,
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/square_720/1590516270",
                "name": "mp4_720x404",
                "width": 720,
                "duration": 424461
            },
            {
                "name": "mp4_320x180",
                "height": 180,
                "duration": 424461,
                "bit_rate": 440,
                "minimum_bit_rate": null,
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/168350/square_320/1590516270",
                "width": 320,
                "file_size": 23303407,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_320/1590516270_00001.png"
            },
            {
                "name": "mp4_1280x720",
                "width": 1280,
                "duration": 424461,
                "aspect": "landscape",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_720/1590516270_00001.png",
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/landscape_720/1590516270",
                "height": 720,
                "file_size": 150190774,
                "bit_rate": 2831,
                "maximum_bit_rate": null
            },
            {
                "container": "mp4",
                "width": 640,
                "file_size": 54369542,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/168350/landscape_480/1590516270",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_480/1590516270_00001.png",
                "height": 360,
                "duration": 424461,
                "bit_rate": 1025,
                "name": "mp4_640x360"
            },
            {
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/1445289064805-h2exzu/1590516270_00001.png",
                "name": "low",
                "width": 1920,
                "duration": 424469,
                "file_size": null,
                "maximum_bit_rate": 5564,
                "container": "ts",
                "url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                "height": 1080,
                "bit_rate": null,
                "minimum_bit_rate": 269,
                "content_type": "application/vnd.apple.mpegurl",
                "aspect": "landscape"
            }
        ],
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/fca25552e2a5422db63f912beed87335/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.jpg",
        "yields": "Servings: 2",
        "sections": [
            {
                "name": null,
                "position": 1,
                "components": [
                    {
                        "id": 68174,
                        "raw_text": "½ cup diced Spam®",
                        "extra_comment": "diced",
                        "position": 1,
                        "measurements": [
                            {
                                "id": 567061,
                                "quantity": "½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 6384,
                            "name": "Spam®",
                            "display_singular": "Spam®",
                            "display_plural": "Spam®s",
                            "created_at": 1587047880,
                            "updated_at": 1587047880
                        }
                    },
                    {
                        "id": 68175,
                        "raw_text": "3 large eggs",
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "id": 567065,
                                "quantity": "3",
                                "unit": {
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "large egg",
                            "display_plural": "large eggs",
                            "created_at": 1494382414,
                            "updated_at": 1509035275,
                            "id": 253,
                            "name": "large egg"
                        }
                    },
                    {
                        "id": 68176,
                        "raw_text": "1 tablespoon olive oil",
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 567063,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "olive oil",
                            "display_singular": "olive oil",
                            "display_plural": "olive oils",
                            "created_at": 1493306183,
                            "updated_at": 1509035290,
                            "id": 4
                        }
                    },
                    {
                        "extra_comment": "diced",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 567064,
                                "quantity": "¼",
                                "unit": {
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup"
                                }
                            },
                            {
                                "id": 567062,
                                "quantity": "15",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "kimchis",
                            "created_at": 1496170737,
                            "updated_at": 1509035217,
                            "id": 973,
                            "name": "kimchi",
                            "display_singular": "kimchi"
                        },
                        "id": 68177,
                        "raw_text": "¼ cup kimchi, diced"
                    },
                    {
                        "id": 68178,
                        "raw_text": "1 tablespoon unsalted butter",
                        "extra_comment": "",
                        "position": 5,
                        "measurements": [
                            {
                                "id": 567067,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "unsalted butter",
                            "display_singular": "unsalted butter",
                            "display_plural": "unsalted butters",
                            "created_at": 1494806355,
                            "updated_at": 1509035272,
                            "id": 291
                        }
                    },
                    {
                        "ingredient": {
                            "display_singular": "white rice",
                            "display_plural": "white rices",
                            "created_at": 1495761680,
                            "updated_at": 1509035230,
                            "id": 819,
                            "name": "white rice"
                        },
                        "id": 68179,
                        "raw_text": "2 cups steamed white rice",
                        "extra_comment": "steamed",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 567070,
                                "quantity": "2",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            },
                            {
                                "quantity": "400",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                },
                                "id": 567069
                            }
                        ]
                    },
                    {
                        "ingredient": {
                            "id": 11,
                            "name": "kosher salt",
                            "display_singular": "kosher salt",
                            "display_plural": "kosher salts",
                            "created_at": 1493307153,
                            "updated_at": 1509035289
                        },
                        "id": 68180,
                        "raw_text": "Kosher salt, to taste",
                        "extra_comment": "to taste",
                        "position": 7,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                },
                                "id": 567068,
                                "quantity": "0"
                            }
                        ]
                    },
                    {
                        "id": 68181,
                        "raw_text": "Furikake, for garnish (optional)",
                        "extra_comment": "for garnish - optional",
                        "position": 8,
                        "measurements": [
                            {
                                "id": 567066,
                                "quantity": "0",
                                "unit": {
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1590592254,
                            "id": 6461,
                            "name": "furikake",
                            "display_singular": "furikake",
                            "display_plural": "furikakes",
                            "created_at": 1590592254
                        }
                    }
                ]
            }
        ],
        "canonical_id": "recipe:6202",
        "buzz_id": null,
        "cook_time_minutes": 10,
        "servings_noun_singular": "serving",
        "slug": "kimchi-spam-and-egg-scramble",
        "approved_at": 1590763274,
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "language": "eng",
        "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
        "brand": null,
        "credits": [
            {
                "name": "Matt Ciampa",
                "type": "internal"
            }
        ]
    },
    {
        "updated_at": 1590763265,
        "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
        "yields": "Servings: 1",
        "brand_id": null,
        "is_shoppable": true,
        "servings_noun_singular": "serving",
        "sections": [
            {
                "components": [
                    {
                        "id": 68167,
                        "raw_text": "½ cup shredded sharp cheddar cheese, divided",
                        "extra_comment": "divided",
                        "position": 1,
                        "measurements": [
                            {
                                "id": 567057,
                                "quantity": "½",
                                "unit": {
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup"
                                }
                            },
                            {
                                "id": 567053,
                                "quantity": "50",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 2952,
                            "name": "shredded sharp cheddar cheese",
                            "display_singular": "shredded sharp cheddar cheese",
                            "display_plural": "shredded sharp cheddar cheeses",
                            "created_at": 1504664792,
                            "updated_at": 1509035098
                        }
                    },
                    {
                        "raw_text": "¼ cup kimchi, drained",
                        "extra_comment": "drained",
                        "position": 2,
                        "measurements": [
                            {
                                "quantity": "¼",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                },
                                "id": 567059
                            },
                            {
                                "id": 567058,
                                "quantity": "15",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "kimchi",
                            "display_singular": "kimchi",
                            "display_plural": "kimchis",
                            "created_at": 1496170737,
                            "updated_at": 1509035217,
                            "id": 973
                        },
                        "id": 68168
                    },
                    {
                        "id": 68169,
                        "raw_text": "2 pieces of Italian bread",
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 567054,
                                "quantity": "2",
                                "unit": {
                                    "system": "none",
                                    "name": "piece",
                                    "abbreviation": "piece",
                                    "display_singular": "piece",
                                    "display_plural": "pieces"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1500491791,
                            "updated_at": 1509035132,
                            "id": 2291,
                            "name": "italian bread",
                            "display_singular": "italian bread",
                            "display_plural": "italian breads"
                        }
                    },
                    {
                        "raw_text": "4 tablespoons (½ stick) unsalted butter",
                        "extra_comment": "1/2 stick",
                        "position": 4,
                        "measurements": [
                            {
                                "unit": {
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon"
                                },
                                "id": 567052,
                                "quantity": "4"
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035272,
                            "id": 291,
                            "name": "unsalted butter",
                            "display_singular": "unsalted butter",
                            "display_plural": "unsalted butters",
                            "created_at": 1494806355
                        },
                        "id": 68170
                    },
                    {
                        "position": 5,
                        "measurements": [
                            {
                                "id": 567055,
                                "quantity": "1",
                                "unit": {
                                    "system": "imperial",
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "olive oils",
                            "created_at": 1493306183,
                            "updated_at": 1509035290,
                            "id": 4,
                            "name": "olive oil",
                            "display_singular": "olive oil"
                        },
                        "id": 68171,
                        "raw_text": "1 teaspoon olive oil",
                        "extra_comment": ""
                    },
                    {
                        "id": 68172,
                        "raw_text": "1 teaspoon sugar",
                        "extra_comment": "",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 567056,
                                "quantity": "1",
                                "unit": {
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "sugars",
                            "created_at": 1493314650,
                            "updated_at": 1509035288,
                            "id": 24,
                            "name": "sugar",
                            "display_singular": "sugar"
                        }
                    },
                    {
                        "position": 7,
                        "measurements": [
                            {
                                "id": 567060,
                                "quantity": "2",
                                "unit": {
                                    "name": "slice",
                                    "abbreviation": "slice",
                                    "display_singular": "slice",
                                    "display_plural": "slices",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "american cheeses",
                            "created_at": 1495665885,
                            "updated_at": 1509035240,
                            "id": 697,
                            "name": "american cheese",
                            "display_singular": "american cheese"
                        },
                        "id": 68173,
                        "raw_text": "2 slices of American cheese",
                        "extra_comment": ""
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "renditions": [
            {
                "height": 404,
                "file_size": 64433716,
                "bit_rate": 1215,
                "aspect": "landscape",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/square_720/1590516270",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_720/1590516270_00001.png",
                "name": "mp4_720x404",
                "width": 720,
                "duration": 424461,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4"
            },
            {
                "height": 180,
                "width": 320,
                "duration": 424461,
                "minimum_bit_rate": null,
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/square_320/1590516270",
                "name": "mp4_320x180",
                "file_size": 23303407,
                "bit_rate": 440,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/square_320/1590516270_00001.png"
            },
            {
                "duration": 424461,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "name": "mp4_1280x720",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/landscape_720/1590516270",
                "height": 720,
                "width": 1280,
                "file_size": 150190774,
                "bit_rate": 2831,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_720/1590516270_00001.png"
            },
            {
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/168350/landscape_480/1590516270",
                "width": 640,
                "duration": 424461,
                "bit_rate": 1025,
                "maximum_bit_rate": null,
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/landscape_480/1590516270_00001.png",
                "name": "mp4_640x360",
                "height": 360,
                "file_size": 54369542
            },
            {
                "maximum_bit_rate": 5564,
                "minimum_bit_rate": 269,
                "content_type": "application/vnd.apple.mpegurl",
                "width": 1920,
                "duration": 424469,
                "file_size": null,
                "bit_rate": null,
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/168350/1445289064805-h2exzu/1590516270_00001.png",
                "name": "low",
                "height": 1080,
                "aspect": "landscape",
                "container": "ts",
                "url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8"
            }
        ],
        "description": "Give your grilled cheese a fiery update by sneaking in tangy, charred kimchi between layers of gooey American cheese and sharp cheddar cheese.",
        "keywords": "3 ways, apahm, asian american recipes, at home, cabbage, cafe maddy, cafemaddy, carbonara, cooking, corona virus, covid, creamy bacon spaghetti, delicious, easy, grilled cheese, heritage, instagram, kimchi, korea, korean, korean american, koreatown, napa cabbage, pasta, recipes, scramble, simply, spam and egg scramble, tik tok",
        "video_id": 105482,
        "total_time_tier": {
            "display_tier": "Under 15 minutes",
            "tier": "under_15_minutes"
        },
        "tags": [
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "id": 1280505,
                "name": "kitchen_shears",
                "display_name": "Kitchen Shears",
                "type": "equipment"
            },
            {
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty",
                "id": 64471
            },
            {
                "type": "appliance",
                "id": 65848,
                "name": "stove_top",
                "display_name": "Stove Top"
            },
            {
                "name": "korean",
                "display_name": "Korean",
                "type": "cuisine",
                "id": 64455
            },
            {
                "id": 64462,
                "name": "comfort_food",
                "display_name": "Comfort Food",
                "type": "dietary"
            },
            {
                "name": "cutting_board",
                "display_name": "Cutting Board",
                "type": "equipment",
                "id": 1280503
            },
            {
                "id": 64444,
                "name": "american",
                "display_name": "American",
                "type": "cuisine"
            },
            {
                "name": "cheese_grater",
                "display_name": "Cheese Grater",
                "type": "equipment",
                "id": 1247769
            },
            {
                "id": 64489,
                "name": "lunch",
                "display_name": "Lunch",
                "type": "meal"
            },
            {
                "id": 65859,
                "name": "pan_fry",
                "display_name": "Pan Fry",
                "type": "method"
            },
            {
                "id": 64486,
                "name": "dinner",
                "display_name": "Dinner",
                "type": "meal"
            },
            {
                "id": 1247788,
                "name": "spatula",
                "display_name": "Spatula",
                "type": "equipment"
            },
            {
                "display_name": "Asian Pacific American Heritage Month",
                "type": "holiday",
                "id": 3802077,
                "name": "asian_pacific_american_heritage_month"
            }
        ],
        "approved_at": 1590763265,
        "draft_status": "published",
        "promotion": "full",
        "language": "eng",
        "instructions": [
            {
                "end_time": 53333,
                "temperature": null,
                "appliance": null,
                "id": 55554,
                "display_text": "Shred the cheddar cheese on the large holes of a box grater.",
                "position": 1,
                "start_time": 49000
            },
            {
                "id": 55555,
                "display_text": "Add the kimchi to a small bowl and use kitchen shears to cut into small pieces.",
                "position": 2,
                "start_time": 61000,
                "end_time": 64333,
                "temperature": null,
                "appliance": null
            },
            {
                "appliance": null,
                "id": 55556,
                "display_text": "Butter both sides of each slice of bread.",
                "position": 3,
                "start_time": 65000,
                "end_time": 68000,
                "temperature": null
            },
            {
                "appliance": null,
                "id": 55557,
                "display_text": "Heat the olive oil in a medium nonstick skillet over medium-high heat. Add the kimchi and cook until softened, about 2–3 minutes. Add the sugar and cook for an additional minute, stirring to incorporate. Transfer the kimchi to a bowl and wipe out the skillet.",
                "position": 4,
                "start_time": 70000,
                "end_time": 92666,
                "temperature": null
            },
            {
                "display_text": "Turn the heat to medium and add both slices of butter bread to the skillet. Cook until the bottom is golden brown, 3–4 minutes. Flip the bread, then add the American cheese to one slice and all but 2 tablespoons of the cheddar cheese to the other slice. Top the American cheese with the kimchi and remaining 2 tablespoons cheddar. Cook until the cheese starts to melt and the underside of the bread begins to turn golden, 3–4 minutes.",
                "position": 5,
                "start_time": 94000,
                "end_time": 117500,
                "temperature": null,
                "appliance": null,
                "id": 55558
            },
            {
                "display_text": "Sandwich the slices of bread together and cook, flipping once, for another 2–3 minutes, until the cheese is gooey and fully melted and the bread is well-toasted.",
                "position": 6,
                "start_time": 119000,
                "end_time": 125000,
                "temperature": null,
                "appliance": null,
                "id": 55559
            },
            {
                "appliance": null,
                "id": 55560,
                "display_text": "Transfer to a cutting board and cut in half.",
                "position": 7,
                "start_time": 130000,
                "end_time": 136166,
                "temperature": null
            },
            {
                "display_text": "Enjoy!",
                "position": 8,
                "start_time": 137000,
                "end_time": 140000,
                "temperature": null,
                "appliance": null,
                "id": 55561
            }
        ],
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/52033da23b824b20a416cdd6b3277e91/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.mp4",
        "buzz_id": null,
        "id": 6201,
        "inspired_by_url": null,
        "brand": null,
        "video_ad_content": "none",
        "aspect_ratio": "16:9",
        "beauty_url": null,
        "total_time_minutes": 15,
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "nutrition": {},
        "created_at": 1590515831,
        "num_servings": 1,
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/10942a0a6c214b649c1405a4fc00c5d2/BFV67735_TiktokSCafemaddyShowsYou3CreativeWaysOfUsingKimchi_FINAL_YT.jpg",
        "credits": [
            {
                "name": "Matt Ciampa",
                "type": "internal"
            }
        ],
        "country": "US",
        "seo_title": "",
        "nutrition_visibility": "auto",
        "prep_time_minutes": 5,
        "servings_noun_plural": "servings",
        "show_id": 17,
        "slug": "kimchi-grilled-cheese",
        "tips_and_ratings_enabled": true,
        "cook_time_minutes": 10,
        "is_one_top": false,
        "name": "Kimchi Grilled Cheese",
        "facebook_posts": [],
        "compilations": [
            {
                "draft_status": "published",
                "is_shoppable": false,
                "keywords": null,
                "slug": "cafemaddy-shows-you-3-creative-ways-of-using-kimchi",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/267181.jpg",
                "canonical_id": "compilation:1495",
                "facebook_posts": [],
                "created_at": 1590515886,
                "country": "US",
                "id": 1495,
                "aspect_ratio": "16:9",
                "language": "eng",
                "promotion": "full",
                "video_id": 105482,
                "buzz_id": null,
                "beauty_url": null,
                "description": null,
                "name": "Cafemaddy Shows You 3 Creative Ways Of Using Kimchi",
                "video_url": "https://vid.tasty.co/output/168350/hls24_1590516270.m3u8",
                "show": [
                    {
                        "id": 17,
                        "name": "Tasty"
                    }
                ],
                "approved_at": 1590763287
            }
        ],
        "user_ratings": {
            "count_positive": 28,
            "count_negative": 3,
            "score": 0.903226
        },
        "canonical_id": "recipe:6201"
    },
    {
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266749.jpg",
        "promotion": "full",
        "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
        "slug": "how-to-bbq-on-a-stove-top-grill",
        "recipes": [
            {
                "nutrition_visibility": "auto",
                "sections": [
                    {
                        "components": [
                            {
                                "raw_text": "2 tablespoons unsalted butter, softened",
                                "extra_comment": "softened",
                                "position": 1,
                                "measurements": [
                                    {
                                        "unit": {
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon"
                                        },
                                        "id": 566534,
                                        "quantity": "2"
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "unsalted butters",
                                    "created_at": 1494806355,
                                    "updated_at": 1509035272,
                                    "id": 291,
                                    "name": "unsalted butter",
                                    "display_singular": "unsalted butter"
                                },
                                "id": 68042
                            },
                            {
                                "id": 68043,
                                "raw_text": "2 tablespoons lemon zest",
                                "extra_comment": "",
                                "position": 2,
                                "measurements": [
                                    {
                                        "id": 566536,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 194,
                                    "name": "lemon zest",
                                    "display_singular": "lemon zest",
                                    "display_plural": "lemon zests",
                                    "created_at": 1494124243,
                                    "updated_at": 1518723289
                                }
                            },
                            {
                                "position": 3,
                                "measurements": [
                                    {
                                        "id": 566537,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 6450,
                                    "name": "lemon pepper seasoning",
                                    "display_singular": "lemon pepper seasoning",
                                    "display_plural": "lemon pepper seasonings",
                                    "created_at": 1590154281,
                                    "updated_at": 1590154281
                                },
                                "id": 68044,
                                "raw_text": "1 tablespoon lemon pepper seasoning",
                                "extra_comment": ""
                            },
                            {
                                "ingredient": {
                                    "display_singular": "nonstick cooking spray",
                                    "display_plural": "nonstick cooking sprays",
                                    "created_at": 1520176895,
                                    "updated_at": 1520176895,
                                    "id": 3826,
                                    "name": "nonstick cooking spray"
                                },
                                "id": 68045,
                                "raw_text": "Grapeseed oil or nonstick cooking spray, for greasing",
                                "extra_comment": "for greasing",
                                "position": 4,
                                "measurements": [
                                    {
                                        "id": 566535,
                                        "quantity": "0",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ]
                            },
                            {
                                "raw_text": "4 ears of sweet corn, husks removed",
                                "extra_comment": "husks removed",
                                "position": 5,
                                "measurements": [
                                    {
                                        "unit": {
                                            "abbreviation": "ear",
                                            "display_singular": "ear",
                                            "display_plural": "ears",
                                            "system": "none",
                                            "name": "ear"
                                        },
                                        "id": 566538,
                                        "quantity": "4"
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "sweet corn",
                                    "display_plural": "sweet corns",
                                    "created_at": 1574825126,
                                    "updated_at": 1574825126,
                                    "id": 5996,
                                    "name": "sweet corn"
                                },
                                "id": 68046
                            },
                            {
                                "raw_text": "1 tablespoon minced fresh flat-leaf parsley, for garnish",
                                "extra_comment": "for garnish",
                                "position": 6,
                                "measurements": [
                                    {
                                        "id": 566539,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 2290,
                                    "name": "fresh flat-leaf parsley",
                                    "display_singular": "fresh flat-leaf parsley",
                                    "display_plural": "fresh flat-leaf parsleys",
                                    "created_at": 1500483640,
                                    "updated_at": 1509035132
                                },
                                "id": 68047
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 566540,
                                        "quantity": "0",
                                        "unit": {
                                            "display_plural": "",
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 4072,
                                    "name": "lemon wedges",
                                    "display_singular": "lemon wedge",
                                    "display_plural": "lemon wedges",
                                    "created_at": 1526070227,
                                    "updated_at": 1526070227
                                },
                                "id": 68048,
                                "raw_text": "Lemon wedges, for serving",
                                "extra_comment": "for serving",
                                "position": 7
                            }
                        ],
                        "name": null,
                        "position": 1
                    }
                ],
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/d9f405db74274f659c281a99d7d4fb07/BFV67133_Gadgetreviewstovetopgrill_FB_Final.mp4",
                "nutrition": {
                    "updated_at": "2020-05-23T08:06:27+02:00",
                    "calories": 337,
                    "carbohydrates": 60,
                    "fat": 10,
                    "protein": 9,
                    "sugar": 18,
                    "fiber": 8
                },
                "draft_status": "published",
                "prep_time_minutes": 5,
                "show_id": 17,
                "total_time_tier": {
                    "tier": "under_15_minutes",
                    "display_tier": "Under 15 minutes"
                },
                "canonical_id": "recipe:6193",
                "id": 6193,
                "inspired_by_url": null,
                "seo_title": "Grilled Sweet Corn with Lemon Pepper Butter",
                "user_ratings": {
                    "count_negative": 0,
                    "score": null,
                    "count_positive": 0
                },
                "show": {
                    "name": "Tasty",
                    "id": 17
                },
                "facebook_posts": [],
                "brand_id": null,
                "buzz_id": null,
                "updated_at": 1590499966,
                "slug": "grilled-sweet-corn-with-lemon-pepper-butter",
                "brand": null,
                "instructions": [
                    {
                        "id": 55488,
                        "display_text": "In a small bowl, stir together the butter, lemon zest, and lemon pepper until well combined.",
                        "position": 1,
                        "start_time": 358500,
                        "end_time": 366666,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "id": 55489,
                        "display_text": "Heat a grill pan over medium-high heat. Grease with grapeseed oil. Brush one side of each ear of corn with the butter mixture, then add to the grill, buttered-side down. Cook until char marks appear, about 5 minutes per side.",
                        "position": 2,
                        "start_time": 370000,
                        "end_time": 377500,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "position": 3,
                        "start_time": 379000,
                        "end_time": 386000,
                        "temperature": null,
                        "appliance": null,
                        "id": 55490,
                        "display_text": "Transfer the corn to a serving platter and brush with more butter. Garnish with the parsley and serve with lemon wedges to squeeze over the corn."
                    },
                    {
                        "start_time": 0,
                        "end_time": 0,
                        "temperature": null,
                        "appliance": null,
                        "id": 55491,
                        "display_text": "Serve immediately or transfer to a 175°F (80°C) to keep warm until ready to serve.",
                        "position": 4
                    },
                    {
                        "start_time": 484000,
                        "end_time": 492666,
                        "temperature": null,
                        "appliance": null,
                        "id": 55492,
                        "display_text": "Enjoy!",
                        "position": 5
                    }
                ],
                "cook_time_minutes": 10,
                "created_at": 1590080706,
                "language": "eng",
                "servings_noun_singular": "serving",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/831cf26059bb41348dcf37929c0e0d30/BFV67133_Gadgetreviewstovetopgrill_FB_Final.jpg",
                "total_time_minutes": 15,
                "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "country": "US",
                "keywords": ", amazon review, bbq, charred, chef out of water, chicken, cook out, gadget review, grill, how to, lodge stove top grill, make it fancy, stove top, trendy vs traditional, worth it",
                "num_servings": 4,
                "renditions": [
                    {
                        "height": 404,
                        "width": 720,
                        "file_size": 77067592,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "aspect": "landscape",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_720/1590081522_00001.png",
                        "duration": 532248,
                        "bit_rate": 1159,
                        "content_type": "video/mp4",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/167982/square_720/1590081522",
                        "name": "mp4_720x404"
                    },
                    {
                        "height": 180,
                        "bit_rate": 456,
                        "maximum_bit_rate": null,
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/167982/square_320/1590081522",
                        "width": 320,
                        "duration": 532248,
                        "file_size": 30288430,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "landscape",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_320/1590081522_00001.png",
                        "name": "mp4_320x180"
                    },
                    {
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_720/1590081522_00001.png",
                        "width": 1280,
                        "duration": 532248,
                        "file_size": 169396597,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/167982/landscape_720/1590081522",
                        "name": "mp4_1280x720",
                        "height": 720,
                        "bit_rate": 2547,
                        "maximum_bit_rate": null,
                        "aspect": "landscape"
                    },
                    {
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "url": "https://vid.tasty.co/output/167982/landscape_480/1590081522",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_480/1590081522_00001.png",
                        "name": "mp4_640x360",
                        "width": 640,
                        "file_size": 66217816,
                        "maximum_bit_rate": null,
                        "aspect": "landscape",
                        "container": "mp4",
                        "height": 360,
                        "duration": 532248,
                        "bit_rate": 996
                    },
                    {
                        "duration": 532240,
                        "container": "ts",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/1445289064805-h2exzu/1590081522_00001.png",
                        "minimum_bit_rate": 272,
                        "content_type": "application/vnd.apple.mpegurl",
                        "aspect": "landscape",
                        "height": 1080,
                        "width": 1920,
                        "file_size": null,
                        "bit_rate": null,
                        "maximum_bit_rate": 4902,
                        "url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                        "name": "low"
                    }
                ],
                "aspect_ratio": "16:9",
                "servings_noun_plural": "servings",
                "tips_and_ratings_enabled": true,
                "is_shoppable": true,
                "promotion": "full",
                "yields": "Servings: 4",
                "compilations": [
                    {
                        "keywords": null,
                        "name": "How To BBQ on a Stove Top Grill",
                        "draft_status": "published",
                        "beauty_url": null,
                        "country": "US",
                        "description": null,
                        "promotion": "full",
                        "aspect_ratio": "16:9",
                        "buzz_id": null,
                        "created_at": 1590023618,
                        "id": 1486,
                        "slug": "how-to-bbq-on-a-stove-top-grill",
                        "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                        "canonical_id": "compilation:1486",
                        "show": [
                            {
                                "id": 17,
                                "name": "Tasty"
                            }
                        ],
                        "approved_at": 1590499995,
                        "language": "eng",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266749.jpg",
                        "video_id": 104514,
                        "facebook_posts": [],
                        "is_shoppable": false
                    }
                ],
                "beauty_url": null,
                "description": "Grilled corn is a mandatory item when we talk about a summer BBQ, but the addition of lemon pepper butter to this classic dish is guaranteed to wow your guests.",
                "is_one_top": false,
                "tags": [
                    {
                        "name": "sides",
                        "display_name": "Sides",
                        "type": "meal",
                        "id": 64490
                    },
                    {
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "occasion",
                        "id": 64504
                    },
                    {
                        "id": 64501,
                        "name": "game_day",
                        "display_name": "Game Day",
                        "type": "occasion"
                    },
                    {
                        "id": 64494,
                        "name": "grill",
                        "display_name": "Grill",
                        "type": "method"
                    },
                    {
                        "type": "seasonal",
                        "id": 64509,
                        "name": "spring",
                        "display_name": "Spring"
                    },
                    {
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty"
                    },
                    {
                        "display_name": "Easy",
                        "type": "difficulty",
                        "id": 64471,
                        "name": "easy"
                    },
                    {
                        "id": 64475,
                        "name": "fourth_of_july",
                        "display_name": "Fourth of July",
                        "type": "holiday"
                    },
                    {
                        "id": 64444,
                        "name": "american",
                        "display_name": "American",
                        "type": "cuisine"
                    },
                    {
                        "display_name": "Summer",
                        "type": "seasonal",
                        "id": 64510,
                        "name": "summer"
                    },
                    {
                        "id": 64445,
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "cuisine"
                    },
                    {
                        "id": 64467,
                        "name": "low_carb",
                        "display_name": "Low-Carb",
                        "type": "dietary"
                    },
                    {
                        "id": 64469,
                        "name": "vegetarian",
                        "display_name": "Vegetarian",
                        "type": "dietary"
                    },
                    {
                        "display_name": "Vegan",
                        "type": "dietary",
                        "id": 64468,
                        "name": "vegan"
                    },
                    {
                        "name": "healthy",
                        "display_name": "Healthy",
                        "type": "dietary",
                        "id": 64466
                    }
                ],
                "credits": [
                    {
                        "name": "Nichi Hoskins",
                        "type": "internal"
                    }
                ],
                "video_ad_content": "none",
                "approved_at": 1590499966,
                "name": "Grilled Sweet Corn With Lemon Pepper Butter",
                "video_id": 104514
            },
            {
                "country": "US",
                "draft_status": "published",
                "slug": "charred-cumin-broccolini",
                "total_time_minutes": 16,
                "brand": null,
                "tags": [
                    {
                        "id": 64509,
                        "name": "spring",
                        "display_name": "Spring",
                        "type": "seasonal"
                    },
                    {
                        "type": "occasion",
                        "id": 64501,
                        "name": "game_day",
                        "display_name": "Game Day"
                    },
                    {
                        "id": 64494,
                        "name": "grill",
                        "display_name": "Grill",
                        "type": "method"
                    },
                    {
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty"
                    },
                    {
                        "display_name": "Vegetarian",
                        "type": "dietary",
                        "id": 64469,
                        "name": "vegetarian"
                    },
                    {
                        "id": 64504,
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "occasion"
                    },
                    {
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "cuisine",
                        "id": 64445
                    },
                    {
                        "id": 64468,
                        "name": "vegan",
                        "display_name": "Vegan",
                        "type": "dietary"
                    },
                    {
                        "id": 64466,
                        "name": "healthy",
                        "display_name": "Healthy",
                        "type": "dietary"
                    },
                    {
                        "id": 64471,
                        "name": "easy",
                        "display_name": "Easy",
                        "type": "difficulty"
                    },
                    {
                        "id": 64490,
                        "name": "sides",
                        "display_name": "Sides",
                        "type": "meal"
                    },
                    {
                        "id": 64444,
                        "name": "american",
                        "display_name": "American",
                        "type": "cuisine"
                    },
                    {
                        "display_name": "Summer",
                        "type": "seasonal",
                        "id": 64510,
                        "name": "summer"
                    }
                ],
                "video_ad_content": "none",
                "created_at": 1590080557,
                "prep_time_minutes": 10,
                "seo_title": "Charred Cumin Broccolini",
                "servings_noun_plural": "servings",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/18262488970e4136850556de72bd68de/BFV67133_Gadgetreviewstovetopgrill_FB_Final.jpg",
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/d9f405db74274f659c281a99d7d4fb07/BFV67133_Gadgetreviewstovetopgrill_FB_Final.mp4",
                "cook_time_minutes": 6,
                "updated_at": 1590499970,
                "video_id": 104514,
                "total_time_tier": {
                    "tier": "under_30_minutes",
                    "display_tier": "Under 30 minutes"
                },
                "canonical_id": "recipe:6192",
                "yields": "Servings: 4",
                "sections": [
                    {
                        "name": null,
                        "position": 1,
                        "components": [
                            {
                                "id": 68033,
                                "raw_text": "1 tablespoon kosher salt, plus more for boiling",
                                "extra_comment": "plus more for boiling",
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 566541,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "kosher salts",
                                    "created_at": 1493307153,
                                    "updated_at": 1509035289,
                                    "id": 11,
                                    "name": "kosher salt",
                                    "display_singular": "kosher salt"
                                }
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 566545,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "pound",
                                            "abbreviation": "lb",
                                            "display_singular": "lb",
                                            "display_plural": "lb",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "id": 566542,
                                        "quantity": "850",
                                        "unit": {
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "broccolini",
                                    "display_singular": "broccolini",
                                    "display_plural": "broccolinis",
                                    "created_at": 1542667786,
                                    "updated_at": 1542667786,
                                    "id": 4926
                                },
                                "id": 68034,
                                "raw_text": "2 pounds broccolini (about 3 bundles )",
                                "extra_comment": "",
                                "position": 2
                            },
                            {
                                "id": 68035,
                                "raw_text": "2 tablespoons olive oil",
                                "extra_comment": "",
                                "position": 3,
                                "measurements": [
                                    {
                                        "id": 566544,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 4,
                                    "name": "olive oil",
                                    "display_singular": "olive oil",
                                    "display_plural": "olive oils",
                                    "created_at": 1493306183,
                                    "updated_at": 1509035290
                                }
                            },
                            {
                                "raw_text": "2 tablespoons cumin",
                                "extra_comment": "",
                                "position": 4,
                                "measurements": [
                                    {
                                        "id": 566543,
                                        "quantity": "2",
                                        "unit": {
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 151,
                                    "name": "cumin",
                                    "display_singular": "cumin",
                                    "display_plural": "cumins",
                                    "created_at": 1493906367,
                                    "updated_at": 1509035283
                                },
                                "id": 68036
                            },
                            {
                                "id": 68037,
                                "raw_text": "1 tablespoon kosher salt",
                                "extra_comment": "",
                                "position": 5,
                                "measurements": [
                                    {
                                        "id": 566546,
                                        "quantity": "1",
                                        "unit": {
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "kosher salts",
                                    "created_at": 1493307153,
                                    "updated_at": 1509035289,
                                    "id": 11,
                                    "name": "kosher salt",
                                    "display_singular": "kosher salt"
                                }
                            },
                            {
                                "id": 68038,
                                "raw_text": "1 tablespoon  ground black pepper",
                                "extra_comment": "",
                                "position": 6,
                                "measurements": [
                                    {
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        },
                                        "id": 566547,
                                        "quantity": "1"
                                    }
                                ],
                                "ingredient": {
                                    "id": 232,
                                    "name": "ground black pepper",
                                    "display_singular": "ground black pepper",
                                    "display_plural": "ground black peppers",
                                    "created_at": 1494292509,
                                    "updated_at": 1509035277
                                }
                            },
                            {
                                "extra_comment": "cut into wedges",
                                "position": 7,
                                "measurements": [
                                    {
                                        "id": 566550,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 155,
                                    "name": "lemon",
                                    "display_singular": "lemon",
                                    "display_plural": "lemons",
                                    "created_at": 1493906426,
                                    "updated_at": 1509035282
                                },
                                "id": 68039,
                                "raw_text": "1 lemon, cut into wedges"
                            },
                            {
                                "measurements": [
                                    {
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        },
                                        "id": 566548,
                                        "quantity": "1"
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035288,
                                    "id": 24,
                                    "name": "sugar",
                                    "display_singular": "sugar",
                                    "display_plural": "sugars",
                                    "created_at": 1493314650
                                },
                                "id": 68040,
                                "raw_text": "1 tablespoon sugar",
                                "extra_comment": "",
                                "position": 8
                            },
                            {
                                "id": 68041,
                                "raw_text": "Grapeseed oil or nonstick cooking spray, for greasing",
                                "extra_comment": "or nonstick cooking spray, for greasing",
                                "position": 9,
                                "measurements": [
                                    {
                                        "id": 566549,
                                        "quantity": "0",
                                        "unit": {
                                            "display_plural": "",
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035139,
                                    "id": 2166,
                                    "name": "grapeseed oil",
                                    "display_singular": "grapeseed oil",
                                    "display_plural": "grapeseed oils",
                                    "created_at": 1500176867
                                }
                            }
                        ]
                    }
                ],
                "id": 6192,
                "is_shoppable": true,
                "keywords": ", amazon review, bbq, charred, chef out of water, chicken, cook out, gadget review, grill, how to, lodge stove top grill, make it fancy, stove top, trendy vs traditional, worth it",
                "language": "eng",
                "tips_and_ratings_enabled": true,
                "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "show": {
                    "id": 17,
                    "name": "Tasty"
                },
                "renditions": [
                    {
                        "container": "mp4",
                        "name": "mp4_720x404",
                        "height": 404,
                        "width": 720,
                        "duration": 532248,
                        "maximum_bit_rate": null,
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/167982/square_720/1590081522",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_720/1590081522_00001.png",
                        "file_size": 77067592,
                        "bit_rate": 1159,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4"
                    },
                    {
                        "width": 320,
                        "duration": 532248,
                        "bit_rate": 456,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "aspect": "landscape",
                        "container": "mp4",
                        "height": 180,
                        "name": "mp4_320x180",
                        "url": "https://vid.tasty.co/output/167982/square_320/1590081522",
                        "content_type": "video/mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_320/1590081522_00001.png",
                        "file_size": 30288430
                    },
                    {
                        "aspect": "landscape",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_720/1590081522_00001.png",
                        "name": "mp4_1280x720",
                        "width": 1280,
                        "duration": 532248,
                        "file_size": 169396597,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "height": 720,
                        "bit_rate": 2547,
                        "maximum_bit_rate": null,
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/167982/landscape_720/1590081522"
                    },
                    {
                        "content_type": "video/mp4",
                        "aspect": "landscape",
                        "container": "mp4",
                        "height": 360,
                        "width": 640,
                        "duration": 532248,
                        "bit_rate": 996,
                        "maximum_bit_rate": null,
                        "url": "https://vid.tasty.co/output/167982/landscape_480/1590081522",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_480/1590081522_00001.png",
                        "name": "mp4_640x360",
                        "file_size": 66217816,
                        "minimum_bit_rate": null
                    },
                    {
                        "url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                        "name": "low",
                        "width": 1920,
                        "duration": 532240,
                        "file_size": null,
                        "container": "ts",
                        "content_type": "application/vnd.apple.mpegurl",
                        "aspect": "landscape",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/1445289064805-h2exzu/1590081522_00001.png",
                        "height": 1080,
                        "bit_rate": null,
                        "maximum_bit_rate": 4902,
                        "minimum_bit_rate": 272
                    }
                ],
                "beauty_url": null,
                "brand_id": null,
                "description": "Broccolini is a fun vegetable that is perfect for the grill! While usually roasted in the oven, throwing broccolini on the grill adds a great char and another layer of flavor.",
                "promotion": "full",
                "show_id": 17,
                "user_ratings": {
                    "count_positive": 0,
                    "count_negative": 0,
                    "score": null
                },
                "compilations": [
                    {
                        "beauty_url": null,
                        "created_at": 1590023618,
                        "description": null,
                        "language": "eng",
                        "approved_at": 1590499995,
                        "aspect_ratio": "16:9",
                        "is_shoppable": false,
                        "keywords": null,
                        "name": "How To BBQ on a Stove Top Grill",
                        "promotion": "full",
                        "slug": "how-to-bbq-on-a-stove-top-grill",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266749.jpg",
                        "country": "US",
                        "draft_status": "published",
                        "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                        "show": [
                            {
                                "id": 17,
                                "name": "Tasty"
                            }
                        ],
                        "buzz_id": null,
                        "facebook_posts": [],
                        "canonical_id": "compilation:1486",
                        "id": 1486,
                        "video_id": 104514
                    }
                ],
                "approved_at": 1590499970,
                "inspired_by_url": null,
                "is_one_top": false,
                "num_servings": 4,
                "servings_noun_singular": "serving",
                "nutrition_visibility": "auto",
                "aspect_ratio": "16:9",
                "name": "Charred Cumin Broccolini",
                "instructions": [
                    {
                        "end_time": 0,
                        "temperature": null,
                        "appliance": null,
                        "id": 55479,
                        "display_text": "Bring a large pot of salted water to a boil. Fill a large bowl with cold water and set nearby.",
                        "position": 1,
                        "start_time": 0
                    },
                    {
                        "id": 55480,
                        "display_text": "Blanch the broccolini in the boiling water for about 3 minutes, then transfer to the bowl of cold water to stop the cooking process.",
                        "position": 2,
                        "start_time": 0,
                        "end_time": 0,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "id": 55481,
                        "display_text": "Remove broccolini from the water, shaking off any excess, and spread on a baking sheet.",
                        "position": 3,
                        "start_time": 0,
                        "end_time": 0,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "id": 55482,
                        "display_text": "Drizzle the broccolini with the olive oil and season with the cumin, salt, and pepper. Toss to coat.",
                        "position": 4,
                        "start_time": 297000,
                        "end_time": 303500,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "display_text": "Heat a grill pan over medium-high heat. Grease with grapeseed oil. Add the broccolini to the hot grill and cook until char marks appear, about 3 minutes per side. Remove the broccolini from the grill.",
                        "position": 5,
                        "start_time": 303000,
                        "end_time": 324666,
                        "temperature": null,
                        "appliance": null,
                        "id": 55483
                    },
                    {
                        "display_text": "Rub the cut sides of the lemon wedges with the sugar to lightly coat, then grill until caramelized, 1 minute on each side.",
                        "position": 6,
                        "start_time": 327000,
                        "end_time": 335666,
                        "temperature": null,
                        "appliance": null,
                        "id": 55484
                    },
                    {
                        "id": 55485,
                        "display_text": "Place the caramelized lemon wedges on top of the broccolini, then squeeze over the broccolini before serving",
                        "position": 7,
                        "start_time": 0,
                        "end_time": 0,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "appliance": null,
                        "id": 55486,
                        "display_text": "Serve immediately or transfer to a 175°F (80°C) to keep warm until ready to serve.",
                        "position": 8,
                        "start_time": 0,
                        "end_time": 0,
                        "temperature": null
                    },
                    {
                        "display_text": "Enjoy!",
                        "position": 9,
                        "start_time": 481000,
                        "end_time": 489500,
                        "temperature": null,
                        "appliance": null,
                        "id": 55487
                    }
                ],
                "credits": [
                    {
                        "name": "Nichi Hoskins",
                        "type": "internal"
                    }
                ],
                "buzz_id": null,
                "facebook_posts": [],
                "nutrition": {
                    "fat": 15,
                    "protein": 6,
                    "sugar": 6,
                    "fiber": 8,
                    "updated_at": "2020-05-23T08:06:26+02:00",
                    "calories": 232,
                    "carbohydrates": 23
                }
            },
            {
                "keywords": ", amazon review, bbq, charred, chef out of water, chicken, cook out, gadget review, grill, how to, lodge stove top grill, make it fancy, stove top, trendy vs traditional, worth it",
                "servings_noun_singular": "serving",
                "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "nutrition_visibility": "auto",
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/d9f405db74274f659c281a99d7d4fb07/BFV67133_Gadgetreviewstovetopgrill_FB_Final.mp4",
                "created_at": 1590023618,
                "is_shoppable": true,
                "inspired_by_url": null,
                "prep_time_minutes": 5,
                "slug": "smoked-paprika-potatoes",
                "renditions": [
                    {
                        "width": 720,
                        "duration": 532248,
                        "file_size": 77067592,
                        "bit_rate": 1159,
                        "minimum_bit_rate": null,
                        "url": "https://vid.tasty.co/output/167982/square_720/1590081522",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_720/1590081522_00001.png",
                        "height": 404,
                        "content_type": "video/mp4",
                        "aspect": "landscape",
                        "container": "mp4",
                        "name": "mp4_720x404",
                        "maximum_bit_rate": null
                    },
                    {
                        "file_size": 30288430,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "aspect": "landscape",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_320/1590081522_00001.png",
                        "name": "mp4_320x180",
                        "height": 180,
                        "duration": 532248,
                        "content_type": "video/mp4",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/167982/square_320/1590081522",
                        "width": 320,
                        "bit_rate": 456
                    },
                    {
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_720/1590081522_00001.png",
                        "name": "mp4_1280x720",
                        "height": 720,
                        "file_size": 169396597,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/167982/landscape_720/1590081522",
                        "width": 1280,
                        "duration": 532248,
                        "bit_rate": 2547,
                        "minimum_bit_rate": null,
                        "container": "mp4"
                    },
                    {
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_480/1590081522_00001.png",
                        "name": "mp4_640x360",
                        "width": 640,
                        "duration": 532248,
                        "file_size": 66217816,
                        "container": "mp4",
                        "content_type": "video/mp4",
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/167982/landscape_480/1590081522",
                        "height": 360,
                        "bit_rate": 996,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null
                    },
                    {
                        "url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                        "width": 1920,
                        "file_size": null,
                        "bit_rate": null,
                        "maximum_bit_rate": 4902,
                        "minimum_bit_rate": 272,
                        "content_type": "application/vnd.apple.mpegurl",
                        "container": "ts",
                        "name": "low",
                        "height": 1080,
                        "duration": 532240,
                        "aspect": "landscape",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/1445289064805-h2exzu/1590081522_00001.png"
                    }
                ],
                "description": "These potatoes make the perfect side dish to accompany what you’re throwing on the grill this summer. They’re the same potato wedges you ate as a child, but grilled and seasoned to perfection!",
                "id": 6191,
                "instructions": [
                    {
                        "id": 55472,
                        "display_text": "Cut the potatoes into wedges. Add the wedges to a pot of cold salted water, then bring to a boil. Cook until the potatoes are almost tender, about 10 minutes, then drain. Spread the potatoes on a baking sheet.",
                        "position": 1,
                        "start_time": 0,
                        "end_time": 0,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "appliance": null,
                        "id": 55473,
                        "display_text": "In a small bowl, mix together the paprika, salt, and pepper.",
                        "position": 2,
                        "start_time": 216000,
                        "end_time": 221666,
                        "temperature": null
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 55474,
                        "display_text": "Drizzle the potatoes with 1 tablespoon of olive oil and season with the spice mixture.",
                        "position": 3,
                        "start_time": 223000,
                        "end_time": 233000
                    },
                    {
                        "start_time": 236000,
                        "end_time": 264500,
                        "temperature": null,
                        "appliance": null,
                        "id": 55475,
                        "display_text": "Heat a grill pan over medium-high heat. Grease the grill with grapeseed oil. Add the potatoes to the hot grill and cook until grill marks form on both sides, about 2 minutes per side.",
                        "position": 4
                    },
                    {
                        "position": 5,
                        "start_time": 280000,
                        "end_time": 291166,
                        "temperature": null,
                        "appliance": null,
                        "id": 55476,
                        "display_text": "Remove the potatoes from the grill, drizzle with the remaining tablespoon of olive oil, and garnish with more paprika and the parsley."
                    },
                    {
                        "id": 55477,
                        "display_text": "Serve immediately or transfer to a 175°F (80°C) to keep warm until ready to serve.",
                        "position": 6,
                        "start_time": 0,
                        "end_time": 0,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 55478,
                        "display_text": "Enjoy!",
                        "position": 7,
                        "start_time": 488000,
                        "end_time": 490833
                    }
                ],
                "canonical_id": "recipe:6191",
                "show": {
                    "id": 17,
                    "name": "Tasty"
                },
                "facebook_posts": [],
                "servings_noun_plural": "servings",
                "video_id": 104514,
                "show_id": 17,
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/8ce2bbb006b54a008ceb06a11f84ea85/BFV67133_Gadgetreviewstovetopgrill_FB_Final.jpg",
                "sections": [
                    {
                        "components": [
                            {
                                "raw_text": "5 large gold potatoes",
                                "extra_comment": "",
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 566551,
                                        "quantity": "5",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 6439,
                                    "name": "large gold potatoes",
                                    "display_singular": "large gold potato",
                                    "display_plural": "large gold potatoes",
                                    "created_at": 1590075774,
                                    "updated_at": 1590075774
                                },
                                "id": 68024
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 566553,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1495929686,
                                    "updated_at": 1509035226,
                                    "id": 878,
                                    "name": "smoked paprika",
                                    "display_singular": "smoked paprika",
                                    "display_plural": "smoked paprikas"
                                },
                                "id": 68025,
                                "raw_text": "2 tablespoons smoked paprika, plus more for garnish",
                                "extra_comment": "plus more for garnish",
                                "position": 2
                            },
                            {
                                "id": 68026,
                                "raw_text": "1 tablespoon kosher salt",
                                "extra_comment": "",
                                "position": 3,
                                "measurements": [
                                    {
                                        "id": 566552,
                                        "quantity": "1",
                                        "unit": {
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "kosher salt",
                                    "display_singular": "kosher salt",
                                    "display_plural": "kosher salts",
                                    "created_at": 1493307153,
                                    "updated_at": 1509035289,
                                    "id": 11
                                }
                            },
                            {
                                "id": 68027,
                                "raw_text": "1 tablespoon ground black pepper",
                                "extra_comment": "",
                                "position": 4,
                                "measurements": [
                                    {
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        },
                                        "id": 566556,
                                        "quantity": "1"
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1494292509,
                                    "updated_at": 1509035277,
                                    "id": 232,
                                    "name": "ground black pepper",
                                    "display_singular": "ground black pepper",
                                    "display_plural": "ground black peppers"
                                }
                            },
                            {
                                "raw_text": "1 tablespoon granulated garlic",
                                "extra_comment": "",
                                "position": 5,
                                "measurements": [
                                    {
                                        "id": 566558,
                                        "quantity": "1",
                                        "unit": {
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 1446,
                                    "name": "granulated garlic",
                                    "display_singular": "granulated garlic",
                                    "display_plural": "granulated garlics",
                                    "created_at": 1496755079,
                                    "updated_at": 1509035188
                                },
                                "id": 68028
                            },
                            {
                                "raw_text": "1 tablespoon granulated onion",
                                "extra_comment": "",
                                "position": 6,
                                "measurements": [
                                    {
                                        "id": 566554,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "granulated onions",
                                    "created_at": 1530334356,
                                    "updated_at": 1530334356,
                                    "id": 4416,
                                    "name": "granulated onion",
                                    "display_singular": "granulated onion"
                                },
                                "id": 68029
                            },
                            {
                                "id": 68030,
                                "raw_text": "2 tablespoons olive oil, divided",
                                "extra_comment": "divided",
                                "position": 7,
                                "measurements": [
                                    {
                                        "id": 566555,
                                        "quantity": "2",
                                        "unit": {
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "olive oils",
                                    "created_at": 1493306183,
                                    "updated_at": 1509035290,
                                    "id": 4,
                                    "name": "olive oil",
                                    "display_singular": "olive oil"
                                }
                            },
                            {
                                "ingredient": {
                                    "name": "nonstick cooking spray",
                                    "display_singular": "nonstick cooking spray",
                                    "display_plural": "nonstick cooking sprays",
                                    "created_at": 1520176895,
                                    "updated_at": 1520176895,
                                    "id": 3826
                                },
                                "id": 68031,
                                "raw_text": "Grapeseed oil or nonstick cooking spray, for greasing",
                                "extra_comment": "for greasing",
                                "position": 8,
                                "measurements": [
                                    {
                                        "unit": {
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none",
                                            "name": ""
                                        },
                                        "id": 566559,
                                        "quantity": "0"
                                    }
                                ]
                            },
                            {
                                "extra_comment": "minced",
                                "position": 9,
                                "measurements": [
                                    {
                                        "id": 566557,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 2290,
                                    "name": "fresh flat-leaf parsley",
                                    "display_singular": "fresh flat-leaf parsley",
                                    "display_plural": "fresh flat-leaf parsleys",
                                    "created_at": 1500483640,
                                    "updated_at": 1509035132
                                },
                                "id": 68032,
                                "raw_text": "2 tablespoon minced fresh flat-leaf parsley"
                            }
                        ],
                        "name": null,
                        "position": 1
                    }
                ],
                "name": "Smoked Paprika Potatoes",
                "promotion": "full",
                "cook_time_minutes": 15,
                "country": "US",
                "is_one_top": false,
                "tips_and_ratings_enabled": true,
                "compilations": [
                    {
                        "promotion": "full",
                        "slug": "how-to-bbq-on-a-stove-top-grill",
                        "created_at": 1590023618,
                        "description": null,
                        "is_shoppable": false,
                        "keywords": null,
                        "language": "eng",
                        "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                        "show": [
                            {
                                "id": 17,
                                "name": "Tasty"
                            }
                        ],
                        "buzz_id": null,
                        "country": "US",
                        "name": "How To BBQ on a Stove Top Grill",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266749.jpg",
                        "video_id": 104514,
                        "facebook_posts": [],
                        "aspect_ratio": "16:9",
                        "beauty_url": null,
                        "draft_status": "published",
                        "id": 1486,
                        "canonical_id": "compilation:1486",
                        "approved_at": 1590499995
                    }
                ],
                "tags": [
                    {
                        "id": 64501,
                        "name": "game_day",
                        "display_name": "Game Day",
                        "type": "occasion"
                    },
                    {
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty"
                    },
                    {
                        "type": "cuisine",
                        "id": 64444,
                        "name": "american",
                        "display_name": "American"
                    },
                    {
                        "display_name": "Summer",
                        "type": "seasonal",
                        "id": 64510,
                        "name": "summer"
                    },
                    {
                        "id": 64504,
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "occasion"
                    },
                    {
                        "id": 64471,
                        "name": "easy",
                        "display_name": "Easy",
                        "type": "difficulty"
                    },
                    {
                        "id": 64445,
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "cuisine"
                    },
                    {
                        "id": 64509,
                        "name": "spring",
                        "display_name": "Spring",
                        "type": "seasonal"
                    },
                    {
                        "name": "sides",
                        "display_name": "Sides",
                        "type": "meal",
                        "id": 64490
                    },
                    {
                        "id": 64475,
                        "name": "fourth_of_july",
                        "display_name": "Fourth of July",
                        "type": "holiday"
                    },
                    {
                        "name": "baking_pan",
                        "display_name": "Baking Pan",
                        "type": "equipment",
                        "id": 1280500
                    },
                    {
                        "id": 1247790,
                        "name": "tongs",
                        "display_name": "Tongs",
                        "type": "equipment"
                    }
                ],
                "approved_at": 1590499976,
                "brand_id": null,
                "video_ad_content": "none",
                "credits": [
                    {
                        "name": "Nichi Hoskins",
                        "type": "internal"
                    }
                ],
                "language": "eng",
                "total_time_tier": {
                    "tier": "under_30_minutes",
                    "display_tier": "Under 30 minutes"
                },
                "num_servings": 4,
                "seo_title": "",
                "updated_at": 1590499976,
                "nutrition": {
                    "carbohydrates": 3,
                    "fat": 7,
                    "protein": 0,
                    "sugar": 0,
                    "fiber": 1,
                    "updated_at": "2020-05-23T08:06:26+02:00",
                    "calories": 75
                },
                "aspect_ratio": "16:9",
                "beauty_url": null,
                "total_time_minutes": 20,
                "yields": "Servings: 4",
                "brand": null,
                "user_ratings": {
                    "count_negative": 0,
                    "score": null,
                    "count_positive": 0
                },
                "buzz_id": null,
                "draft_status": "published"
            },
            {
                "created_at": 1590023456,
                "draft_status": "published",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/8bbcb9fd9a094521aab2a02f11396ab7/BFV67133_Gadgetreviewstovetopgrill_FB_Final.jpg",
                "compilations": [
                    {
                        "created_at": 1590023618,
                        "draft_status": "published",
                        "id": 1486,
                        "promotion": "full",
                        "approved_at": 1590499995,
                        "beauty_url": null,
                        "buzz_id": null,
                        "country": "US",
                        "slug": "how-to-bbq-on-a-stove-top-grill",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266749.jpg",
                        "show": [
                            {
                                "id": 17,
                                "name": "Tasty"
                            }
                        ],
                        "description": null,
                        "is_shoppable": false,
                        "video_id": 104514,
                        "canonical_id": "compilation:1486",
                        "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                        "facebook_posts": [],
                        "aspect_ratio": "16:9",
                        "keywords": null,
                        "language": "eng",
                        "name": "How To BBQ on a Stove Top Grill"
                    }
                ],
                "video_ad_content": "none",
                "brand_id": null,
                "is_shoppable": true,
                "num_servings": 4,
                "show_id": 17,
                "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "yields": "Servings: 4",
                "user_ratings": {
                    "count_positive": 0,
                    "count_negative": 0,
                    "score": null
                },
                "beauty_url": null,
                "inspired_by_url": null,
                "servings_noun_singular": "serving",
                "id": 6190,
                "name": "Raspberry Chipotle Chicken Thighs",
                "tips_and_ratings_enabled": true,
                "canonical_id": "recipe:6190",
                "aspect_ratio": "16:9",
                "keywords": ", amazon review, bbq, charred, chef out of water, chicken, cook out, gadget review, grill, how to, lodge stove top grill, make it fancy, stove top, trendy vs traditional, worth it",
                "brand": null,
                "sections": [
                    {
                        "components": [
                            {
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 566560,
                                        "quantity": "4",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "garlic powders",
                                    "created_at": 1493307128,
                                    "updated_at": 1509035289,
                                    "id": 9,
                                    "name": "garlic powder",
                                    "display_singular": "garlic powder"
                                },
                                "id": 68009,
                                "raw_text": "4 tablespoons garlic powder, divided",
                                "extra_comment": "divided"
                            },
                            {
                                "extra_comment": "divided",
                                "position": 2,
                                "measurements": [
                                    {
                                        "id": 566571,
                                        "quantity": "4",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 8,
                                    "name": "onion powder",
                                    "display_singular": "onion powder",
                                    "display_plural": "onion powders",
                                    "created_at": 1493307116,
                                    "updated_at": 1509035289
                                },
                                "id": 68010,
                                "raw_text": "4 tablespoons onion powder, divided"
                            },
                            {
                                "position": 3,
                                "measurements": [
                                    {
                                        "quantity": "2",
                                        "unit": {
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon"
                                        },
                                        "id": 566561
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "smoked paprika",
                                    "display_plural": "smoked paprikas",
                                    "created_at": 1495929686,
                                    "updated_at": 1509035226,
                                    "id": 878,
                                    "name": "smoked paprika"
                                },
                                "id": 68011,
                                "raw_text": "2 tablespoons smoked paprika",
                                "extra_comment": ""
                            },
                            {
                                "position": 4,
                                "measurements": [
                                    {
                                        "id": 566572,
                                        "quantity": "2",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1493307153,
                                    "updated_at": 1509035289,
                                    "id": 11,
                                    "name": "kosher salt",
                                    "display_singular": "kosher salt",
                                    "display_plural": "kosher salts"
                                },
                                "id": 68012,
                                "raw_text": "2 tablespoons kosher salt, plus more to taste",
                                "extra_comment": "plus more to taste"
                            },
                            {
                                "id": 68013,
                                "raw_text": "2 tablespoons freshly ground black pepper, plus more to taste",
                                "extra_comment": "plus more to taste",
                                "position": 5,
                                "measurements": [
                                    {
                                        "id": 566564,
                                        "quantity": "2",
                                        "unit": {
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "freshly ground black pepper",
                                    "display_plural": "freshly ground black peppers",
                                    "created_at": 1493925438,
                                    "updated_at": 1509035282,
                                    "id": 166,
                                    "name": "freshly ground black pepper"
                                }
                            },
                            {
                                "id": 68014,
                                "raw_text": "6-8 boneless, skinless chicken thighs",
                                "extra_comment": "",
                                "position": 6,
                                "measurements": [
                                    {
                                        "id": 566563,
                                        "quantity": "6",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1562791202,
                                    "updated_at": 1562791202,
                                    "id": 5576,
                                    "name": "boneless skinless chicken thighs",
                                    "display_singular": "boneless skinless chicken thigh",
                                    "display_plural": "boneless skinless chicken thighs"
                                }
                            },
                            {
                                "id": 68015,
                                "raw_text": "2 tablespoons olive oil",
                                "extra_comment": "",
                                "position": 7,
                                "measurements": [
                                    {
                                        "id": 566562,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 4,
                                    "name": "olive oil",
                                    "display_singular": "olive oil",
                                    "display_plural": "olive oils",
                                    "created_at": 1493306183,
                                    "updated_at": 1509035290
                                }
                            },
                            {
                                "ingredient": {
                                    "display_singular": "unsalted butter",
                                    "display_plural": "unsalted butters",
                                    "created_at": 1494806355,
                                    "updated_at": 1509035272,
                                    "id": 291,
                                    "name": "unsalted butter"
                                },
                                "id": 68016,
                                "raw_text": "1 tablespoon unsalted butter",
                                "extra_comment": "",
                                "position": 8,
                                "measurements": [
                                    {
                                        "id": 566565,
                                        "quantity": "1",
                                        "unit": {
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon"
                                        }
                                    }
                                ]
                            },
                            {
                                "raw_text": "6 ounces raspberries",
                                "extra_comment": "",
                                "position": 9,
                                "measurements": [
                                    {
                                        "id": 566576,
                                        "quantity": "170",
                                        "unit": {
                                            "display_plural": "g",
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g"
                                        }
                                    },
                                    {
                                        "quantity": "6",
                                        "unit": {
                                            "display_singular": "oz",
                                            "display_plural": "oz",
                                            "system": "imperial",
                                            "name": "ounce",
                                            "abbreviation": "oz"
                                        },
                                        "id": 566575
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "raspberry",
                                    "display_plural": "raspberries",
                                    "created_at": 1495065307,
                                    "updated_at": 1509035261,
                                    "id": 431,
                                    "name": "raspberry"
                                },
                                "id": 68017
                            },
                            {
                                "id": 68018,
                                "raw_text": "2 tablespoons brown sugar",
                                "extra_comment": "",
                                "position": 10,
                                "measurements": [
                                    {
                                        "id": 566570,
                                        "quantity": "2",
                                        "unit": {
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1493307081,
                                    "updated_at": 1509035289,
                                    "id": 6,
                                    "name": "brown sugar",
                                    "display_singular": "brown sugar",
                                    "display_plural": "brown sugars"
                                }
                            },
                            {
                                "id": 68019,
                                "raw_text": "1 tablespoon chipotle chile powder",
                                "extra_comment": "",
                                "position": 11,
                                "measurements": [
                                    {
                                        "id": 566574,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "chipotle powder",
                                    "display_singular": "chipotle powder",
                                    "display_plural": "chipotle powders",
                                    "created_at": 1499981021,
                                    "updated_at": 1509035145,
                                    "id": 2083
                                }
                            },
                            {
                                "ingredient": {
                                    "name": "ketchup",
                                    "display_singular": "ketchup",
                                    "display_plural": "ketchups",
                                    "created_at": 1493307243,
                                    "updated_at": 1509035289,
                                    "id": 14
                                },
                                "id": 68020,
                                "raw_text": "½ cup ketchup",
                                "extra_comment": "",
                                "position": 12,
                                "measurements": [
                                    {
                                        "id": 566567,
                                        "quantity": "120",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        }
                                    },
                                    {
                                        "id": 566566,
                                        "quantity": "½",
                                        "unit": {
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial",
                                            "name": "cup",
                                            "abbreviation": "c"
                                        }
                                    }
                                ]
                            },
                            {
                                "extra_comment": "",
                                "position": 13,
                                "measurements": [
                                    {
                                        "id": 566569,
                                        "quantity": "1",
                                        "unit": {
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 6441,
                                    "name": "mesquite liquid smoke",
                                    "display_singular": "mesquite liquid smoke",
                                    "display_plural": "mesquite liquid smokes",
                                    "created_at": 1590075979,
                                    "updated_at": 1590075979
                                },
                                "id": 68021,
                                "raw_text": "1 tablespoon mesquite liquid smoke"
                            },
                            {
                                "id": 68022,
                                "raw_text": "Grapeseed oil or nonstick cooking spray, for greasing",
                                "extra_comment": "for greasing",
                                "position": 14,
                                "measurements": [
                                    {
                                        "id": 566568,
                                        "quantity": "0",
                                        "unit": {
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1520176895,
                                    "id": 3826,
                                    "name": "nonstick cooking spray",
                                    "display_singular": "nonstick cooking spray",
                                    "display_plural": "nonstick cooking sprays",
                                    "created_at": 1520176895
                                }
                            },
                            {
                                "raw_text": "1 tablespoon minced fresh flat-leaf parsley, for garnish",
                                "extra_comment": "minced, for garnish",
                                "position": 15,
                                "measurements": [
                                    {
                                        "unit": {
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp"
                                        },
                                        "id": 566573,
                                        "quantity": "1"
                                    }
                                ],
                                "ingredient": {
                                    "id": 2290,
                                    "name": "fresh flat-leaf parsley",
                                    "display_singular": "fresh flat-leaf parsley",
                                    "display_plural": "fresh flat-leaf parsleys",
                                    "created_at": 1500483640,
                                    "updated_at": 1509035132
                                },
                                "id": 68023
                            }
                        ],
                        "name": null,
                        "position": 1
                    }
                ],
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/d9f405db74274f659c281a99d7d4fb07/BFV67133_Gadgetreviewstovetopgrill_FB_Final.mp4",
                "country": "US",
                "description": "Chicken thighs grilled with a sweet, smoky, and slightly spicy sauce are sure to kick things up a notch at your next indoor or outdoor barbecue.",
                "total_time_minutes": 30,
                "updated_at": 1590499982,
                "facebook_posts": [],
                "approved_at": 1590499981,
                "buzz_id": null,
                "cook_time_minutes": 20,
                "prep_time_minutes": 10,
                "seo_title": "Raspberry Chipotle Chicken Thighs",
                "slug": "raspberry-chipotle-chicken-thighs",
                "total_time_tier": {
                    "display_tier": "Under 30 minutes",
                    "tier": "under_30_minutes"
                },
                "tags": [
                    {
                        "id": 64444,
                        "name": "american",
                        "display_name": "American",
                        "type": "cuisine"
                    },
                    {
                        "id": 64509,
                        "name": "spring",
                        "display_name": "Spring",
                        "type": "seasonal"
                    },
                    {
                        "id": 64445,
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "cuisine"
                    },
                    {
                        "id": 64510,
                        "name": "summer",
                        "display_name": "Summer",
                        "type": "seasonal"
                    },
                    {
                        "type": "meal",
                        "id": 64486,
                        "name": "dinner",
                        "display_name": "Dinner"
                    },
                    {
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty",
                        "id": 64472
                    },
                    {
                        "id": 64504,
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "occasion"
                    },
                    {
                        "id": 64501,
                        "name": "game_day",
                        "display_name": "Game Day",
                        "type": "occasion"
                    },
                    {
                        "id": 1247790,
                        "name": "tongs",
                        "display_name": "Tongs",
                        "type": "equipment"
                    },
                    {
                        "id": 1280510,
                        "name": "mixing_bowl",
                        "display_name": "Mixing Bowl",
                        "type": "equipment"
                    }
                ],
                "credits": [
                    {
                        "name": "Nichi Hoskins",
                        "type": "internal"
                    }
                ],
                "show": {
                    "id": 17,
                    "name": "Tasty"
                },
                "renditions": [
                    {
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/167982/square_720/1590081522",
                        "width": 720,
                        "duration": 532248,
                        "file_size": 77067592,
                        "bit_rate": 1159,
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_720/1590081522_00001.png",
                        "height": 404,
                        "content_type": "video/mp4",
                        "aspect": "landscape",
                        "name": "mp4_720x404"
                    },
                    {
                        "name": "mp4_320x180",
                        "bit_rate": 456,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "aspect": "landscape",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_320/1590081522_00001.png",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/167982/square_320/1590081522",
                        "height": 180,
                        "width": 320,
                        "duration": 532248,
                        "file_size": 30288430,
                        "content_type": "video/mp4"
                    },
                    {
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_720/1590081522_00001.png",
                        "name": "mp4_1280x720",
                        "height": 720,
                        "bit_rate": 2547,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/167982/landscape_720/1590081522",
                        "width": 1280,
                        "duration": 532248,
                        "file_size": 169396597,
                        "content_type": "video/mp4",
                        "aspect": "landscape"
                    },
                    {
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_480/1590081522_00001.png",
                        "name": "mp4_640x360",
                        "width": 640,
                        "duration": 532248,
                        "bit_rate": 996,
                        "aspect": "landscape",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/167982/landscape_480/1590081522",
                        "height": 360,
                        "file_size": 66217816,
                        "content_type": "video/mp4"
                    },
                    {
                        "width": 1920,
                        "minimum_bit_rate": 272,
                        "content_type": "application/vnd.apple.mpegurl",
                        "container": "ts",
                        "height": 1080,
                        "file_size": null,
                        "bit_rate": null,
                        "maximum_bit_rate": 4902,
                        "aspect": "landscape",
                        "url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/1445289064805-h2exzu/1590081522_00001.png",
                        "name": "low",
                        "duration": 532240
                    }
                ],
                "is_one_top": false,
                "language": "eng",
                "promotion": "full",
                "servings_noun_plural": "servings",
                "video_id": 104514,
                "nutrition_visibility": "auto",
                "instructions": [
                    {
                        "display_text": "In a small bowl, mix together 2 tablespoons garlic powder, 2 tablespoons onion powder, the paprika, salt, and pepper.",
                        "position": 1,
                        "start_time": 67666,
                        "end_time": 77166,
                        "temperature": null,
                        "appliance": null,
                        "id": 55466
                    },
                    {
                        "id": 55467,
                        "display_text": "Drizzle the chicken thighs with the olive oil, then sprinkle with half of the spice mixture and rub to coat well. Sprinkle with the remaining spice mixture. Set aside while you make the sauce, or refrigerate overnight if you’re grilling the next day.",
                        "position": 2,
                        "start_time": 84000,
                        "end_time": 107666,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "position": 3,
                        "start_time": 121000,
                        "end_time": 197833,
                        "temperature": null,
                        "appliance": null,
                        "id": 55468,
                        "display_text": "Melt the butter in a small saucepan over medium heat. Add the raspberries and cook until  they start to break down, about 3 minutes. Add the brown sugar, chipotle powder, remaining 2 tablespoons garlic powder, remaining 2 tablespoons onion powder, the ketchup, and liquid smoke and season with salt and pepper to taste. Bring to a simmer and cook until the sauce thickens, 4 minutes. Remove the pot from the heat and set aside."
                    },
                    {
                        "end_time": 420666,
                        "temperature": null,
                        "appliance": null,
                        "id": 55469,
                        "display_text": "Heat a grill pan over medium-high heat. Grease with grapeseed oil. Add the chicken and grill for about 7 minutes on each side, until the internal temperature of the chicken reaches 165°F (75°C). Once the chicken has cooked through, generously brush with some of the raspberry chipotle sauce. Continue grilling until the sauce caramelizes, 1–2 minutes, then repeat on the other side.",
                        "position": 4,
                        "start_time": 391666
                    },
                    {
                        "id": 55470,
                        "display_text": "Garnish the chicken with the parsley and serve warm with the remaining sauce alongside.",
                        "position": 5,
                        "start_time": 0,
                        "end_time": 0,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "position": 6,
                        "start_time": 491000,
                        "end_time": 494166,
                        "temperature": null,
                        "appliance": null,
                        "id": 55471,
                        "display_text": "Enjoy!"
                    }
                ],
                "nutrition": {
                    "carbohydrates": 29,
                    "fat": 19,
                    "protein": 28,
                    "sugar": 11,
                    "fiber": 6,
                    "updated_at": "2020-05-22T08:07:45+02:00",
                    "calories": 389
                }
            }
        ],
        "description": null,
        "draft_status": "published",
        "show_id": 17,
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "aspect_ratio": "16:9",
        "is_shoppable": false,
        "renditions": [
            {
                "duration": 532248,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/167982/square_720/1590081522",
                "width": 720,
                "file_size": 77067592,
                "bit_rate": 1159,
                "maximum_bit_rate": null,
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_720/1590081522_00001.png",
                "name": "mp4_720x404",
                "height": 404
            },
            {
                "duration": 532248,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "container": "mp4",
                "height": 180,
                "width": 320,
                "url": "https://vid.tasty.co/output/167982/square_320/1590081522",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_320/1590081522_00001.png",
                "name": "mp4_320x180",
                "file_size": 30288430,
                "bit_rate": 456
            },
            {
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/167982/landscape_720/1590081522",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_720/1590081522_00001.png",
                "name": "mp4_1280x720",
                "height": 720,
                "width": 1280,
                "file_size": 169396597,
                "bit_rate": 2547,
                "container": "mp4",
                "duration": 532248,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4"
            },
            {
                "file_size": 66217816,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "height": 360,
                "width": 640,
                "duration": 532248,
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167982/landscape_480/1590081522",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_480/1590081522_00001.png",
                "name": "mp4_640x360",
                "bit_rate": 996,
                "minimum_bit_rate": null,
                "aspect": "landscape"
            },
            {
                "height": 1080,
                "width": 1920,
                "bit_rate": null,
                "minimum_bit_rate": 272,
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/1445289064805-h2exzu/1590081522_00001.png",
                "name": "low",
                "duration": 532240,
                "file_size": null,
                "maximum_bit_rate": 4902,
                "content_type": "application/vnd.apple.mpegurl",
                "aspect": "landscape",
                "container": "ts",
                "url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8"
            }
        ],
        "created_at": 1590023618,
        "updated_at": 1590499995,
        "video_id": 104514,
        "facebook_posts": [],
        "country": "US",
        "id": 1486,
        "language": "eng",
        "name": "How To BBQ on a Stove Top Grill",
        "canonical_id": "compilation:1486",
        "tags": [
            {
                "id": 64444,
                "name": "american",
                "display_name": "American",
                "type": "cuisine"
            },
            {
                "display_name": "Under 30 Minutes",
                "type": "difficulty",
                "id": 64472,
                "name": "under_30_minutes"
            },
            {
                "id": 64501,
                "name": "game_day",
                "display_name": "Game Day",
                "type": "occasion"
            },
            {
                "id": 64510,
                "name": "summer",
                "display_name": "Summer",
                "type": "seasonal"
            },
            {
                "name": "spring",
                "display_name": "Spring",
                "type": "seasonal",
                "id": 64509
            },
            {
                "id": 64504,
                "name": "bbq",
                "display_name": "BBQ",
                "type": "occasion"
            }
        ],
        "credits": [
            {
                "name": "Nichi Hoskins",
                "type": "internal"
            }
        ],
        "approved_at": 1590499995,
        "beauty_url": null,
        "keywords": ", amazon review, bbq, charred, chef out of water, chicken, cook out, gadget review, grill, how to, lodge stove top grill, make it fancy, stove top, trendy vs traditional, worth it",
        "buzz_id": null
    },
    {
        "sections": [
            {
                "name": null,
                "position": 1,
                "components": [
                    {
                        "ingredient": {
                            "display_plural": "garlic powders",
                            "created_at": 1493307128,
                            "updated_at": 1509035289,
                            "id": 9,
                            "name": "garlic powder",
                            "display_singular": "garlic powder"
                        },
                        "id": 68009,
                        "raw_text": "4 tablespoons garlic powder, divided",
                        "extra_comment": "divided",
                        "position": 1,
                        "measurements": [
                            {
                                "id": 566560,
                                "quantity": "4",
                                "unit": {
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon"
                                }
                            }
                        ]
                    },
                    {
                        "measurements": [
                            {
                                "quantity": "4",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                },
                                "id": 566571
                            }
                        ],
                        "ingredient": {
                            "id": 8,
                            "name": "onion powder",
                            "display_singular": "onion powder",
                            "display_plural": "onion powders",
                            "created_at": 1493307116,
                            "updated_at": 1509035289
                        },
                        "id": 68010,
                        "raw_text": "4 tablespoons onion powder, divided",
                        "extra_comment": "divided",
                        "position": 2
                    },
                    {
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 566561,
                                "quantity": "2",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "smoked paprika",
                            "display_plural": "smoked paprikas",
                            "created_at": 1495929686,
                            "updated_at": 1509035226,
                            "id": 878,
                            "name": "smoked paprika"
                        },
                        "id": 68011,
                        "raw_text": "2 tablespoons smoked paprika"
                    },
                    {
                        "ingredient": {
                            "id": 11,
                            "name": "kosher salt",
                            "display_singular": "kosher salt",
                            "display_plural": "kosher salts",
                            "created_at": 1493307153,
                            "updated_at": 1509035289
                        },
                        "id": 68012,
                        "raw_text": "2 tablespoons kosher salt, plus more to taste",
                        "extra_comment": "plus more to taste",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 566572,
                                "quantity": "2",
                                "unit": {
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon"
                                }
                            }
                        ]
                    },
                    {
                        "id": 68013,
                        "raw_text": "2 tablespoons freshly ground black pepper, plus more to taste",
                        "extra_comment": "plus more to taste",
                        "position": 5,
                        "measurements": [
                            {
                                "id": 566564,
                                "quantity": "2",
                                "unit": {
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 166,
                            "name": "freshly ground black pepper",
                            "display_singular": "freshly ground black pepper",
                            "display_plural": "freshly ground black peppers",
                            "created_at": 1493925438,
                            "updated_at": 1509035282
                        }
                    },
                    {
                        "extra_comment": "",
                        "position": 6,
                        "measurements": [
                            {
                                "quantity": "6",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                },
                                "id": 566563
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1562791202,
                            "id": 5576,
                            "name": "boneless skinless chicken thighs",
                            "display_singular": "boneless skinless chicken thigh",
                            "display_plural": "boneless skinless chicken thighs",
                            "created_at": 1562791202
                        },
                        "id": 68014,
                        "raw_text": "6-8 boneless, skinless chicken thighs"
                    },
                    {
                        "id": 68015,
                        "raw_text": "2 tablespoons olive oil",
                        "extra_comment": "",
                        "position": 7,
                        "measurements": [
                            {
                                "id": 566562,
                                "quantity": "2",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "olive oils",
                            "created_at": 1493306183,
                            "updated_at": 1509035290,
                            "id": 4,
                            "name": "olive oil",
                            "display_singular": "olive oil"
                        }
                    },
                    {
                        "raw_text": "1 tablespoon unsalted butter",
                        "extra_comment": "",
                        "position": 8,
                        "measurements": [
                            {
                                "id": 566565,
                                "quantity": "1",
                                "unit": {
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "unsalted butter",
                            "display_singular": "unsalted butter",
                            "display_plural": "unsalted butters",
                            "created_at": 1494806355,
                            "updated_at": 1509035272,
                            "id": 291
                        },
                        "id": 68016
                    },
                    {
                        "ingredient": {
                            "id": 431,
                            "name": "raspberry",
                            "display_singular": "raspberry",
                            "display_plural": "raspberries",
                            "created_at": 1495065307,
                            "updated_at": 1509035261
                        },
                        "id": 68017,
                        "raw_text": "6 ounces raspberries",
                        "extra_comment": "",
                        "position": 9,
                        "measurements": [
                            {
                                "id": 566576,
                                "quantity": "170",
                                "unit": {
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g"
                                }
                            },
                            {
                                "id": 566575,
                                "quantity": "6",
                                "unit": {
                                    "name": "ounce",
                                    "abbreviation": "oz",
                                    "display_singular": "oz",
                                    "display_plural": "oz",
                                    "system": "imperial"
                                }
                            }
                        ]
                    },
                    {
                        "id": 68018,
                        "raw_text": "2 tablespoons brown sugar",
                        "extra_comment": "",
                        "position": 10,
                        "measurements": [
                            {
                                "id": 566570,
                                "quantity": "2",
                                "unit": {
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 6,
                            "name": "brown sugar",
                            "display_singular": "brown sugar",
                            "display_plural": "brown sugars",
                            "created_at": 1493307081,
                            "updated_at": 1509035289
                        }
                    },
                    {
                        "id": 68019,
                        "raw_text": "1 tablespoon chipotle chile powder",
                        "extra_comment": "",
                        "position": 11,
                        "measurements": [
                            {
                                "id": 566574,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 2083,
                            "name": "chipotle powder",
                            "display_singular": "chipotle powder",
                            "display_plural": "chipotle powders",
                            "created_at": 1499981021,
                            "updated_at": 1509035145
                        }
                    },
                    {
                        "ingredient": {
                            "id": 14,
                            "name": "ketchup",
                            "display_singular": "ketchup",
                            "display_plural": "ketchups",
                            "created_at": 1493307243,
                            "updated_at": 1509035289
                        },
                        "id": 68020,
                        "raw_text": "½ cup ketchup",
                        "extra_comment": "",
                        "position": 12,
                        "measurements": [
                            {
                                "quantity": "120",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                },
                                "id": 566567
                            },
                            {
                                "id": 566566,
                                "quantity": "½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            }
                        ]
                    },
                    {
                        "position": 13,
                        "measurements": [
                            {
                                "id": 566569,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "mesquite liquid smoke",
                            "display_singular": "mesquite liquid smoke",
                            "display_plural": "mesquite liquid smokes",
                            "created_at": 1590075979,
                            "updated_at": 1590075979,
                            "id": 6441
                        },
                        "id": 68021,
                        "raw_text": "1 tablespoon mesquite liquid smoke",
                        "extra_comment": ""
                    },
                    {
                        "id": 68022,
                        "raw_text": "Grapeseed oil or nonstick cooking spray, for greasing",
                        "extra_comment": "for greasing",
                        "position": 14,
                        "measurements": [
                            {
                                "id": 566568,
                                "quantity": "0",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "nonstick cooking spray",
                            "display_singular": "nonstick cooking spray",
                            "display_plural": "nonstick cooking sprays",
                            "created_at": 1520176895,
                            "updated_at": 1520176895,
                            "id": 3826
                        }
                    },
                    {
                        "id": 68023,
                        "raw_text": "1 tablespoon minced fresh flat-leaf parsley, for garnish",
                        "extra_comment": "minced, for garnish",
                        "position": 15,
                        "measurements": [
                            {
                                "id": 566573,
                                "quantity": "1",
                                "unit": {
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon"
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035132,
                            "id": 2290,
                            "name": "fresh flat-leaf parsley",
                            "display_singular": "fresh flat-leaf parsley",
                            "display_plural": "fresh flat-leaf parsleys",
                            "created_at": 1500483640
                        }
                    }
                ]
            }
        ],
        "renditions": [
            {
                "aspect": "landscape",
                "width": 720,
                "duration": 532248,
                "file_size": 77067592,
                "bit_rate": 1159,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "url": "https://vid.tasty.co/output/167982/square_720/1590081522",
                "height": 404,
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_720/1590081522_00001.png",
                "name": "mp4_720x404"
            },
            {
                "bit_rate": 456,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "container": "mp4",
                "duration": 532248,
                "file_size": 30288430,
                "name": "mp4_320x180",
                "url": "https://vid.tasty.co/output/167982/square_320/1590081522",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_320/1590081522_00001.png",
                "height": 180,
                "width": 320
            },
            {
                "url": "https://vid.tasty.co/output/167982/landscape_720/1590081522",
                "height": 720,
                "duration": 532248,
                "bit_rate": 2547,
                "minimum_bit_rate": null,
                "aspect": "landscape",
                "container": "mp4",
                "width": 1280,
                "file_size": 169396597,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_720/1590081522_00001.png",
                "name": "mp4_1280x720"
            },
            {
                "height": 360,
                "duration": 532248,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "aspect": "landscape",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167982/landscape_480/1590081522",
                "name": "mp4_640x360",
                "width": 640,
                "file_size": 66217816,
                "bit_rate": 996,
                "content_type": "video/mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_480/1590081522_00001.png"
            },
            {
                "maximum_bit_rate": 4902,
                "content_type": "application/vnd.apple.mpegurl",
                "height": 1080,
                "duration": 532240,
                "bit_rate": null,
                "aspect": "landscape",
                "container": "ts",
                "url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/1445289064805-h2exzu/1590081522_00001.png",
                "name": "low",
                "width": 1920,
                "file_size": null,
                "minimum_bit_rate": 272
            }
        ],
        "cook_time_minutes": 20,
        "name": "Raspberry Chipotle Chicken Thighs",
        "nutrition_visibility": "auto",
        "compilations": [
            {
                "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "country": "US",
                "id": 1486,
                "is_shoppable": false,
                "language": "eng",
                "name": "How To BBQ on a Stove Top Grill",
                "promotion": "full",
                "slug": "how-to-bbq-on-a-stove-top-grill",
                "video_id": 104514,
                "approved_at": 1590499995,
                "buzz_id": null,
                "description": null,
                "facebook_posts": [],
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266749.jpg",
                "show": [
                    {
                        "id": 17,
                        "name": "Tasty"
                    }
                ],
                "beauty_url": null,
                "created_at": 1590023618,
                "draft_status": "published",
                "aspect_ratio": "16:9",
                "keywords": null,
                "canonical_id": "compilation:1486"
            }
        ],
        "canonical_id": "recipe:6190",
        "facebook_posts": [],
        "draft_status": "published",
        "language": "eng",
        "servings_noun_plural": "servings",
        "credits": [
            {
                "name": "Nichi Hoskins",
                "type": "internal"
            }
        ],
        "brand_id": null,
        "description": "Chicken thighs grilled with a sweet, smoky, and slightly spicy sauce are sure to kick things up a notch at your next indoor or outdoor barbecue.",
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "keywords": ", amazon review, bbq, charred, chef out of water, chicken, cook out, gadget review, grill, how to, lodge stove top grill, make it fancy, stove top, trendy vs traditional, worth it",
        "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
        "approved_at": 1590499981,
        "aspect_ratio": "16:9",
        "inspired_by_url": null,
        "is_one_top": false,
        "show_id": 17,
        "total_time_minutes": 30,
        "updated_at": 1590499982,
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "beauty_url": null,
        "created_at": 1590023456,
        "is_shoppable": true,
        "promotion": "full",
        "video_ad_content": "none",
        "video_id": 104514,
        "instructions": [
            {
                "end_time": 77166,
                "temperature": null,
                "appliance": null,
                "id": 55466,
                "display_text": "In a small bowl, mix together 2 tablespoons garlic powder, 2 tablespoons onion powder, the paprika, salt, and pepper.",
                "position": 1,
                "start_time": 67666
            },
            {
                "end_time": 107666,
                "temperature": null,
                "appliance": null,
                "id": 55467,
                "display_text": "Drizzle the chicken thighs with the olive oil, then sprinkle with half of the spice mixture and rub to coat well. Sprinkle with the remaining spice mixture. Set aside while you make the sauce, or refrigerate overnight if you’re grilling the next day.",
                "position": 2,
                "start_time": 84000
            },
            {
                "end_time": 197833,
                "temperature": null,
                "appliance": null,
                "id": 55468,
                "display_text": "Melt the butter in a small saucepan over medium heat. Add the raspberries and cook until  they start to break down, about 3 minutes. Add the brown sugar, chipotle powder, remaining 2 tablespoons garlic powder, remaining 2 tablespoons onion powder, the ketchup, and liquid smoke and season with salt and pepper to taste. Bring to a simmer and cook until the sauce thickens, 4 minutes. Remove the pot from the heat and set aside.",
                "position": 3,
                "start_time": 121000
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55469,
                "display_text": "Heat a grill pan over medium-high heat. Grease with grapeseed oil. Add the chicken and grill for about 7 minutes on each side, until the internal temperature of the chicken reaches 165°F (75°C). Once the chicken has cooked through, generously brush with some of the raspberry chipotle sauce. Continue grilling until the sauce caramelizes, 1–2 minutes, then repeat on the other side.",
                "position": 4,
                "start_time": 391666,
                "end_time": 420666
            },
            {
                "id": 55470,
                "display_text": "Garnish the chicken with the parsley and serve warm with the remaining sauce alongside.",
                "position": 5,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "id": 55471,
                "display_text": "Enjoy!",
                "position": 6,
                "start_time": 491000,
                "end_time": 494166,
                "temperature": null,
                "appliance": null
            }
        ],
        "tags": [
            {
                "type": "cuisine",
                "id": 64444,
                "name": "american",
                "display_name": "American"
            },
            {
                "id": 64509,
                "name": "spring",
                "display_name": "Spring",
                "type": "seasonal"
            },
            {
                "id": 64445,
                "name": "bbq",
                "display_name": "BBQ",
                "type": "cuisine"
            },
            {
                "type": "seasonal",
                "id": 64510,
                "name": "summer",
                "display_name": "Summer"
            },
            {
                "id": 64486,
                "name": "dinner",
                "display_name": "Dinner",
                "type": "meal"
            },
            {
                "type": "difficulty",
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes"
            },
            {
                "id": 64504,
                "name": "bbq",
                "display_name": "BBQ",
                "type": "occasion"
            },
            {
                "id": 64501,
                "name": "game_day",
                "display_name": "Game Day",
                "type": "occasion"
            },
            {
                "id": 1247790,
                "name": "tongs",
                "display_name": "Tongs",
                "type": "equipment"
            },
            {
                "id": 1280510,
                "name": "mixing_bowl",
                "display_name": "Mixing Bowl",
                "type": "equipment"
            }
        ],
        "user_ratings": {
            "score": 0.903846,
            "count_positive": 47,
            "count_negative": 5
        },
        "country": "US",
        "seo_title": "Raspberry Chipotle Chicken Thighs",
        "slug": "raspberry-chipotle-chicken-thighs",
        "tips_and_ratings_enabled": true,
        "brand": null,
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/d9f405db74274f659c281a99d7d4fb07/BFV67133_Gadgetreviewstovetopgrill_FB_Final.mp4",
        "num_servings": 4,
        "prep_time_minutes": 10,
        "servings_noun_singular": "serving",
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/8bbcb9fd9a094521aab2a02f11396ab7/BFV67133_Gadgetreviewstovetopgrill_FB_Final.jpg",
        "buzz_id": null,
        "id": 6190,
        "yields": "Servings: 4",
        "nutrition": {
            "calories": 389,
            "carbohydrates": 29,
            "fat": 19,
            "protein": 28,
            "sugar": 11,
            "fiber": 6,
            "updated_at": "2020-05-22T08:07:45+02:00"
        }
    },
    {
        "buzz_id": null,
        "description": "These potatoes make the perfect side dish to accompany what you’re throwing on the grill this summer. They’re the same potato wedges you ate as a child, but grilled and seasoned to perfection!",
        "id": 6191,
        "servings_noun_plural": "servings",
        "tips_and_ratings_enabled": true,
        "yields": "Servings: 4",
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/d9f405db74274f659c281a99d7d4fb07/BFV67133_Gadgetreviewstovetopgrill_FB_Final.mp4",
        "nutrition": {
            "sugar": 0,
            "fiber": 1,
            "updated_at": "2020-05-23T08:06:26+02:00",
            "calories": 75,
            "carbohydrates": 3,
            "fat": 7,
            "protein": 0
        },
        "aspect_ratio": "16:9",
        "is_one_top": false,
        "keywords": ", amazon review, bbq, charred, chef out of water, chicken, cook out, gadget review, grill, how to, lodge stove top grill, make it fancy, stove top, trendy vs traditional, worth it",
        "promotion": "full",
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "brand_id": null,
        "country": "US",
        "is_shoppable": true,
        "prep_time_minutes": 5,
        "slug": "smoked-paprika-potatoes",
        "nutrition_visibility": "auto",
        "compilations": [
            {
                "aspect_ratio": "16:9",
                "keywords": null,
                "promotion": "full",
                "slug": "how-to-bbq-on-a-stove-top-grill",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266749.jpg",
                "beauty_url": null,
                "buzz_id": null,
                "draft_status": "published",
                "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "facebook_posts": [],
                "approved_at": 1590499995,
                "created_at": 1590023618,
                "id": 1486,
                "is_shoppable": false,
                "language": "eng",
                "name": "How To BBQ on a Stove Top Grill",
                "video_id": 104514,
                "show": [
                    {
                        "id": 17,
                        "name": "Tasty"
                    }
                ],
                "country": "US",
                "description": null,
                "canonical_id": "compilation:1486"
            }
        ],
        "brand": null,
        "instructions": [
            {
                "id": 55472,
                "display_text": "Cut the potatoes into wedges. Add the wedges to a pot of cold salted water, then bring to a boil. Cook until the potatoes are almost tender, about 10 minutes, then drain. Spread the potatoes on a baking sheet.",
                "position": 1,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "appliance": null,
                "id": 55473,
                "display_text": "In a small bowl, mix together the paprika, salt, and pepper.",
                "position": 2,
                "start_time": 216000,
                "end_time": 221666,
                "temperature": null
            },
            {
                "id": 55474,
                "display_text": "Drizzle the potatoes with 1 tablespoon of olive oil and season with the spice mixture.",
                "position": 3,
                "start_time": 223000,
                "end_time": 233000,
                "temperature": null,
                "appliance": null
            },
            {
                "display_text": "Heat a grill pan over medium-high heat. Grease the grill with grapeseed oil. Add the potatoes to the hot grill and cook until grill marks form on both sides, about 2 minutes per side.",
                "position": 4,
                "start_time": 236000,
                "end_time": 264500,
                "temperature": null,
                "appliance": null,
                "id": 55475
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55476,
                "display_text": "Remove the potatoes from the grill, drizzle with the remaining tablespoon of olive oil, and garnish with more paprika and the parsley.",
                "position": 5,
                "start_time": 280000,
                "end_time": 291166
            },
            {
                "appliance": null,
                "id": 55477,
                "display_text": "Serve immediately or transfer to a 175°F (80°C) to keep warm until ready to serve.",
                "position": 6,
                "start_time": 0,
                "end_time": 0,
                "temperature": null
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55478,
                "display_text": "Enjoy!",
                "position": 7,
                "start_time": 488000,
                "end_time": 490833
            }
        ],
        "user_ratings": {
            "count_negative": 8,
            "score": 0.888889,
            "count_positive": 64
        },
        "video_ad_content": "none",
        "renditions": [
            {
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "container": "mp4",
                "height": 404,
                "width": 720,
                "duration": 532248,
                "maximum_bit_rate": null,
                "url": "https://vid.tasty.co/output/167982/square_720/1590081522",
                "name": "mp4_720x404",
                "file_size": 77067592,
                "bit_rate": 1159,
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_720/1590081522_00001.png"
            },
            {
                "height": 180,
                "bit_rate": 456,
                "maximum_bit_rate": null,
                "aspect": "landscape",
                "name": "mp4_320x180",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_320/1590081522_00001.png",
                "width": 320,
                "duration": 532248,
                "file_size": 30288430,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167982/square_320/1590081522"
            },
            {
                "file_size": 169396597,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "name": "mp4_1280x720",
                "height": 720,
                "duration": 532248,
                "bit_rate": 2547,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167982/landscape_720/1590081522",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_720/1590081522_00001.png",
                "width": 1280
            },
            {
                "height": 360,
                "bit_rate": 996,
                "aspect": "landscape",
                "width": 640,
                "duration": 532248,
                "file_size": 66217816,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167982/landscape_480/1590081522",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_480/1590081522_00001.png",
                "name": "mp4_640x360"
            },
            {
                "bit_rate": null,
                "minimum_bit_rate": 272,
                "content_type": "application/vnd.apple.mpegurl",
                "aspect": "landscape",
                "container": "ts",
                "url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/1445289064805-h2exzu/1590081522_00001.png",
                "height": 1080,
                "width": 1920,
                "duration": 532240,
                "file_size": null,
                "maximum_bit_rate": 4902,
                "name": "low"
            }
        ],
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/8ce2bbb006b54a008ceb06a11f84ea85/BFV67133_Gadgetreviewstovetopgrill_FB_Final.jpg",
        "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
        "sections": [
            {
                "components": [
                    {
                        "position": 1,
                        "measurements": [
                            {
                                "id": 566551,
                                "quantity": "5",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "large gold potato",
                            "display_plural": "large gold potatoes",
                            "created_at": 1590075774,
                            "updated_at": 1590075774,
                            "id": 6439,
                            "name": "large gold potatoes"
                        },
                        "id": 68024,
                        "raw_text": "5 large gold potatoes",
                        "extra_comment": ""
                    },
                    {
                        "raw_text": "2 tablespoons smoked paprika, plus more for garnish",
                        "extra_comment": "plus more for garnish",
                        "position": 2,
                        "measurements": [
                            {
                                "quantity": "2",
                                "unit": {
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon"
                                },
                                "id": 566553
                            }
                        ],
                        "ingredient": {
                            "display_singular": "smoked paprika",
                            "display_plural": "smoked paprikas",
                            "created_at": 1495929686,
                            "updated_at": 1509035226,
                            "id": 878,
                            "name": "smoked paprika"
                        },
                        "id": 68025
                    },
                    {
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 566552,
                                "quantity": "1",
                                "unit": {
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 11,
                            "name": "kosher salt",
                            "display_singular": "kosher salt",
                            "display_plural": "kosher salts",
                            "created_at": 1493307153,
                            "updated_at": 1509035289
                        },
                        "id": 68026,
                        "raw_text": "1 tablespoon kosher salt"
                    },
                    {
                        "id": 68027,
                        "raw_text": "1 tablespoon ground black pepper",
                        "extra_comment": "",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 566556,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 232,
                            "name": "ground black pepper",
                            "display_singular": "ground black pepper",
                            "display_plural": "ground black peppers",
                            "created_at": 1494292509,
                            "updated_at": 1509035277
                        }
                    },
                    {
                        "position": 5,
                        "measurements": [
                            {
                                "quantity": "1",
                                "unit": {
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp"
                                },
                                "id": 566558
                            }
                        ],
                        "ingredient": {
                            "display_plural": "granulated garlics",
                            "created_at": 1496755079,
                            "updated_at": 1509035188,
                            "id": 1446,
                            "name": "granulated garlic",
                            "display_singular": "granulated garlic"
                        },
                        "id": 68028,
                        "raw_text": "1 tablespoon granulated garlic",
                        "extra_comment": ""
                    },
                    {
                        "id": 68029,
                        "raw_text": "1 tablespoon granulated onion",
                        "extra_comment": "",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 566554,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1530334356,
                            "id": 4416,
                            "name": "granulated onion",
                            "display_singular": "granulated onion",
                            "display_plural": "granulated onions",
                            "created_at": 1530334356
                        }
                    },
                    {
                        "position": 7,
                        "measurements": [
                            {
                                "quantity": "2",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                },
                                "id": 566555
                            }
                        ],
                        "ingredient": {
                            "display_plural": "olive oils",
                            "created_at": 1493306183,
                            "updated_at": 1509035290,
                            "id": 4,
                            "name": "olive oil",
                            "display_singular": "olive oil"
                        },
                        "id": 68030,
                        "raw_text": "2 tablespoons olive oil, divided",
                        "extra_comment": "divided"
                    },
                    {
                        "position": 8,
                        "measurements": [
                            {
                                "unit": {
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": ""
                                },
                                "id": 566559,
                                "quantity": "0"
                            }
                        ],
                        "ingredient": {
                            "display_plural": "nonstick cooking sprays",
                            "created_at": 1520176895,
                            "updated_at": 1520176895,
                            "id": 3826,
                            "name": "nonstick cooking spray",
                            "display_singular": "nonstick cooking spray"
                        },
                        "id": 68031,
                        "raw_text": "Grapeseed oil or nonstick cooking spray, for greasing",
                        "extra_comment": "for greasing"
                    },
                    {
                        "extra_comment": "minced",
                        "position": 9,
                        "measurements": [
                            {
                                "id": 566557,
                                "quantity": "2",
                                "unit": {
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 2290,
                            "name": "fresh flat-leaf parsley",
                            "display_singular": "fresh flat-leaf parsley",
                            "display_plural": "fresh flat-leaf parsleys",
                            "created_at": 1500483640,
                            "updated_at": 1509035132
                        },
                        "id": 68032,
                        "raw_text": "2 tablespoon minced fresh flat-leaf parsley"
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "tags": [
            {
                "id": 64501,
                "name": "game_day",
                "display_name": "Game Day",
                "type": "occasion"
            },
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "type": "cuisine",
                "id": 64444,
                "name": "american",
                "display_name": "American"
            },
            {
                "id": 64510,
                "name": "summer",
                "display_name": "Summer",
                "type": "seasonal"
            },
            {
                "id": 64504,
                "name": "bbq",
                "display_name": "BBQ",
                "type": "occasion"
            },
            {
                "id": 64471,
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty"
            },
            {
                "id": 64445,
                "name": "bbq",
                "display_name": "BBQ",
                "type": "cuisine"
            },
            {
                "type": "seasonal",
                "id": 64509,
                "name": "spring",
                "display_name": "Spring"
            },
            {
                "id": 64490,
                "name": "sides",
                "display_name": "Sides",
                "type": "meal"
            },
            {
                "id": 64475,
                "name": "fourth_of_july",
                "display_name": "Fourth of July",
                "type": "holiday"
            },
            {
                "id": 1280500,
                "name": "baking_pan",
                "display_name": "Baking Pan",
                "type": "equipment"
            },
            {
                "display_name": "Tongs",
                "type": "equipment",
                "id": 1247790,
                "name": "tongs"
            }
        ],
        "canonical_id": "recipe:6191",
        "approved_at": 1590499976,
        "language": "eng",
        "seo_title": "",
        "servings_noun_singular": "serving",
        "total_time_minutes": 20,
        "updated_at": 1590499976,
        "credits": [
            {
                "name": "Nichi Hoskins",
                "type": "internal"
            }
        ],
        "created_at": 1590023618,
        "draft_status": "published",
        "name": "Smoked Paprika Potatoes",
        "num_servings": 4,
        "show_id": 17,
        "cook_time_minutes": 15,
        "beauty_url": null,
        "inspired_by_url": null,
        "video_id": 104514,
        "facebook_posts": []
    },
    {
        "beauty_url": null,
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/18262488970e4136850556de72bd68de/BFV67133_Gadgetreviewstovetopgrill_FB_Final.jpg",
        "nutrition_visibility": "auto",
        "yields": "Servings: 4",
        "cook_time_minutes": 6,
        "is_shoppable": true,
        "prep_time_minutes": 10,
        "servings_noun_plural": "servings",
        "approved_at": 1590499970,
        "keywords": ", amazon review, bbq, charred, chef out of water, chicken, cook out, gadget review, grill, how to, lodge stove top grill, make it fancy, stove top, trendy vs traditional, worth it",
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/d9f405db74274f659c281a99d7d4fb07/BFV67133_Gadgetreviewstovetopgrill_FB_Final.mp4",
        "nutrition": {
            "fat": 15,
            "protein": 6,
            "sugar": 6,
            "fiber": 8,
            "updated_at": "2020-05-23T08:06:26+02:00",
            "calories": 232,
            "carbohydrates": 23
        },
        "seo_title": "Charred Cumin Broccolini",
        "updated_at": 1590499970,
        "brand": null,
        "video_ad_content": "none",
        "aspect_ratio": "16:9",
        "draft_status": "published",
        "id": 6192,
        "inspired_by_url": null,
        "description": "Broccolini is a fun vegetable that is perfect for the grill! While usually roasted in the oven, throwing broccolini on the grill adds a great char and another layer of flavor.",
        "servings_noun_singular": "serving",
        "canonical_id": "recipe:6192",
        "show_id": 17,
        "tips_and_ratings_enabled": true,
        "country": "US",
        "created_at": 1590080557,
        "name": "Charred Cumin Broccolini",
        "num_servings": 4,
        "video_id": 104514,
        "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
        "compilations": [
            {
                "buzz_id": null,
                "name": "How To BBQ on a Stove Top Grill",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266749.jpg",
                "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "canonical_id": "compilation:1486",
                "beauty_url": null,
                "id": 1486,
                "is_shoppable": false,
                "description": null,
                "keywords": null,
                "language": "eng",
                "promotion": "full",
                "facebook_posts": [],
                "created_at": 1590023618,
                "aspect_ratio": "16:9",
                "country": "US",
                "draft_status": "published",
                "slug": "how-to-bbq-on-a-stove-top-grill",
                "video_id": 104514,
                "show": [
                    {
                        "id": 17,
                        "name": "Tasty"
                    }
                ],
                "approved_at": 1590499995
            }
        ],
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "brand_id": null,
        "is_one_top": false,
        "language": "eng",
        "total_time_minutes": 16,
        "instructions": [
            {
                "position": 1,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55479,
                "display_text": "Bring a large pot of salted water to a boil. Fill a large bowl with cold water and set nearby."
            },
            {
                "display_text": "Blanch the broccolini in the boiling water for about 3 minutes, then transfer to the bowl of cold water to stop the cooking process.",
                "position": 2,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55480
            },
            {
                "id": 55481,
                "display_text": "Remove broccolini from the water, shaking off any excess, and spread on a baking sheet.",
                "position": 3,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "end_time": 303500,
                "temperature": null,
                "appliance": null,
                "id": 55482,
                "display_text": "Drizzle the broccolini with the olive oil and season with the cumin, salt, and pepper. Toss to coat.",
                "position": 4,
                "start_time": 297000
            },
            {
                "id": 55483,
                "display_text": "Heat a grill pan over medium-high heat. Grease with grapeseed oil. Add the broccolini to the hot grill and cook until char marks appear, about 3 minutes per side. Remove the broccolini from the grill.",
                "position": 5,
                "start_time": 303000,
                "end_time": 324666,
                "temperature": null,
                "appliance": null
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55484,
                "display_text": "Rub the cut sides of the lemon wedges with the sugar to lightly coat, then grill until caramelized, 1 minute on each side.",
                "position": 6,
                "start_time": 327000,
                "end_time": 335666
            },
            {
                "id": 55485,
                "display_text": "Place the caramelized lemon wedges on top of the broccolini, then squeeze over the broccolini before serving",
                "position": 7,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "id": 55486,
                "display_text": "Serve immediately or transfer to a 175°F (80°C) to keep warm until ready to serve.",
                "position": 8,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55487,
                "display_text": "Enjoy!",
                "position": 9,
                "start_time": 481000,
                "end_time": 489500
            }
        ],
        "user_ratings": {
            "count_positive": 20,
            "count_negative": 4,
            "score": 0.833333
        },
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "renditions": [
            {
                "height": 404,
                "bit_rate": 1159,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "width": 720,
                "duration": 532248,
                "file_size": 77067592,
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167982/square_720/1590081522",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_720/1590081522_00001.png",
                "name": "mp4_720x404"
            },
            {
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "container": "mp4",
                "height": 180,
                "width": 320,
                "duration": 532248,
                "file_size": 30288430,
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_320/1590081522_00001.png",
                "name": "mp4_320x180",
                "bit_rate": 456,
                "minimum_bit_rate": null,
                "url": "https://vid.tasty.co/output/167982/square_320/1590081522"
            },
            {
                "height": 720,
                "duration": 532248,
                "bit_rate": 2547,
                "url": "https://vid.tasty.co/output/167982/landscape_720/1590081522",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_720/1590081522_00001.png",
                "container": "mp4",
                "name": "mp4_1280x720",
                "width": 1280,
                "file_size": 169396597,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape"
            },
            {
                "file_size": 66217816,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4",
                "name": "mp4_640x360",
                "width": 640,
                "duration": 532248,
                "bit_rate": 996,
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/167982/landscape_480/1590081522",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_480/1590081522_00001.png",
                "height": 360
            },
            {
                "container": "ts",
                "url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "width": 1920,
                "duration": 532240,
                "maximum_bit_rate": 4902,
                "content_type": "application/vnd.apple.mpegurl",
                "aspect": "landscape",
                "name": "low",
                "height": 1080,
                "file_size": null,
                "bit_rate": null,
                "minimum_bit_rate": 272,
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/1445289064805-h2exzu/1590081522_00001.png"
            }
        ],
        "tags": [
            {
                "id": 64509,
                "name": "spring",
                "display_name": "Spring",
                "type": "seasonal"
            },
            {
                "id": 64501,
                "name": "game_day",
                "display_name": "Game Day",
                "type": "occasion"
            },
            {
                "type": "method",
                "id": 64494,
                "name": "grill",
                "display_name": "Grill"
            },
            {
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty",
                "id": 64472
            },
            {
                "id": 64469,
                "name": "vegetarian",
                "display_name": "Vegetarian",
                "type": "dietary"
            },
            {
                "id": 64504,
                "name": "bbq",
                "display_name": "BBQ",
                "type": "occasion"
            },
            {
                "display_name": "BBQ",
                "type": "cuisine",
                "id": 64445,
                "name": "bbq"
            },
            {
                "display_name": "Vegan",
                "type": "dietary",
                "id": 64468,
                "name": "vegan"
            },
            {
                "id": 64466,
                "name": "healthy",
                "display_name": "Healthy",
                "type": "dietary"
            },
            {
                "type": "difficulty",
                "id": 64471,
                "name": "easy",
                "display_name": "Easy"
            },
            {
                "type": "meal",
                "id": 64490,
                "name": "sides",
                "display_name": "Sides"
            },
            {
                "name": "american",
                "display_name": "American",
                "type": "cuisine",
                "id": 64444
            },
            {
                "id": 64510,
                "name": "summer",
                "display_name": "Summer",
                "type": "seasonal"
            }
        ],
        "credits": [
            {
                "name": "Nichi Hoskins",
                "type": "internal"
            }
        ],
        "facebook_posts": [],
        "buzz_id": null,
        "promotion": "full",
        "slug": "charred-cumin-broccolini",
        "sections": [
            {
                "position": 1,
                "components": [
                    {
                        "id": 68033,
                        "raw_text": "1 tablespoon kosher salt, plus more for boiling",
                        "extra_comment": "plus more for boiling",
                        "position": 1,
                        "measurements": [
                            {
                                "id": 566541,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 11,
                            "name": "kosher salt",
                            "display_singular": "kosher salt",
                            "display_plural": "kosher salts",
                            "created_at": 1493307153,
                            "updated_at": 1509035289
                        }
                    },
                    {
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "id": 566545,
                                "quantity": "2",
                                "unit": {
                                    "system": "imperial",
                                    "name": "pound",
                                    "abbreviation": "lb",
                                    "display_singular": "lb",
                                    "display_plural": "lb"
                                }
                            },
                            {
                                "unit": {
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g"
                                },
                                "id": 566542,
                                "quantity": "850"
                            }
                        ],
                        "ingredient": {
                            "id": 4926,
                            "name": "broccolini",
                            "display_singular": "broccolini",
                            "display_plural": "broccolinis",
                            "created_at": 1542667786,
                            "updated_at": 1542667786
                        },
                        "id": 68034,
                        "raw_text": "2 pounds broccolini (about 3 bundles )"
                    },
                    {
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 566544,
                                "quantity": "2",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 4,
                            "name": "olive oil",
                            "display_singular": "olive oil",
                            "display_plural": "olive oils",
                            "created_at": 1493306183,
                            "updated_at": 1509035290
                        },
                        "id": 68035,
                        "raw_text": "2 tablespoons olive oil"
                    },
                    {
                        "id": 68036,
                        "raw_text": "2 tablespoons cumin",
                        "extra_comment": "",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 566543,
                                "quantity": "2",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1493906367,
                            "updated_at": 1509035283,
                            "id": 151,
                            "name": "cumin",
                            "display_singular": "cumin",
                            "display_plural": "cumins"
                        }
                    },
                    {
                        "measurements": [
                            {
                                "id": 566546,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1493307153,
                            "updated_at": 1509035289,
                            "id": 11,
                            "name": "kosher salt",
                            "display_singular": "kosher salt",
                            "display_plural": "kosher salts"
                        },
                        "id": 68037,
                        "raw_text": "1 tablespoon kosher salt",
                        "extra_comment": "",
                        "position": 5
                    },
                    {
                        "id": 68038,
                        "raw_text": "1 tablespoon  ground black pepper",
                        "extra_comment": "",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 566547,
                                "quantity": "1",
                                "unit": {
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1494292509,
                            "updated_at": 1509035277,
                            "id": 232,
                            "name": "ground black pepper",
                            "display_singular": "ground black pepper",
                            "display_plural": "ground black peppers"
                        }
                    },
                    {
                        "position": 7,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                },
                                "id": 566550,
                                "quantity": "1"
                            }
                        ],
                        "ingredient": {
                            "display_plural": "lemons",
                            "created_at": 1493906426,
                            "updated_at": 1509035282,
                            "id": 155,
                            "name": "lemon",
                            "display_singular": "lemon"
                        },
                        "id": 68039,
                        "raw_text": "1 lemon, cut into wedges",
                        "extra_comment": "cut into wedges"
                    },
                    {
                        "position": 8,
                        "measurements": [
                            {
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                },
                                "id": 566548
                            }
                        ],
                        "ingredient": {
                            "display_singular": "sugar",
                            "display_plural": "sugars",
                            "created_at": 1493314650,
                            "updated_at": 1509035288,
                            "id": 24,
                            "name": "sugar"
                        },
                        "id": 68040,
                        "raw_text": "1 tablespoon sugar",
                        "extra_comment": ""
                    },
                    {
                        "measurements": [
                            {
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                },
                                "id": 566549,
                                "quantity": "0"
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035139,
                            "id": 2166,
                            "name": "grapeseed oil",
                            "display_singular": "grapeseed oil",
                            "display_plural": "grapeseed oils",
                            "created_at": 1500176867
                        },
                        "id": 68041,
                        "raw_text": "Grapeseed oil or nonstick cooking spray, for greasing",
                        "extra_comment": "or nonstick cooking spray, for greasing",
                        "position": 9
                    }
                ],
                "name": null
            }
        ]
    },
    {
        "id": 6193,
        "language": "eng",
        "yields": "Servings: 4",
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "promotion": "full",
        "updated_at": 1590499966,
        "compilations": [
            {
                "created_at": 1590023618,
                "description": null,
                "keywords": null,
                "slug": "how-to-bbq-on-a-stove-top-grill",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266749.jpg",
                "facebook_posts": [],
                "promotion": "full",
                "video_id": 104514,
                "canonical_id": "compilation:1486",
                "beauty_url": null,
                "buzz_id": null,
                "country": "US",
                "id": 1486,
                "is_shoppable": false,
                "name": "How To BBQ on a Stove Top Grill",
                "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "show": [
                    {
                        "id": 17,
                        "name": "Tasty"
                    }
                ],
                "approved_at": 1590499995,
                "aspect_ratio": "16:9",
                "draft_status": "published",
                "language": "eng"
            }
        ],
        "brand": null,
        "approved_at": 1590499966,
        "draft_status": "published",
        "inspired_by_url": null,
        "name": "Grilled Sweet Corn With Lemon Pepper Butter",
        "user_ratings": {
            "count_negative": 7,
            "score": 0.890625,
            "count_positive": 57
        },
        "credits": [
            {
                "type": "internal",
                "name": "Nichi Hoskins"
            }
        ],
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/831cf26059bb41348dcf37929c0e0d30/BFV67133_Gadgetreviewstovetopgrill_FB_Final.jpg",
        "instructions": [
            {
                "appliance": null,
                "id": 55488,
                "display_text": "In a small bowl, stir together the butter, lemon zest, and lemon pepper until well combined.",
                "position": 1,
                "start_time": 358500,
                "end_time": 366666,
                "temperature": null
            },
            {
                "id": 55489,
                "display_text": "Heat a grill pan over medium-high heat. Grease with grapeseed oil. Brush one side of each ear of corn with the butter mixture, then add to the grill, buttered-side down. Cook until char marks appear, about 5 minutes per side.",
                "position": 2,
                "start_time": 370000,
                "end_time": 377500,
                "temperature": null,
                "appliance": null
            },
            {
                "id": 55490,
                "display_text": "Transfer the corn to a serving platter and brush with more butter. Garnish with the parsley and serve with lemon wedges to squeeze over the corn.",
                "position": 3,
                "start_time": 379000,
                "end_time": 386000,
                "temperature": null,
                "appliance": null
            },
            {
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55491,
                "display_text": "Serve immediately or transfer to a 175°F (80°C) to keep warm until ready to serve.",
                "position": 4
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55492,
                "display_text": "Enjoy!",
                "position": 5,
                "start_time": 484000,
                "end_time": 492666
            }
        ],
        "beauty_url": null,
        "brand_id": null,
        "description": "Grilled corn is a mandatory item when we talk about a summer BBQ, but the addition of lemon pepper butter to this classic dish is guaranteed to wow your guests.",
        "show_id": 17,
        "tips_and_ratings_enabled": true,
        "tags": [
            {
                "id": 64490,
                "name": "sides",
                "display_name": "Sides",
                "type": "meal"
            },
            {
                "id": 64504,
                "name": "bbq",
                "display_name": "BBQ",
                "type": "occasion"
            },
            {
                "id": 64501,
                "name": "game_day",
                "display_name": "Game Day",
                "type": "occasion"
            },
            {
                "id": 64494,
                "name": "grill",
                "display_name": "Grill",
                "type": "method"
            },
            {
                "id": 64509,
                "name": "spring",
                "display_name": "Spring",
                "type": "seasonal"
            },
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty",
                "id": 64471
            },
            {
                "id": 64475,
                "name": "fourth_of_july",
                "display_name": "Fourth of July",
                "type": "holiday"
            },
            {
                "id": 64444,
                "name": "american",
                "display_name": "American",
                "type": "cuisine"
            },
            {
                "id": 64510,
                "name": "summer",
                "display_name": "Summer",
                "type": "seasonal"
            },
            {
                "type": "cuisine",
                "id": 64445,
                "name": "bbq",
                "display_name": "BBQ"
            },
            {
                "display_name": "Low-Carb",
                "type": "dietary",
                "id": 64467,
                "name": "low_carb"
            },
            {
                "id": 64469,
                "name": "vegetarian",
                "display_name": "Vegetarian",
                "type": "dietary"
            },
            {
                "id": 64468,
                "name": "vegan",
                "display_name": "Vegan",
                "type": "dietary"
            },
            {
                "name": "healthy",
                "display_name": "Healthy",
                "type": "dietary",
                "id": 64466
            }
        ],
        "country": "US",
        "num_servings": 4,
        "prep_time_minutes": 5,
        "seo_title": "Grilled Sweet Corn with Lemon Pepper Butter",
        "is_shoppable": true,
        "slug": "grilled-sweet-corn-with-lemon-pepper-butter",
        "video_id": 104514,
        "sections": [
            {
                "components": [
                    {
                        "extra_comment": "softened",
                        "position": 1,
                        "measurements": [
                            {
                                "quantity": "2",
                                "unit": {
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon"
                                },
                                "id": 566534
                            }
                        ],
                        "ingredient": {
                            "display_singular": "unsalted butter",
                            "display_plural": "unsalted butters",
                            "created_at": 1494806355,
                            "updated_at": 1509035272,
                            "id": 291,
                            "name": "unsalted butter"
                        },
                        "id": 68042,
                        "raw_text": "2 tablespoons unsalted butter, softened"
                    },
                    {
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "id": 566536,
                                "quantity": "2",
                                "unit": {
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1494124243,
                            "updated_at": 1518723289,
                            "id": 194,
                            "name": "lemon zest",
                            "display_singular": "lemon zest",
                            "display_plural": "lemon zests"
                        },
                        "id": 68043,
                        "raw_text": "2 tablespoons lemon zest"
                    },
                    {
                        "ingredient": {
                            "name": "lemon pepper seasoning",
                            "display_singular": "lemon pepper seasoning",
                            "display_plural": "lemon pepper seasonings",
                            "created_at": 1590154281,
                            "updated_at": 1590154281,
                            "id": 6450
                        },
                        "id": 68044,
                        "raw_text": "1 tablespoon lemon pepper seasoning",
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 566537,
                                "quantity": "1",
                                "unit": {
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon"
                                }
                            }
                        ]
                    },
                    {
                        "id": 68045,
                        "raw_text": "Grapeseed oil or nonstick cooking spray, for greasing",
                        "extra_comment": "for greasing",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 566535,
                                "quantity": "0",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "nonstick cooking spray",
                            "display_singular": "nonstick cooking spray",
                            "display_plural": "nonstick cooking sprays",
                            "created_at": 1520176895,
                            "updated_at": 1520176895,
                            "id": 3826
                        }
                    },
                    {
                        "extra_comment": "husks removed",
                        "position": 5,
                        "measurements": [
                            {
                                "id": 566538,
                                "quantity": "4",
                                "unit": {
                                    "display_plural": "ears",
                                    "system": "none",
                                    "name": "ear",
                                    "abbreviation": "ear",
                                    "display_singular": "ear"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "sweet corn",
                            "display_plural": "sweet corns",
                            "created_at": 1574825126,
                            "updated_at": 1574825126,
                            "id": 5996,
                            "name": "sweet corn"
                        },
                        "id": 68046,
                        "raw_text": "4 ears of sweet corn, husks removed"
                    },
                    {
                        "id": 68047,
                        "raw_text": "1 tablespoon minced fresh flat-leaf parsley, for garnish",
                        "extra_comment": "for garnish",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 566539,
                                "quantity": "1",
                                "unit": {
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 2290,
                            "name": "fresh flat-leaf parsley",
                            "display_singular": "fresh flat-leaf parsley",
                            "display_plural": "fresh flat-leaf parsleys",
                            "created_at": 1500483640,
                            "updated_at": 1509035132
                        }
                    },
                    {
                        "position": 7,
                        "measurements": [
                            {
                                "id": 566540,
                                "quantity": "0",
                                "unit": {
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 4072,
                            "name": "lemon wedges",
                            "display_singular": "lemon wedge",
                            "display_plural": "lemon wedges",
                            "created_at": 1526070227,
                            "updated_at": 1526070227
                        },
                        "id": 68048,
                        "raw_text": "Lemon wedges, for serving",
                        "extra_comment": "for serving"
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "servings_noun_singular": "serving",
        "video_url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/d9f405db74274f659c281a99d7d4fb07/BFV67133_Gadgetreviewstovetopgrill_FB_Final.mp4",
        "nutrition": {
            "protein": 9,
            "sugar": 18,
            "fiber": 8,
            "updated_at": "2020-05-23T08:06:27+02:00",
            "calories": 337,
            "carbohydrates": 60,
            "fat": 10
        },
        "nutrition_visibility": "auto",
        "total_time_tier": {
            "tier": "under_15_minutes",
            "display_tier": "Under 15 minutes"
        },
        "canonical_id": "recipe:6193",
        "facebook_posts": [],
        "buzz_id": null,
        "cook_time_minutes": 10,
        "is_one_top": false,
        "total_time_minutes": 15,
        "video_ad_content": "none",
        "renditions": [
            {
                "height": 404,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_720/1590081522_00001.png",
                "name": "mp4_720x404",
                "width": 720,
                "duration": 532248,
                "file_size": 77067592,
                "bit_rate": 1159,
                "minimum_bit_rate": null,
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/167982/square_720/1590081522"
            },
            {
                "file_size": 30288430,
                "bit_rate": 456,
                "minimum_bit_rate": null,
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167982/square_320/1590081522",
                "height": 180,
                "width": 320,
                "duration": 532248,
                "name": "mp4_320x180",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/square_320/1590081522_00001.png",
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape"
            },
            {
                "width": 1280,
                "duration": 532248,
                "file_size": 169396597,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_720/1590081522_00001.png",
                "height": 720,
                "name": "mp4_1280x720",
                "aspect": "landscape",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167982/landscape_720/1590081522",
                "bit_rate": 2547
            },
            {
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/landscape_480/1590081522_00001.png",
                "duration": 532248,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "url": "https://vid.tasty.co/output/167982/landscape_480/1590081522",
                "height": 360,
                "width": 640,
                "file_size": 66217816,
                "bit_rate": 996,
                "container": "mp4",
                "name": "mp4_640x360"
            },
            {
                "height": 1080,
                "duration": 532240,
                "file_size": null,
                "bit_rate": null,
                "maximum_bit_rate": 4902,
                "aspect": "landscape",
                "container": "ts",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167982/1445289064805-h2exzu/1590081522_00001.png",
                "width": 1920,
                "minimum_bit_rate": 272,
                "content_type": "application/vnd.apple.mpegurl",
                "url": "https://vid.tasty.co/output/167982/hls24_1590081522.m3u8",
                "name": "low"
            }
        ],
        "aspect_ratio": "16:9",
        "created_at": 1590080706,
        "keywords": ", amazon review, bbq, charred, chef out of water, chicken, cook out, gadget review, grill, how to, lodge stove top grill, make it fancy, stove top, trendy vs traditional, worth it",
        "servings_noun_plural": "servings"
    },
    {
        "is_one_top": false,
        "promotion": "full",
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/f03119eaa8c445b7a0f4bd877af46910/BFV66916_IMade3MealsWithOnly5Ingredients_MJC_FINAL_YT.jpg",
        "video_id": 104177,
        "beauty_url": null,
        "country": "US",
        "created_at": 1590006554,
        "id": 6188,
        "total_time_tier": {
            "tier": "under_45_minutes",
            "display_tier": "Under 45 minutes"
        },
        "user_ratings": {
            "count_positive": 50,
            "count_negative": 9,
            "score": 0.847458
        },
        "draft_status": "published",
        "show_id": 17,
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/ef2a9700872d4c34b8d0704462709ec3/BFV66916_IMade3MealsWithOnly5Ingredients_MJC_FINAL_YT.mp4",
        "renditions": [
            {
                "url": "https://vid.tasty.co/output/167901/square_720/1590007174",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/square_720/1590007174_00001.png",
                "height": 404,
                "duration": 607945,
                "bit_rate": 898,
                "aspect": "landscape",
                "content_type": "video/mp4",
                "container": "mp4",
                "name": "mp4_720x404",
                "width": 720,
                "file_size": 68199607,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null
            },
            {
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/square_320/1590007174_00001.png",
                "name": "mp4_320x180",
                "height": 180,
                "width": 320,
                "minimum_bit_rate": null,
                "aspect": "landscape",
                "content_type": "video/mp4",
                "url": "https://vid.tasty.co/output/167901/square_320/1590007174",
                "duration": 607945,
                "file_size": 28103404,
                "bit_rate": 370,
                "maximum_bit_rate": null
            },
            {
                "maximum_bit_rate": null,
                "url": "https://vid.tasty.co/output/167901/landscape_720/1590007174",
                "bit_rate": 2032,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "height": 720,
                "width": 1280,
                "duration": 607945,
                "file_size": 154384786,
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/landscape_720/1590007174_00001.png",
                "name": "mp4_1280x720"
            },
            {
                "url": "https://vid.tasty.co/output/167901/landscape_480/1590007174",
                "name": "mp4_640x360",
                "width": 640,
                "duration": 607945,
                "bit_rate": 771,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/landscape_480/1590007174_00001.png",
                "height": 360,
                "file_size": 58572041,
                "maximum_bit_rate": null,
                "aspect": "landscape",
                "container": "mp4"
            },
            {
                "aspect": "landscape",
                "container": "ts",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/1445289064805-h2exzu/1590007174_00001.png",
                "name": "low",
                "height": 1080,
                "minimum_bit_rate": 271,
                "content_type": "application/vnd.apple.mpegurl",
                "bit_rate": null,
                "maximum_bit_rate": 4093,
                "url": "https://vid.tasty.co/output/167901/hls24_1590007174.m3u8",
                "width": 1920,
                "duration": 607941,
                "file_size": null
            }
        ],
        "servings_noun_plural": "servings",
        "updated_at": 1590151388,
        "compilations": [
            {
                "aspect_ratio": "16:9",
                "country": "US",
                "description": null,
                "id": 1485,
                "language": "eng",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266618.jpg",
                "promotion": "full",
                "video_id": 104177,
                "video_url": "https://vid.tasty.co/output/167901/hls24_1590007174.m3u8",
                "approved_at": 1590151400,
                "is_shoppable": true,
                "keywords": null,
                "name": "I Made 3 Meals With Only 5 Ingredients",
                "canonical_id": "compilation:1485",
                "facebook_posts": [],
                "beauty_url": null,
                "buzz_id": null,
                "created_at": 1590006554,
                "draft_status": "published",
                "slug": "i-made-3-meals-with-only-5-ingredients",
                "show": [
                    {
                        "name": "Tasty",
                        "id": 17
                    }
                ]
            }
        ],
        "brand": null,
        "credits": [
            {
                "name": "Matt Ciampa",
                "type": "internal"
            }
        ],
        "brand_id": null,
        "inspired_by_url": null,
        "is_shoppable": true,
        "servings_noun_singular": "serving",
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "nutrition": {
            "fat": 76,
            "protein": 9,
            "sugar": 2,
            "fiber": 2,
            "updated_at": "2020-05-22T08:07:45+02:00",
            "calories": 797,
            "carbohydrates": 19
        },
        "instructions": [
            {
                "temperature": null,
                "appliance": null,
                "id": 55447,
                "display_text": "Heat the olive oil in a large, wide pan with high sides over medium-high heat until shimmering. Add the potatoes, onion, and 1 teaspoon salt. Add more olive oil as needed to ensure everything is submerged. Reduce the heat to medium and cook until the potatoes are fork tender, 5–7 minutes. Remove the pan from the heat.",
                "position": 1,
                "start_time": 157000,
                "end_time": 189000
            },
            {
                "end_time": 199333,
                "temperature": null,
                "appliance": null,
                "id": 55448,
                "display_text": "Place a metal strainer over a large heatproof bowl. Carefully strain the potatoes and onion and set aside to cool. Reserve the cooking oil.",
                "position": 2,
                "start_time": 195000
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55449,
                "display_text": "In a separate large bowl, whisk together eggs, 1 teaspoon of salt, and pepper until foamy, 2–3 minutes. Gently fold in the cooled potato and onion mixture and let sit at room temperature for 5 minutes.",
                "position": 3,
                "start_time": 203000,
                "end_time": 227166
            },
            {
                "appliance": null,
                "id": 55450,
                "display_text": "Heat 3 tablespoons of the reserved oil in a 9 or 10-inch nonstick skillet over medium heat. When the oil is shimmering, pour in the egg and potato mixture and spread in an even layer. Cook, tilting the pan and lifting up the edges with spatula to let the uncooked egg run underneath, until the edges are pale yellow and lacy, 4–5 minutes.",
                "position": 4,
                "start_time": 244000,
                "end_time": 275500,
                "temperature": null
            },
            {
                "display_text": "Remove the pan from the heat and place a large heatproof plate over the skillet. Working quickly, invert the pan to flip the tortilla onto the plate.",
                "position": 5,
                "start_time": 282000,
                "end_time": 285333,
                "temperature": null,
                "appliance": null,
                "id": 55451
            },
            {
                "id": 55452,
                "display_text": "Return the skillet to medium heat, add 1 tablespoon of the reserved oil, then slide the tortilla back into the pan with the uncooked side facing down. Use spatula to tuck in the sides of the tortilla. Cook until the bottom of the tortilla is browned and the center is cooked through, 2–3 minutes.",
                "position": 6,
                "start_time": 288000,
                "end_time": 312166,
                "temperature": null,
                "appliance": null
            },
            {
                "id": 55453,
                "display_text": "Slide the tortilla onto the plate and let rest for 5–10 minutes before slicing and serving.",
                "position": 7,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "start_time": 327000,
                "end_time": 334000,
                "temperature": null,
                "appliance": null,
                "id": 55454,
                "display_text": "Enjoy!",
                "position": 8
            }
        ],
        "tags": [
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "name": "lunch",
                "display_name": "Lunch",
                "type": "meal",
                "id": 64489
            },
            {
                "id": 1247794,
                "name": "wooden_spoon",
                "display_name": "Wooden Spoon",
                "type": "equipment"
            },
            {
                "display_name": "Chef's Knife",
                "type": "equipment",
                "id": 1280501,
                "name": "chefs_knife"
            },
            {
                "display_name": "Colander",
                "type": "equipment",
                "id": 1247770,
                "name": "colander"
            },
            {
                "id": 1280508,
                "name": "measuring_spoons",
                "display_name": "Measuring Spoons",
                "type": "equipment"
            },
            {
                "id": 64471,
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty"
            },
            {
                "name": "pan_fry",
                "display_name": "Pan Fry",
                "type": "method",
                "id": 65859
            },
            {
                "id": 65848,
                "name": "stove_top",
                "display_name": "Stove Top",
                "type": "appliance"
            },
            {
                "type": "meal",
                "id": 64483,
                "name": "breakfast",
                "display_name": "Breakfast"
            },
            {
                "id": 1280510,
                "name": "mixing_bowl",
                "display_name": "Mixing Bowl",
                "type": "equipment"
            },
            {
                "name": "cutting_board",
                "display_name": "Cutting Board",
                "type": "equipment",
                "id": 1280503
            },
            {
                "type": "equipment",
                "id": 1280506,
                "name": "liquid_measuring_cup",
                "display_name": "Liquid Measuring Cup"
            },
            {
                "id": 1247793,
                "name": "whisk",
                "display_name": "Whisk",
                "type": "equipment"
            }
        ],
        "keywords": "3 dishes, 5 ingredients, basic, beginner recipes, breakfast, cooking at home, corona virus, covid-19, easy, easy recipes, eggs, eggs in purgatory, eggs in tomato, flour, gnocchi, gnocchi with tomato sauce, homemade, lunch dinner, not hard, onions, pantry ingredients, potatoes, recipes, recipes with 5 ingredients, simple, simple dishes, simples recipes, tasty, tomatoes, tortilla espanola",
        "num_servings": 6,
        "slug": "easy-tortilla-espanola",
        "total_time_minutes": 40,
        "buzz_id": null,
        "seo_title": "",
        "nutrition_visibility": "auto",
        "facebook_posts": [],
        "approved_at": 1590151388,
        "aspect_ratio": "16:9",
        "language": "eng",
        "yields": "Servings: 6",
        "tips_and_ratings_enabled": true,
        "video_url": "https://vid.tasty.co/output/167901/hls24_1590007174.m3u8",
        "sections": [
            {
                "components": [
                    {
                        "extra_comment": "plus more as needed",
                        "position": 1,
                        "measurements": [
                            {
                                "quantity": "480",
                                "unit": {
                                    "name": "milliliter",
                                    "abbreviation": "mL",
                                    "display_singular": "mL",
                                    "display_plural": "mL",
                                    "system": "metric"
                                },
                                "id": 566372
                            },
                            {
                                "id": 566371,
                                "quantity": "2",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "olive oil",
                            "display_singular": "olive oil",
                            "display_plural": "olive oils",
                            "created_at": 1493306183,
                            "updated_at": 1509035290,
                            "id": 4
                        },
                        "id": 67995,
                        "raw_text": "2 cups olive oil, plus more as needed"
                    },
                    {
                        "measurements": [
                            {
                                "id": 566378,
                                "quantity": "425",
                                "unit": {
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram"
                                }
                            },
                            {
                                "id": 566377,
                                "quantity": "1",
                                "unit": {
                                    "name": "pound",
                                    "abbreviation": "lb",
                                    "display_singular": "lb",
                                    "display_plural": "lb",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "russet potato",
                            "display_singular": "russet potato",
                            "display_plural": "russet potatoes",
                            "created_at": 1496587506,
                            "updated_at": 1509035200,
                            "id": 1232
                        },
                        "id": 67996,
                        "raw_text": "1 pound russet potatoes, peeled and sliced into ⅛-inch rounds",
                        "extra_comment": "peeled and sliced into 1/8 in rounds (1mm)",
                        "position": 2
                    },
                    {
                        "id": 67997,
                        "raw_text": "1 medium onion, thinly sliced",
                        "extra_comment": "thinly sliced",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 566375,
                                "quantity": "1",
                                "unit": {
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "medium onion",
                            "display_plural": "medium onions",
                            "created_at": 1494212624,
                            "updated_at": 1509035279,
                            "id": 213,
                            "name": "medium onion"
                        }
                    },
                    {
                        "id": 67998,
                        "raw_text": "2 teaspoons kosher salt, divided",
                        "extra_comment": "divided",
                        "position": 4,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons",
                                    "system": "imperial"
                                },
                                "id": 566373,
                                "quantity": "2"
                            }
                        ],
                        "ingredient": {
                            "display_plural": "kosher salts",
                            "created_at": 1493307153,
                            "updated_at": 1509035289,
                            "id": 11,
                            "name": "kosher salt",
                            "display_singular": "kosher salt"
                        }
                    },
                    {
                        "position": 5,
                        "measurements": [
                            {
                                "id": 566374,
                                "quantity": "6",
                                "unit": {
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035275,
                            "id": 253,
                            "name": "large egg",
                            "display_singular": "large egg",
                            "display_plural": "large eggs",
                            "created_at": 1494382414
                        },
                        "id": 67999,
                        "raw_text": "6 large eggs",
                        "extra_comment": ""
                    },
                    {
                        "extra_comment": "to taste",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 566376,
                                "quantity": "0",
                                "unit": {
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035282,
                            "id": 166,
                            "name": "freshly ground black pepper",
                            "display_singular": "freshly ground black pepper",
                            "display_plural": "freshly ground black peppers",
                            "created_at": 1493925438
                        },
                        "id": 68000,
                        "raw_text": "Freshly ground black pepper, to taste"
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "video_ad_content": "none",
        "cook_time_minutes": 20,
        "description": "This classic Spanish dish is perfect for breakfast, lunch, or dinner, or even a snack! We’re keeping the base recipe under 5 ingredients, but it would also be great with vegetables and/or Spanish ham folded in.",
        "name": "Easy Tortilla Española",
        "prep_time_minutes": 5,
        "canonical_id": "recipe:6188"
    },
    {
        "is_one_top": false,
        "num_servings": 2,
        "servings_noun_plural": "servings",
        "brand": null,
        "country": "US",
        "buzz_id": null,
        "keywords": "3 dishes, 5 ingredients, basic, beginner recipes, breakfast, cooking at home, corona virus, covid-19, easy, easy recipes, eggs, eggs in purgatory, eggs in tomato, flour, gnocchi, gnocchi with tomato sauce, homemade, lunch dinner, not hard, onions, pantry ingredients, potatoes, recipes, recipes with 5 ingredients, simple, simple dishes, simples recipes, tasty, tomatoes, tortilla espanola",
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/05d5f0821572460e983ae658a6dea4d8/BFV66916_IMade3MealsWithOnly5Ingredients_MJC_FINAL_YT.jpg",
        "tips_and_ratings_enabled": true,
        "yields": "Servings: 2",
        "tags": [
            {
                "id": 65848,
                "name": "stove_top",
                "display_name": "Stove Top",
                "type": "appliance"
            },
            {
                "id": 64483,
                "name": "breakfast",
                "display_name": "Breakfast",
                "type": "meal"
            },
            {
                "id": 65859,
                "name": "pan_fry",
                "display_name": "Pan Fry",
                "type": "method"
            },
            {
                "display_name": "5 Ingredients or Less",
                "type": "difficulty",
                "id": 64470,
                "name": "5_ingredients_or_less"
            },
            {
                "display_name": "Easy",
                "type": "difficulty",
                "id": 64471,
                "name": "easy"
            },
            {
                "type": "equipment",
                "id": 1247794,
                "name": "wooden_spoon",
                "display_name": "Wooden Spoon"
            },
            {
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty",
                "id": 64472
            },
            {
                "type": "equipment",
                "id": 1280506,
                "name": "liquid_measuring_cup",
                "display_name": "Liquid Measuring Cup"
            }
        ],
        "nutrition": {
            "sugar": 6,
            "fiber": 2,
            "updated_at": "2020-05-22T08:07:44+02:00",
            "calories": 259,
            "carbohydrates": 11,
            "fat": 19,
            "protein": 9
        },
        "beauty_url": null,
        "seo_title": "",
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/ef2a9700872d4c34b8d0704462709ec3/BFV66916_IMade3MealsWithOnly5Ingredients_MJC_FINAL_YT.mp4",
        "canonical_id": "recipe:6187",
        "promotion": "full",
        "brand_id": null,
        "cook_time_minutes": 15,
        "created_at": 1590006261,
        "is_shoppable": true,
        "name": "Eggs In Purgatory",
        "show_id": 17,
        "slug": "eggs-in-purgatory",
        "approved_at": 1590151379,
        "nutrition_visibility": "auto",
        "compilations": [
            {
                "video_id": 104177,
                "facebook_posts": [],
                "aspect_ratio": "16:9",
                "beauty_url": null,
                "buzz_id": null,
                "draft_status": "published",
                "name": "I Made 3 Meals With Only 5 Ingredients",
                "promotion": "full",
                "id": 1485,
                "canonical_id": "compilation:1485",
                "description": null,
                "language": "eng",
                "slug": "i-made-3-meals-with-only-5-ingredients",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/266618.jpg",
                "show": [
                    {
                        "name": "Tasty",
                        "id": 17
                    }
                ],
                "approved_at": 1590151400,
                "country": "US",
                "created_at": 1590006554,
                "is_shoppable": true,
                "keywords": null,
                "video_url": "https://vid.tasty.co/output/167901/hls24_1590007174.m3u8"
            }
        ],
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "instructions": [
            {
                "id": 55440,
                "display_text": "Heat the olive oil in a small skillet over medium heat. Add the onion and cook until softened, about 5 minutes.",
                "position": 1,
                "start_time": 49333,
                "end_time": 55000,
                "temperature": null,
                "appliance": null
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55441,
                "display_text": "Add the water and tomatoes and season with salt and pepper. Cover and cook until the tomatoes start to burst but still hold some of their shape, 5–7 minutes.",
                "position": 2,
                "start_time": 56000,
                "end_time": 81666
            },
            {
                "display_text": "Using the back of a spoon, create 2 wells in the tomato mixture. Crack an egg into each well. Season with salt and pepper.",
                "position": 3,
                "start_time": 100000,
                "end_time": 114500,
                "temperature": null,
                "appliance": null,
                "id": 55442
            },
            {
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55443,
                "display_text": "Cook uncovered for 5–6 minutes, until the egg whites are set but the yolks are still a bit runny.",
                "position": 4
            },
            {
                "start_time": 122666,
                "end_time": 126500,
                "temperature": null,
                "appliance": null,
                "id": 55444,
                "display_text": "Serve immediately.",
                "position": 5
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55445,
                "display_text": "Enjoy!",
                "position": 6,
                "start_time": 127000,
                "end_time": 136166
            }
        ],
        "user_ratings": {
            "score": 0.686567,
            "count_positive": 46,
            "count_negative": 21
        },
        "credits": [
            {
                "name": "Matt Ciampa",
                "type": "internal"
            }
        ],
        "renditions": [
            {
                "height": 404,
                "file_size": 68199607,
                "bit_rate": 898,
                "maximum_bit_rate": null,
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/square_720/1590007174_00001.png",
                "name": "mp4_720x404",
                "width": 720,
                "duration": 607945,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167901/square_720/1590007174"
            },
            {
                "width": 320,
                "duration": 607945,
                "bit_rate": 370,
                "minimum_bit_rate": null,
                "url": "https://vid.tasty.co/output/167901/square_320/1590007174",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/square_320/1590007174_00001.png",
                "name": "mp4_320x180",
                "height": 180,
                "file_size": 28103404,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "container": "mp4"
            },
            {
                "height": 720,
                "minimum_bit_rate": null,
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167901/landscape_720/1590007174",
                "width": 1280,
                "duration": 607945,
                "file_size": 154384786,
                "bit_rate": 2032,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/landscape_720/1590007174_00001.png",
                "name": "mp4_1280x720"
            },
            {
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "landscape",
                "container": "mp4",
                "name": "mp4_640x360",
                "file_size": 58572041,
                "bit_rate": 771,
                "duration": 607945,
                "maximum_bit_rate": null,
                "url": "https://vid.tasty.co/output/167901/landscape_480/1590007174",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/landscape_480/1590007174_00001.png",
                "height": 360,
                "width": 640
            },
            {
                "bit_rate": null,
                "content_type": "application/vnd.apple.mpegurl",
                "url": "https://vid.tasty.co/output/167901/hls24_1590007174.m3u8",
                "name": "low",
                "height": 1080,
                "width": 1920,
                "duration": 607941,
                "aspect": "landscape",
                "container": "ts",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167901/1445289064805-h2exzu/1590007174_00001.png",
                "file_size": null,
                "maximum_bit_rate": 4093,
                "minimum_bit_rate": 271
            }
        ],
        "total_time_minutes": 20,
        "video_ad_content": "none",
        "facebook_posts": [],
        "description": "A simple yet fancy breakfast that comes together with 5 ingredients and in under 20 minutes. If you can’t find cherry tomatoes, you can substitute grape tomatoes or even tomatoes on the vine–just dice them up!",
        "prep_time_minutes": 5,
        "updated_at": 1590151380,
        "video_id": 104177,
        "video_url": "https://vid.tasty.co/output/167901/hls24_1590007174.m3u8",
        "id": 6187,
        "draft_status": "published",
        "inspired_by_url": null,
        "language": "eng",
        "servings_noun_singular": "serving",
        "sections": [
            {
                "components": [
                    {
                        "id": 67988,
                        "raw_text": "2 tablespoons olive oil",
                        "extra_comment": "",
                        "position": 1,
                        "measurements": [
                            {
                                "id": 566365,
                                "quantity": "2",
                                "unit": {
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 4,
                            "name": "olive oil",
                            "display_singular": "olive oil",
                            "display_plural": "olive oils",
                            "created_at": 1493306183,
                            "updated_at": 1509035290
                        }
                    },
                    {
                        "ingredient": {
                            "id": 942,
                            "name": "medium yellow onion",
                            "display_singular": "medium yellow onion",
                            "display_plural": "medium yellow onions",
                            "created_at": 1496102165,
                            "updated_at": 1509035220
                        },
                        "id": 67989,
                        "raw_text": "½ medium yellow onion, diced",
                        "extra_comment": "diced",
                        "position": 2,
                        "measurements": [
                            {
                                "id": 566362,
                                "quantity": "½",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                }
                            }
                        ]
                    },
                    {
                        "id": 67990,
                        "raw_text": "⅓ cup water",
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 566370,
                                "quantity": "80",
                                "unit": {
                                    "name": "milliliter",
                                    "abbreviation": "mL",
                                    "display_singular": "mL",
                                    "display_plural": "mL",
                                    "system": "metric"
                                }
                            },
                            {
                                "id": 566369,
                                "quantity": "⅓",
                                "unit": {
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup",
                                    "abbreviation": "c"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "water",
                            "display_plural": "waters",
                            "created_at": 1494124627,
                            "updated_at": 1509035280,
                            "id": 197,
                            "name": "water"
                        }
                    },
                    {
                        "raw_text": "10 ounces cherry tomatoes",
                        "extra_comment": "",
                        "position": 4,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                },
                                "id": 566364,
                                "quantity": "285"
                            },
                            {
                                "id": 566363,
                                "quantity": "10",
                                "unit": {
                                    "display_singular": "oz",
                                    "display_plural": "oz",
                                    "system": "imperial",
                                    "name": "ounce",
                                    "abbreviation": "oz"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "cherry tomatoes",
                            "created_at": 1494623731,
                            "updated_at": 1509035275,
                            "id": 264,
                            "name": "cherry tomatoes",
                            "display_singular": "cherry tomato"
                        },
                        "id": 67991
                    },
                    {
                        "id": 67992,
                        "raw_text": "Kosher salt, to taste",
                        "extra_comment": "to taste",
                        "position": 5,
                        "measurements": [
                            {
                                "id": 566367,
                                "quantity": "0",
                                "unit": {
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "kosher salts",
                            "created_at": 1493307153,
                            "updated_at": 1509035289,
                            "id": 11,
                            "name": "kosher salt",
                            "display_singular": "kosher salt"
                        }
                    },
                    {
                        "position": 6,
                        "measurements": [
                            {
                                "id": 566368,
                                "quantity": "0",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "freshly ground black pepper",
                            "display_plural": "freshly ground black peppers",
                            "created_at": 1493925438,
                            "updated_at": 1509035282,
                            "id": 166,
                            "name": "freshly ground black pepper"
                        },
                        "id": 67993,
                        "raw_text": "Freshly ground black pepper, to taste",
                        "extra_comment": "to taste"
                    },
                    {
                        "position": 7,
                        "measurements": [
                            {
                                "quantity": "2",
                                "unit": {
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": ""
                                },
                                "id": 566366
                            }
                        ],
                        "ingredient": {
                            "display_plural": "large eggs",
                            "created_at": 1494382414,
                            "updated_at": 1509035275,
                            "id": 253,
                            "name": "large egg",
                            "display_singular": "large egg"
                        },
                        "id": 67994,
                        "raw_text": "2 large eggs",
                        "extra_comment": ""
                    }
                ],
                "name": null,
                "position": 1
            }
        ],
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "aspect_ratio": "16:9"
    },
    {
        "buzz_id": null,
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/265808.jpg",
        "video_url": "https://vid.tasty.co/output/167434/hls24_1589548556.m3u8",
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "instructions": [
            {
                "end_time": 37966,
                "temperature": null,
                "appliance": null,
                "id": 55372,
                "display_text": "In a bowl, whisk together eggs, whole milk, and Oikos Greek Yogurt Key Lime. Add in sugar, baking powder, kosher salt, and flour. Whisk until all the dry ingredients are incorporated.",
                "position": 1,
                "start_time": 10333
            },
            {
                "position": 2,
                "start_time": 38000,
                "end_time": 47616,
                "temperature": null,
                "appliance": null,
                "id": 55373,
                "display_text": "Coat waffle iron with nonstick spray. Heat up iron. Pour a heaping ½ cup of batter in the center of the iron. Close and cook for 3–4 minutes, or until golden brown and cooked through. Repeat with the rest of the batter."
            },
            {
                "start_time": 48000,
                "end_time": 63666,
                "temperature": null,
                "appliance": null,
                "id": 55374,
                "display_text": "In a small bowl, whisk together Oikos Greek Yogurt Key Lime, lime zest, and maple syrup. Top waffles with a dollop and garnish with a lime slice.",
                "position": 3
            },
            {
                "position": 4,
                "start_time": 64000,
                "end_time": 72000,
                "temperature": null,
                "appliance": null,
                "id": 55390,
                "display_text": "Serve and enjoy! "
            }
        ],
        "sections": [
            {
                "components": [
                    {
                        "ingredient": {
                            "id": 19,
                            "name": "egg",
                            "display_singular": "egg",
                            "display_plural": "eggs",
                            "created_at": 1493314622,
                            "updated_at": 1509035288
                        },
                        "id": 67909,
                        "raw_text": "2 eggs",
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "id": 565797,
                                "quantity": "2",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                }
                            }
                        ]
                    },
                    {
                        "id": 67910,
                        "raw_text": "1⅓ cups whole milk",
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "milliliter",
                                    "abbreviation": "mL",
                                    "display_singular": "mL",
                                    "display_plural": "mL",
                                    "system": "metric"
                                },
                                "id": 565813,
                                "quantity": "320"
                            },
                            {
                                "id": 565808,
                                "quantity": "1 ⅓",
                                "unit": {
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup",
                                    "abbreviation": "c"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "whole milk",
                            "display_plural": "whole milks",
                            "created_at": 1495732941,
                            "updated_at": 1509035235,
                            "id": 770,
                            "name": "whole milk"
                        }
                    },
                    {
                        "raw_text": "1 (5.3-ounce) container Oikos Greek Yogurt Key Lime",
                        "extra_comment": "(5.3-ounce)",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 565805,
                                "quantity": "1",
                                "unit": {
                                    "name": "container",
                                    "abbreviation": "container",
                                    "display_singular": "container",
                                    "display_plural": "containers",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 6430,
                            "name": "Oikos Greek Yogurt Key Lime",
                            "display_singular": "Oikos Greek Yogurt Key Lime",
                            "display_plural": "Oikos Greek Yogurt Key Limes",
                            "created_at": 1589839347,
                            "updated_at": 1589839347
                        },
                        "id": 67911
                    },
                    {
                        "position": 5,
                        "measurements": [
                            {
                                "id": 565809,
                                "quantity": "15",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            },
                            {
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                },
                                "id": 565802
                            }
                        ],
                        "ingredient": {
                            "id": 24,
                            "name": "sugar",
                            "display_singular": "sugar",
                            "display_plural": "sugars",
                            "created_at": 1493314650,
                            "updated_at": 1509035288
                        },
                        "id": 67912,
                        "raw_text": "1 tablespoon sugar",
                        "extra_comment": ""
                    },
                    {
                        "id": 67913,
                        "raw_text": "1 tablespoon baking powder",
                        "extra_comment": "",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 565804,
                                "quantity": "12",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            },
                            {
                                "id": 565798,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035288,
                            "id": 23,
                            "name": "baking powder",
                            "display_singular": "baking powder",
                            "display_plural": "baking powders",
                            "created_at": 1493314647
                        }
                    },
                    {
                        "ingredient": {
                            "id": 11,
                            "name": "kosher salt",
                            "display_singular": "kosher salt",
                            "display_plural": "kosher salts",
                            "created_at": 1493307153,
                            "updated_at": 1509035289
                        },
                        "id": 67914,
                        "raw_text": "½ teaspoon kosher salt",
                        "extra_comment": "",
                        "position": 7,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                },
                                "id": 565812,
                                "quantity": "2.5"
                            },
                            {
                                "id": 565807,
                                "quantity": "½",
                                "unit": {
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons",
                                    "system": "imperial"
                                }
                            }
                        ]
                    },
                    {
                        "ingredient": {
                            "updated_at": 1509035288,
                            "id": 25,
                            "name": "flour",
                            "display_singular": "flour",
                            "display_plural": "flours",
                            "created_at": 1493314654
                        },
                        "id": 67915,
                        "raw_text": "2 cups flour",
                        "extra_comment": "",
                        "position": 8,
                        "measurements": [
                            {
                                "id": 565811,
                                "quantity": "250",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            },
                            {
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                },
                                "id": 565800,
                                "quantity": "2"
                            }
                        ]
                    }
                ],
                "name": "Waffles",
                "position": 1
            },
            {
                "components": [
                    {
                        "id": 67917,
                        "raw_text": "1 (5.3-ounce) container Oikos Greek Yogurt Key Lime",
                        "extra_comment": "(5.3-ounce)",
                        "position": 10,
                        "measurements": [
                            {
                                "id": 565801,
                                "quantity": "1",
                                "unit": {
                                    "name": "container",
                                    "abbreviation": "container",
                                    "display_singular": "container",
                                    "display_plural": "containers",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 6430,
                            "name": "Oikos Greek Yogurt Key Lime",
                            "display_singular": "Oikos Greek Yogurt Key Lime",
                            "display_plural": "Oikos Greek Yogurt Key Limes",
                            "created_at": 1589839347,
                            "updated_at": 1589839347
                        }
                    },
                    {
                        "position": 11,
                        "measurements": [
                            {
                                "id": 565810,
                                "quantity": "2",
                                "unit": {
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g"
                                }
                            },
                            {
                                "id": 565806,
                                "quantity": "2",
                                "unit": {
                                    "system": "imperial",
                                    "name": "teaspoon",
                                    "abbreviation": "tsp",
                                    "display_singular": "teaspoon",
                                    "display_plural": "teaspoons"
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035247,
                            "id": 611,
                            "name": "lime zest",
                            "display_singular": "lime zest",
                            "display_plural": "lime zests",
                            "created_at": 1495486264
                        },
                        "id": 67918,
                        "raw_text": "Zest of 1 lime",
                        "extra_comment": ""
                    },
                    {
                        "id": 67919,
                        "raw_text": "1 tablespoon maple syrup",
                        "extra_comment": "",
                        "position": 12,
                        "measurements": [
                            {
                                "id": 565803,
                                "quantity": "21",
                                "unit": {
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g"
                                }
                            },
                            {
                                "quantity": "1",
                                "unit": {
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp"
                                },
                                "id": 565799
                            }
                        ],
                        "ingredient": {
                            "id": 359,
                            "name": "maple syrup",
                            "display_singular": "maple syrup",
                            "display_plural": "maple syrups",
                            "created_at": 1494966352,
                            "updated_at": 1509035267
                        }
                    },
                    {
                        "id": 67920,
                        "raw_text": "Lime slices, for garnish",
                        "extra_comment": "for garnish",
                        "position": 13,
                        "measurements": [
                            {
                                "id": 565814,
                                "quantity": "0",
                                "unit": {
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1589839474,
                            "updated_at": 1589839474,
                            "id": 6431,
                            "name": "Lime slices",
                            "display_singular": "Lime slice",
                            "display_plural": "Lime slices"
                        }
                    }
                ],
                "name": "Topping",
                "position": 2
            }
        ],
        "tags": [
            {
                "name": "american",
                "display_name": "American",
                "type": "cuisine",
                "id": 64444
            },
            {
                "id": 64469,
                "name": "vegetarian",
                "display_name": "Vegetarian",
                "type": "dietary"
            },
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "type": "meal",
                "id": 64483,
                "name": "breakfast",
                "display_name": "Breakfast"
            },
            {
                "display_name": "Desserts",
                "type": "meal",
                "id": 64485,
                "name": "desserts"
            },
            {
                "id": 64484,
                "name": "brunch",
                "display_name": "Brunch",
                "type": "occasion"
            },
            {
                "id": 64462,
                "name": "comfort_food",
                "display_name": "Comfort Food",
                "type": "dietary"
            },
            {
                "id": 4202175,
                "name": "sponsored_recipe",
                "display_name": "Sponsored Recipe",
                "type": "business_tags"
            }
        ],
        "beauty_url": null,
        "video_ad_content": "co_branded",
        "nutrition_visibility": "auto",
        "renditions": [
            {
                "bit_rate": 1618,
                "minimum_bit_rate": null,
                "aspect": "square",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167434/square_720/1589548556",
                "width": 720,
                "duration": 77277,
                "file_size": 15621901,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167434/square_720/1589548556_00001.png",
                "name": "mp4_720x720",
                "height": 720
            },
            {
                "aspect": "square",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167434/square_320/1589548556_00001.png",
                "name": "mp4_320x320",
                "width": 320,
                "file_size": 5549112,
                "bit_rate": 575,
                "content_type": "video/mp4",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167434/square_320/1589548556",
                "height": 320,
                "duration": 77277,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null
            },
            {
                "file_size": 15620221,
                "bit_rate": 1618,
                "aspect": "square",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167434/landscape_720/1589548556_00001.png",
                "name": "mp4_720x720",
                "url": "https://vid.tasty.co/output/167434/landscape_720/1589548556",
                "height": 720,
                "width": 720,
                "duration": 77277,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4"
            },
            {
                "content_type": "video/mp4",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/167434/landscape_480/1589548556",
                "bit_rate": 934,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "file_size": 9016126,
                "aspect": "square",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167434/landscape_480/1589548556_00001.png",
                "name": "mp4_480x480",
                "height": 480,
                "width": 480,
                "duration": 77277
            },
            {
                "height": 1080,
                "container": "ts",
                "url": "https://vid.tasty.co/output/167434/hls24_1589548556.m3u8",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/167434/1445289064805-h2exzu/1589548556_00001.png",
                "name": "low",
                "content_type": "application/vnd.apple.mpegurl",
                "aspect": "square",
                "width": 1080,
                "duration": 77290,
                "file_size": null,
                "bit_rate": null,
                "maximum_bit_rate": 2907,
                "minimum_bit_rate": 273
            }
        ],
        "language": "eng",
        "inspired_by_url": null,
        "name": "Key Lime Waffles",
        "num_servings": 6,
        "promotion": "full",
        "seo_title": "",
        "brand_id": 6,
        "is_shoppable": true,
        "servings_noun_plural": "servings",
        "servings_noun_singular": "serving",
        "tips_and_ratings_enabled": true,
        "updated_at": 1589839976,
        "canonical_id": "recipe:6178",
        "facebook_posts": [],
        "draft_status": "published",
        "description": "",
        "total_time_minutes": null,
        "credits": [
            {
                "type": "brand",
                "id": 6,
                "name": "Dannon Oikos",
                "slug": "dannon-oikos",
                "image_url": "https://img.buzzfeed.com/tasty-app-user-assets-prod-us-east-1/brands/fa4cc61fc38f47889021c4e2dc826cfa.png"
            }
        ],
        "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/5eda213712e74fa5855651af3e9f1bd9/DANONExWALMART_Waffles_35180-262501_EV_051220_V6_Hero.mp4",
        "cook_time_minutes": null,
        "is_one_top": false,
        "brand": {
            "id": 6,
            "name": "Dannon Oikos",
            "slug": "dannon-oikos",
            "image_url": "https://img.buzzfeed.com/tasty-app-user-assets-prod-us-east-1/brands/fa4cc61fc38f47889021c4e2dc826cfa.png"
        },
        "user_ratings": {
            "score": 0.818919,
            "count_positive": 303,
            "count_negative": 67
        },
        "id": 6178,
        "aspect_ratio": "1:1",
        "country": "US",
        "created_at": 1589835854,
        "keywords": "",
        "prep_time_minutes": null,
        "slug": "key-lime-waffles",
        "video_id": 105599,
        "approved_at": 1589839975,
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "yields": "Servings 6",
        "compilations": [],
        "nutrition": {
            "sugar": 7,
            "fiber": 1,
            "updated_at": "2020-05-20T08:01:09+02:00",
            "calories": 256,
            "carbohydrates": 44,
            "fat": 3,
            "protein": 10
        },
        "show_id": 17
    },
    {
        "promotion": "full",
        "show_id": 17,
        "nutrition_visibility": "auto",
        "brand": null,
        "sections": [
            {
                "components": [
                    {
                        "measurements": [
                            {
                                "id": 565309,
                                "quantity": "245",
                                "unit": {
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g"
                                }
                            },
                            {
                                "id": 565308,
                                "quantity": "1",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035224,
                            "id": 898,
                            "name": "creamy peanut butter",
                            "display_singular": "creamy peanut butter",
                            "display_plural": "creamy peanut butters",
                            "created_at": 1496004371
                        },
                        "id": 67829,
                        "raw_text": "1 cup creamy peanut butter",
                        "extra_comment": "",
                        "position": 2
                    },
                    {
                        "measurements": [
                            {
                                "id": 565313,
                                "quantity": "224",
                                "unit": {
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g"
                                }
                            },
                            {
                                "id": 565312,
                                "quantity": "⅔",
                                "unit": {
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1494966352,
                            "updated_at": 1509035267,
                            "id": 359,
                            "name": "maple syrup",
                            "display_singular": "maple syrup",
                            "display_plural": "maple syrups"
                        },
                        "id": 67830,
                        "raw_text": "⅔ cup maple syrup",
                        "extra_comment": "",
                        "position": 3
                    },
                    {
                        "extra_comment": "",
                        "position": 4,
                        "measurements": [
                            {
                                "id": 565315,
                                "quantity": "125",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            },
                            {
                                "id": 565314,
                                "quantity": "1",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1513187920,
                            "id": 3393,
                            "name": "all purpose flour",
                            "display_singular": "all purpose flour",
                            "display_plural": "all purpose flours",
                            "created_at": 1513187920
                        },
                        "id": 67831,
                        "raw_text": "1 cup all-purpose flour"
                    }
                ],
                "name": "Cookies",
                "position": 1
            },
            {
                "components": [
                    {
                        "id": 67833,
                        "raw_text": "½ cup creamy peanut butter",
                        "extra_comment": "",
                        "position": 6,
                        "measurements": [
                            {
                                "id": 565311,
                                "quantity": "120",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            },
                            {
                                "id": 565310,
                                "quantity": "½",
                                "unit": {
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup"
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035224,
                            "id": 898,
                            "name": "creamy peanut butter",
                            "display_singular": "creamy peanut butter",
                            "display_plural": "creamy peanut butters",
                            "created_at": 1496004371
                        }
                    },
                    {
                        "id": 67834,
                        "raw_text": "2 tablespoons unsalted butter, softened",
                        "extra_comment": "softened",
                        "position": 7,
                        "measurements": [
                            {
                                "quantity": "2",
                                "unit": {
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon"
                                },
                                "id": 565305
                            }
                        ],
                        "ingredient": {
                            "created_at": 1494806355,
                            "updated_at": 1509035272,
                            "id": 291,
                            "name": "unsalted butter",
                            "display_singular": "unsalted butter",
                            "display_plural": "unsalted butters"
                        }
                    },
                    {
                        "ingredient": {
                            "name": "powdered sugar",
                            "display_singular": "powdered sugar",
                            "display_plural": "powdered sugars",
                            "created_at": 1493747135,
                            "updated_at": 1509035283,
                            "id": 144
                        },
                        "id": 67835,
                        "raw_text": "1 cup powdered sugar",
                        "extra_comment": "",
                        "position": 8,
                        "measurements": [
                            {
                                "id": 565307,
                                "quantity": "110",
                                "unit": {
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g"
                                }
                            },
                            {
                                "id": 565306,
                                "quantity": "1",
                                "unit": {
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup"
                                }
                            }
                        ]
                    },
                    {
                        "id": 67836,
                        "raw_text": "Water, as needed",
                        "extra_comment": "as needed",
                        "position": 9,
                        "measurements": [
                            {
                                "id": 565304,
                                "quantity": "0",
                                "unit": {
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035280,
                            "id": 197,
                            "name": "water",
                            "display_singular": "water",
                            "display_plural": "waters",
                            "created_at": 1494124627
                        }
                    }
                ],
                "name": "Peanut Butter Frosting",
                "position": 2
            }
        ],
        "description": "Your favorite childhood cookie just got a 5-ingredient makeover with silky smooth peanut butter frosting sandwiched between crunchy peanut butter cookies. The best part? They come together in less than 30 minutes. This recipe also works great with almond butter.",
        "is_one_top": false,
        "language": "eng",
        "original_video_url": null,
        "nutrition": {
            "updated_at": "2020-05-16T08:01:03+02:00",
            "calories": 438,
            "carbohydrates": 50,
            "fat": 23,
            "protein": 10,
            "sugar": 30,
            "fiber": 2
        },
        "instructions": [
            {
                "id": 55336,
                "display_text": "Preheat the oven to 350°F (180°C). Line a baking sheet with parchment paper (or use a nonstick pan).",
                "position": 1,
                "start_time": 0,
                "end_time": 0,
                "temperature": 350,
                "appliance": "oven"
            },
            {
                "id": 55337,
                "display_text": "Make the cookies: In a large bowl, beat together the peanut butter, maple syrup, and flour with an electric hand mixer until smooth.",
                "position": 2,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "temperature": null,
                "appliance": null,
                "id": 55338,
                "display_text": "Roll the dough into ½-inch (1 ¼ cm) balls. Arrange the balls on the prepared baking sheet, placing 2 balls side-by-side for each cookie. Use a fork to create a criss-cross pattern on top of the cookies to create peanut shapes.",
                "position": 3,
                "start_time": 0,
                "end_time": 0
            },
            {
                "id": 55339,
                "display_text": "Bake the cookies for 12 minutes, or until edges are starting to turn golden brown. Let cool completely.",
                "position": 4,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "id": 55340,
                "display_text": "Meanwhile, make the frosting: In a large bowl, whip the peanut butter and butter with an electric hand mixer until combined. Add the powdered sugar and beat until combined. Add a bit of water as needed to achieve a smooth consistency.",
                "position": 5,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "appliance": null,
                "id": 55341,
                "display_text": "Spread a little bit of the frosting on the underside of 1 of the cookie, then sandwich with another cookie. Repeat with the remaining cookies and frosting.",
                "position": 6,
                "start_time": 0,
                "end_time": 0,
                "temperature": null
            },
            {
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55342,
                "display_text": "Enjoy!",
                "position": 7
            }
        ],
        "user_ratings": {
            "count_positive": 0,
            "count_negative": 0,
            "score": null
        },
        "cook_time_minutes": 12,
        "yields": "Makes 10-12 cookies",
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "compilations": [],
        "draft_status": "published",
        "keywords": "",
        "seo_title": "",
        "prep_time_minutes": 5,
        "servings_noun_plural": "cookies",
        "updated_at": 1589510451,
        "tags": [
            {
                "type": "dietary",
                "id": 64488,
                "name": "kid_friendly",
                "display_name": "Kid-Friendly"
            },
            {
                "type": "appliance",
                "id": 65844,
                "name": "hand_mixer",
                "display_name": "Hand Mixer"
            },
            {
                "id": 1280500,
                "name": "baking_pan",
                "display_name": "Baking Pan",
                "type": "equipment"
            },
            {
                "display_name": "Bakery Goods",
                "type": "meal",
                "id": 65857,
                "name": "bakery_goods"
            },
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "id": 65846,
                "name": "oven",
                "display_name": "Oven",
                "type": "appliance"
            },
            {
                "name": "spatula",
                "display_name": "Spatula",
                "type": "equipment",
                "id": 1247788
            },
            {
                "id": 1280511,
                "name": "offset_spatula",
                "display_name": "Offset Spatula",
                "type": "equipment"
            },
            {
                "type": "meal",
                "id": 64491,
                "name": "snacks",
                "display_name": "Snacks"
            },
            {
                "type": "method",
                "id": 64492,
                "name": "bake",
                "display_name": "Bake"
            },
            {
                "name": "parchment_paper",
                "display_name": "Parchment Paper",
                "type": "equipment",
                "id": 1247780
            },
            {
                "id": 64470,
                "name": "5_ingredients_or_less",
                "display_name": "5 Ingredients or Less",
                "type": "difficulty"
            },
            {
                "id": 1247775,
                "name": "oven_mitts",
                "display_name": "Oven Mitts",
                "type": "equipment"
            },
            {
                "name": "mixing_bowl",
                "display_name": "Mixing Bowl",
                "type": "equipment",
                "id": 1280510
            },
            {
                "display_name": "Dry Measuring Cups",
                "type": "equipment",
                "id": 1280507,
                "name": "dry_measuring_cups"
            }
        ],
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "approved_at": 1589510552,
        "beauty_url": null,
        "brand_id": null,
        "renditions": [],
        "credits": [
            {
                "name": "Kelly Paige",
                "type": "internal"
            }
        ],
        "created_at": 1589479639,
        "id": 6173,
        "inspired_by_url": null,
        "num_servings": 10,
        "slug": "5-ingredient-copycat-nutter-butter-cookies",
        "thumbnail_url": "https://img.buzzfeed.com/tasty-app-user-assets-prod-us-east-1/recipes/d4bc84833241436f85cde6fef79be83b.jpeg",
        "video_id": null,
        "canonical_id": "recipe:6173",
        "buzz_id": null,
        "is_shoppable": true,
        "name": "5-Ingredient Copycat Nutter Butter Cookies",
        "facebook_posts": [],
        "country": "US",
        "servings_noun_singular": "cookie",
        "video_url": null,
        "video_ad_content": null,
        "aspect_ratio": "16:9",
        "tips_and_ratings_enabled": true,
        "total_time_minutes": 17
    },
    {
        "facebook_posts": [],
        "created_at": 1584513567,
        "show_id": 34,
        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/258777.jpg",
        "video_id": 102112,
        "recipes": [
            {
                "video_url": "https://vid.tasty.co/output/69168/hls24_1513721193.m3u8",
                "user_ratings": {
                    "count_positive": 130,
                    "count_negative": 3,
                    "score": 0.977444
                },
                "canonical_id": "recipe:3228",
                "aspect_ratio": "1:1",
                "prep_time_minutes": null,
                "slug": "beet-and-pear-salad-with-lemon-vinaigrette",
                "inspired_by_url": null,
                "total_time_minutes": null,
                "yields": "Servings: 6",
                "total_time_tier": {
                    "tier": "under_30_minutes",
                    "display_tier": "Under 30 minutes"
                },
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/82b4599333444db7b66b5aed0a730d80/BFV34166_PearAndBeetSalad-FB.mp4",
                "approved_at": 1515426669,
                "buzz_id": 4719818,
                "description": null,
                "_id": 3228,
                "brand": null,
                "instructions": [
                    {
                        "id": 27225,
                        "display_text": "In a non-stick skillet, toast the walnuts over medium-high heat for 5 minutes, stirring occasionally. Remove from heat and set aside.",
                        "position": 1,
                        "start_time": 14000,
                        "end_time": 18166,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "appliance": null,
                        "id": 27226,
                        "display_text": "In a small bowl, mix the lemon juice, water, honey, thyme, salt, and pepper.",
                        "position": 2,
                        "start_time": 18666,
                        "end_time": 30533,
                        "temperature": null
                    },
                    {
                        "id": 27227,
                        "display_text": "While whisking, slowly add the oil to the bowl to create a vinaigrette.",
                        "position": 3,
                        "start_time": 30866,
                        "end_time": 38733,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "appliance": null,
                        "id": 27228,
                        "display_text": "In a large salad bowl, add the mixed greens, pears, beets, onions, walnuts, and lemon vinaigrette. Toss to combine all ingredients and serve.",
                        "position": 4,
                        "start_time": 39566,
                        "end_time": 57266,
                        "temperature": null
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 27229,
                        "display_text": "Enjoy!",
                        "position": 5,
                        "start_time": 57433,
                        "end_time": 59933
                    }
                ],
                "credits": [
                    {
                        "name": "Joey Firoben",
                        "type": "internal"
                    }
                ],
                "video_ad_content": "none",
                "_op_type": "index",
                "is_one_top": false,
                "promotion": "full",
                "updated_at": 1560182366,
                "nutrition_visibility": "auto",
                "sections": [
                    {
                        "components": [
                            {
                                "ingredient": {
                                    "id": 346,
                                    "name": "walnuts",
                                    "display_singular": "walnut",
                                    "display_plural": "walnuts",
                                    "created_at": 1494884855,
                                    "updated_at": 1509035268
                                },
                                "id": 30927,
                                "raw_text": "½ cup walnuts, roughly chopped",
                                "extra_comment": "roughly chopped",
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 396674,
                                        "quantity": "½",
                                        "unit": {
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "unit": {
                                            "display_plural": "g",
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g"
                                        },
                                        "id": 396671,
                                        "quantity": "50"
                                    }
                                ]
                            },
                            {
                                "id": 30928,
                                "raw_text": "1 pear, thinly sliced",
                                "extra_comment": "thinly sliced",
                                "position": 2,
                                "measurements": [
                                    {
                                        "quantity": "1",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        },
                                        "id": 396676
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "pears",
                                    "created_at": 1496147472,
                                    "updated_at": 1509035220,
                                    "id": 946,
                                    "name": "pear",
                                    "display_singular": "pear"
                                }
                            },
                            {
                                "raw_text": "5 ounces mixed greens",
                                "extra_comment": "",
                                "position": 3,
                                "measurements": [
                                    {
                                        "id": 396670,
                                        "quantity": "5",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "ounce",
                                            "abbreviation": "oz",
                                            "display_singular": "oz",
                                            "display_plural": "oz"
                                        }
                                    },
                                    {
                                        "id": 396668,
                                        "quantity": "140",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "mixed greens",
                                    "display_singular": "mixed green",
                                    "display_plural": "mixed greens",
                                    "created_at": 1494623707,
                                    "updated_at": 1509035275,
                                    "id": 263
                                },
                                "id": 30929
                            },
                            {
                                "position": 4,
                                "measurements": [
                                    {
                                        "id": 396678,
                                        "quantity": "3",
                                        "unit": {
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1515425930,
                                    "id": 3475,
                                    "name": "beet",
                                    "display_singular": "beet",
                                    "display_plural": "beets",
                                    "created_at": 1515425930
                                },
                                "id": 30930,
                                "raw_text": "3 beets, cooked, halved, and thinly sliced",
                                "extra_comment": "cooked, halved, and thinly sliced"
                            },
                            {
                                "position": 5,
                                "measurements": [
                                    {
                                        "id": 396666,
                                        "quantity": "¼",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "red onions",
                                    "created_at": 1493307196,
                                    "updated_at": 1509035289,
                                    "id": 13,
                                    "name": "red onion",
                                    "display_singular": "red onion"
                                },
                                "id": 30931,
                                "raw_text": "¼ red onion, thinly sliced",
                                "extra_comment": "thinly sliced"
                            }
                        ],
                        "name": null,
                        "position": 1
                    },
                    {
                        "position": 2,
                        "components": [
                            {
                                "measurements": [
                                    {
                                        "id": 396679,
                                        "quantity": "1",
                                        "unit": {
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "lemon",
                                    "display_plural": "lemons",
                                    "created_at": 1493906426,
                                    "updated_at": 1509035282,
                                    "id": 155,
                                    "name": "lemon"
                                },
                                "id": 30933,
                                "raw_text": "Juice of 1 lemon",
                                "extra_comment": "juiced",
                                "position": 7
                            },
                            {
                                "raw_text": "1 tablespoon water",
                                "extra_comment": "",
                                "position": 8,
                                "measurements": [
                                    {
                                        "id": 396669,
                                        "quantity": "1",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 197,
                                    "name": "water",
                                    "display_singular": "water",
                                    "display_plural": "waters",
                                    "created_at": 1494124627,
                                    "updated_at": 1509035280
                                },
                                "id": 30934
                            },
                            {
                                "extra_comment": "",
                                "position": 9,
                                "measurements": [
                                    {
                                        "unit": {
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon"
                                        },
                                        "id": 396672,
                                        "quantity": "1"
                                    }
                                ],
                                "ingredient": {
                                    "id": 52,
                                    "name": "honey",
                                    "display_singular": "honey",
                                    "display_plural": "honeys",
                                    "created_at": 1493430363,
                                    "updated_at": 1509035286
                                },
                                "id": 30935,
                                "raw_text": "1 tablespoon honey"
                            },
                            {
                                "id": 30936,
                                "raw_text": "1 teaspoon thyme",
                                "extra_comment": "",
                                "position": 10,
                                "measurements": [
                                    {
                                        "id": 396675,
                                        "quantity": "1",
                                        "unit": {
                                            "abbreviation": "tsp",
                                            "display_singular": "teaspoon",
                                            "display_plural": "teaspoons",
                                            "system": "imperial",
                                            "name": "teaspoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "fresh thyme",
                                    "display_singular": "fresh thyme",
                                    "display_plural": "fresh thymes",
                                    "created_at": 1495134646,
                                    "updated_at": 1509035257,
                                    "id": 477
                                }
                            },
                            {
                                "position": 11,
                                "measurements": [
                                    {
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        },
                                        "id": 396677,
                                        "quantity": "0"
                                    }
                                ],
                                "ingredient": {
                                    "id": 22,
                                    "name": "salt",
                                    "display_singular": "salt",
                                    "display_plural": "salts",
                                    "created_at": 1493314644,
                                    "updated_at": 1509035288
                                },
                                "id": 30937,
                                "raw_text": "Salt, to taste",
                                "extra_comment": "to taste"
                            },
                            {
                                "raw_text": "Pepper, to taste",
                                "extra_comment": "to taste",
                                "position": 12,
                                "measurements": [
                                    {
                                        "id": 396673,
                                        "quantity": "0",
                                        "unit": {
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "pepper",
                                    "display_plural": "peppers",
                                    "created_at": 1493314935,
                                    "updated_at": 1509035287,
                                    "id": 29,
                                    "name": "pepper"
                                },
                                "id": 30938
                            },
                            {
                                "extra_comment": "",
                                "position": 13,
                                "measurements": [
                                    {
                                        "id": 396667,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 100,
                                    "name": "oil",
                                    "display_singular": "oil",
                                    "display_plural": "oils",
                                    "created_at": 1493745145,
                                    "updated_at": 1509035284
                                },
                                "id": 32350,
                                "raw_text": "n/a"
                            }
                        ],
                        "name": "Lemon Vinaigrette"
                    }
                ],
                "_index": "recipes-20200519201145",
                "brand_id": null,
                "created_at": 1513719841,
                "id": 3228,
                "is_shoppable": true,
                "seo_title": null,
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/cba0607359a34ed38c9aef290b433171/BFV34166_PearAndBeetSalad-FB.jpg",
                "tips_and_ratings_enabled": true,
                "facebook_posts": [],
                "cook_time_minutes": null,
                "country": "US",
                "draft_status": "published",
                "renditions": [
                    {
                        "duration": 63748,
                        "file_size": 19971631,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "url": "https://vid.tasty.co/output/69168/square_720/1513721193",
                        "name": "mp4_720x720",
                        "height": 720,
                        "width": 720,
                        "aspect": "square",
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/69168/square_720/1513721193_00001.png",
                        "bit_rate": 2507,
                        "minimum_bit_rate": null
                    },
                    {
                        "file_size": 6486037,
                        "maximum_bit_rate": null,
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/69168/square_320/1513721193_00001.png",
                        "name": "mp4_320x320",
                        "height": 320,
                        "width": 320,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "square",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/69168/square_320/1513721193",
                        "duration": 63748,
                        "bit_rate": 814
                    },
                    {
                        "container": "mp4",
                        "height": 720,
                        "width": 720,
                        "duration": 63748,
                        "file_size": 19972995,
                        "bit_rate": 2507,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "name": "mp4_720x720",
                        "maximum_bit_rate": null,
                        "aspect": "square",
                        "url": "https://vid.tasty.co/output/69168/landscape_720/1513721193",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/69168/landscape_720/1513721193_00001.png"
                    },
                    {
                        "file_size": 11205706,
                        "bit_rate": 1407,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "square",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/69168/landscape_480/1513721193",
                        "height": 480,
                        "duration": 63748,
                        "minimum_bit_rate": null,
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/69168/landscape_480/1513721193_00001.png",
                        "name": "mp4_480x480",
                        "width": 480
                    },
                    {
                        "minimum_bit_rate": 278,
                        "content_type": "application/vnd.apple.mpegurl",
                        "aspect": "square",
                        "url": "https://vid.tasty.co/output/69168/hls24_1513721193.m3u8",
                        "height": 1080,
                        "width": 1080,
                        "bit_rate": null,
                        "maximum_bit_rate": 4479,
                        "container": "ts",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/69168/1445289064805-h2exzu/1513721193_00001.png",
                        "name": "low",
                        "duration": 63689,
                        "file_size": null
                    }
                ],
                "nutrition": {
                    "fiber": 2,
                    "updated_at": "2019-05-29T16:00:00+02:00",
                    "calories": 135,
                    "carbohydrates": 11,
                    "fat": 9,
                    "protein": 2,
                    "sugar": 8
                },
                "beauty_url": "https://img.buzzfeed.com/video-api-prod/assets/faf9e3e6f4cd42a9a0bece8d98e82b8f/BFV34166_PearAndBeetSaladBeauty.jpg",
                "num_servings": 6,
                "video_id": 39574,
                "servings_noun_singular": "serving",
                "_type": "recipe",
                "keywords": null,
                "language": "eng",
                "servings_noun_plural": "servings",
                "tags": [
                    {
                        "id": 64463,
                        "name": "dairy_free",
                        "display_name": "Dairy-Free",
                        "type": "dietary"
                    },
                    {
                        "id": 64469,
                        "name": "vegetarian",
                        "display_name": "Vegetarian",
                        "type": "dietary"
                    },
                    {
                        "id": 64466,
                        "name": "healthy",
                        "display_name": "Healthy",
                        "type": "dietary"
                    },
                    {
                        "display_name": "Gluten-Free",
                        "type": "dietary",
                        "id": 64465,
                        "name": "gluten_free"
                    },
                    {
                        "type": "dietary",
                        "id": 64468,
                        "name": "vegan",
                        "display_name": "Vegan"
                    },
                    {
                        "type": "equipment",
                        "id": 1280503,
                        "name": "cutting_board",
                        "display_name": "Cutting Board"
                    },
                    {
                        "id": 1247787,
                        "name": "saute_pan",
                        "display_name": "Saute Pan",
                        "type": "equipment"
                    },
                    {
                        "name": "mixing_bowl",
                        "display_name": "Mixing Bowl",
                        "type": "equipment",
                        "id": 1280510
                    },
                    {
                        "id": 1280508,
                        "name": "measuring_spoons",
                        "display_name": "Measuring Spoons",
                        "type": "equipment"
                    },
                    {
                        "id": 1247785,
                        "name": "pyrex",
                        "display_name": "Pyrex",
                        "type": "equipment"
                    },
                    {
                        "name": "dry_measuring_cups",
                        "display_name": "Dry Measuring Cups",
                        "type": "equipment",
                        "id": 1280507
                    },
                    {
                        "id": 1280501,
                        "name": "chefs_knife",
                        "display_name": "Chef's Knife",
                        "type": "equipment"
                    },
                    {
                        "id": 1247793,
                        "name": "whisk",
                        "display_name": "Whisk",
                        "type": "equipment"
                    },
                    {
                        "id": 64471,
                        "name": "easy",
                        "display_name": "Easy",
                        "type": "difficulty"
                    },
                    {
                        "id": 65848,
                        "name": "stove_top",
                        "display_name": "Stove Top",
                        "type": "appliance"
                    },
                    {
                        "display_name": "Low-Carb",
                        "type": "dietary",
                        "id": 64467,
                        "name": "low_carb"
                    },
                    {
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty",
                        "id": 64472,
                        "name": "under_30_minutes"
                    },
                    {
                        "type": "method",
                        "id": 65859,
                        "name": "pan_fry",
                        "display_name": "Pan Fry"
                    },
                    {
                        "display_name": "Weeknight",
                        "type": "occasion",
                        "id": 64505,
                        "name": "weeknight"
                    },
                    {
                        "id": 3028912,
                        "name": "chop_champ",
                        "display_name": "Chop Champ",
                        "type": "business_tags"
                    },
                    {
                        "id": 64479,
                        "name": "thanksgiving",
                        "display_name": "Thanksgiving",
                        "type": "holiday"
                    },
                    {
                        "type": "meal",
                        "id": 64490,
                        "name": "sides",
                        "display_name": "Sides"
                    }
                ],
                "show": {
                    "name": "Goodful",
                    "id": 34
                },
                "name": "Beet And Pear Salad With Lemon Vinaigrette",
                "show_id": 34,
                "compilations": [
                    {
                        "id": 707,
                        "keywords": null,
                        "promotion": "full",
                        "slug": "revamped-classic-thanksgiving-side-dishes",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/185674.jpg",
                        "video_url": "https://vid.tasty.co/output/113593/hls24_1540835779.m3u8",
                        "buzz_id": null,
                        "is_shoppable": false,
                        "language": "eng",
                        "canonical_id": "compilation:707",
                        "beauty_url": null,
                        "country": "US",
                        "created_at": 1540591207,
                        "name": "Revamped Classic Thanksgiving Side Dishes",
                        "facebook_posts": [],
                        "approved_at": 1541174737,
                        "aspect_ratio": "16:9",
                        "description": null,
                        "draft_status": "published",
                        "video_id": 69574,
                        "show": [
                            {
                                "id": 17,
                                "name": "Tasty"
                            }
                        ]
                    },
                    {
                        "id": 1438,
                        "video_id": 102112,
                        "approved_at": 1589378001,
                        "beauty_url": null,
                        "is_shoppable": false,
                        "keywords": null,
                        "video_url": "https://vid.tasty.co/output/162722/hls24_1584513638.m3u8",
                        "facebook_posts": [],
                        "aspect_ratio": "1:1",
                        "created_at": 1584513567,
                        "canonical_id": "compilation:1438",
                        "buzz_id": null,
                        "promotion": "full",
                        "draft_status": "published",
                        "language": "eng",
                        "name": "Refreshing Summer Fruit Salads",
                        "slug": "refreshing-summer-fruit-salads",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/258777.jpg",
                        "show": [
                            {
                                "id": 34,
                                "name": "Goodful"
                            }
                        ],
                        "country": "US",
                        "description": null
                    }
                ]
            },
            {
                "yields": "Servings: 6",
                "cook_time_minutes": null,
                "country": "US",
                "inspired_by_url": "http://www.healthywomen.org/content/article/mango-and-watermelon-salad",
                "servings_noun_plural": "servings",
                "tips_and_ratings_enabled": true,
                "video_id": 56421,
                "video_url": "https://vid.tasty.co/output/96222/hls24_1527619829.m3u8",
                "tags": [
                    {
                        "id": 64466,
                        "name": "healthy",
                        "display_name": "Healthy",
                        "type": "dietary"
                    },
                    {
                        "type": "difficulty",
                        "id": 64471,
                        "name": "easy",
                        "display_name": "Easy"
                    },
                    {
                        "id": 1280503,
                        "name": "cutting_board",
                        "display_name": "Cutting Board",
                        "type": "equipment"
                    },
                    {
                        "id": 1280501,
                        "name": "chefs_knife",
                        "display_name": "Chef's Knife",
                        "type": "equipment"
                    },
                    {
                        "name": "whisk",
                        "display_name": "Whisk",
                        "type": "equipment",
                        "id": 1247793
                    },
                    {
                        "id": 1247794,
                        "name": "wooden_spoon",
                        "display_name": "Wooden Spoon",
                        "type": "equipment"
                    },
                    {
                        "id": 64489,
                        "name": "lunch",
                        "display_name": "Lunch",
                        "type": "meal"
                    },
                    {
                        "id": 64469,
                        "name": "vegetarian",
                        "display_name": "Vegetarian",
                        "type": "dietary"
                    },
                    {
                        "type": "difficulty",
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes"
                    },
                    {
                        "id": 64463,
                        "name": "dairy_free",
                        "display_name": "Dairy-Free",
                        "type": "dietary"
                    },
                    {
                        "id": 1247785,
                        "name": "pyrex",
                        "display_name": "Pyrex",
                        "type": "equipment"
                    },
                    {
                        "display_name": "Mixing Bowl",
                        "type": "equipment",
                        "id": 1280510,
                        "name": "mixing_bowl"
                    },
                    {
                        "id": 64510,
                        "name": "summer",
                        "display_name": "Summer",
                        "type": "seasonal"
                    }
                ],
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/17cd5717ca7843c1ae84ab835c28ecfc/BFV43452_WatermelonSalads3Ways-FB.mp4",
                "nutrition": {
                    "protein": 3,
                    "sugar": 4,
                    "fiber": 2,
                    "updated_at": "2019-05-29T16:00:00+02:00",
                    "calories": 126,
                    "carbohydrates": 8,
                    "fat": 9
                },
                "_index": "recipes-20200519201145",
                "compilations": [
                    {
                        "description": null,
                        "draft_status": "published",
                        "id": 575,
                        "language": "eng",
                        "promotion": "full",
                        "canonical_id": "compilation:575",
                        "country": "US",
                        "buzz_id": null,
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/159622.jpg",
                        "video_url": "https://vid.tasty.co/output/96222/hls24_1527619829.m3u8",
                        "facebook_posts": [],
                        "beauty_url": "https://img.buzzfeed.com/video-api-prod/assets/3401a9b8b2c347aaaa4ebd9cf90d7b0b/BFV43452_WatermelonSalads3WaysBeauty.jpg",
                        "aspect_ratio": "1:1",
                        "is_shoppable": false,
                        "keywords": null,
                        "slug": "watermelon-salads-3-ways",
                        "video_id": 56421,
                        "approved_at": 1527799093,
                        "name": "Watermelon Salads 3 Ways",
                        "show": [
                            {
                                "id": 34,
                                "name": "Goodful"
                            }
                        ],
                        "created_at": 1527614339
                    },
                    {
                        "beauty_url": null,
                        "buzz_id": null,
                        "country": "US",
                        "draft_status": "published",
                        "id": 1438,
                        "name": "Refreshing Summer Fruit Salads",
                        "aspect_ratio": "1:1",
                        "keywords": null,
                        "promotion": "full",
                        "slug": "refreshing-summer-fruit-salads",
                        "video_url": "https://vid.tasty.co/output/162722/hls24_1584513638.m3u8",
                        "show": [
                            {
                                "id": 34,
                                "name": "Goodful"
                            }
                        ],
                        "description": null,
                        "is_shoppable": false,
                        "language": "eng",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/258777.jpg",
                        "video_id": 102112,
                        "canonical_id": "compilation:1438",
                        "approved_at": 1589378001,
                        "created_at": 1584513567,
                        "facebook_posts": []
                    }
                ],
                "_type": "recipe",
                "beauty_url": null,
                "id": 3945,
                "promotion": "full",
                "credits": [
                    {
                        "name": "Joey Firoben",
                        "type": "internal"
                    }
                ],
                "aspect_ratio": "1:1",
                "draft_status": "published",
                "keywords": "almonds, arugula, balsamic reduction, feta, fruit, healthy, herbs, honey lime dressing, lemon vinaigrette, light, nuts, salad, spinach, summer, veggies, watermelon",
                "total_time_minutes": null,
                "updated_at": 1560181600,
                "_op_type": "index",
                "video_ad_content": "none",
                "is_one_top": false,
                "is_shoppable": true,
                "num_servings": 6,
                "brand": null,
                "instructions": [
                    {
                        "id": 34720,
                        "display_text": "Using a sharp knife, cut off the top and bottom of the watermelon to reveal the red flesh and create flat surfaces on both sides. Stand the watermelon upright, and slice off the remaining peel so only the red core remains.",
                        "position": 1,
                        "start_time": 5333,
                        "end_time": 24333,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "position": 2,
                        "start_time": 24000,
                        "end_time": 36500,
                        "temperature": null,
                        "appliance": null,
                        "id": 34721,
                        "display_text": "Cut the watermelon into roughly 1-inch (2-cm) cubes and transfer to a large bowl."
                    },
                    {
                        "display_text": "In a small bowl, whisk together the lemon juice, water, honey, salt, and pepper.",
                        "position": 3,
                        "start_time": 39000,
                        "end_time": 49500,
                        "temperature": null,
                        "appliance": null,
                        "id": 34722
                    },
                    {
                        "id": 34723,
                        "display_text": "While whisking, gradually add the oil in a very slow, steady stream to create an emulsification. Set aside.",
                        "position": 4,
                        "start_time": 50000,
                        "end_time": 54333,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "display_text": "Add the spinach, mango, almonds, and lemon vinaigrette to the large bowl with the watermelon.",
                        "position": 5,
                        "start_time": 55000,
                        "end_time": 67833,
                        "temperature": null,
                        "appliance": null,
                        "id": 34724
                    },
                    {
                        "start_time": 68000,
                        "end_time": 71666,
                        "temperature": null,
                        "appliance": null,
                        "id": 34725,
                        "display_text": "Toss the salad until well-combined, then divide between serving bowls.",
                        "position": 6
                    },
                    {
                        "position": 7,
                        "start_time": 79666,
                        "end_time": 82833,
                        "temperature": null,
                        "appliance": null,
                        "id": 34726,
                        "display_text": "Enjoy!"
                    }
                ],
                "sections": [
                    {
                        "components": [
                            {
                                "ingredient": {
                                    "display_singular": "small watermelon",
                                    "display_plural": "small watermelons",
                                    "created_at": 1527798360,
                                    "updated_at": 1527798360,
                                    "id": 4202,
                                    "name": "small watermelon"
                                },
                                "id": 39887,
                                "raw_text": "1 small watermelon",
                                "extra_comment": "",
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 322212,
                                        "quantity": "1",
                                        "unit": {
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none",
                                            "name": ""
                                        }
                                    }
                                ]
                            },
                            {
                                "id": 39888,
                                "raw_text": "3 cups baby spinach",
                                "extra_comment": "",
                                "position": 2,
                                "measurements": [
                                    {
                                        "id": 322210,
                                        "quantity": "120",
                                        "unit": {
                                            "display_plural": "g",
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g"
                                        }
                                    },
                                    {
                                        "id": 322209,
                                        "quantity": "3",
                                        "unit": {
                                            "display_plural": "cups",
                                            "system": "imperial",
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035279,
                                    "id": 208,
                                    "name": "baby spinach",
                                    "display_singular": "baby spinach",
                                    "display_plural": "baby spinaches",
                                    "created_at": 1494210136
                                }
                            },
                            {
                                "id": 39889,
                                "raw_text": "1 mango, pitted, peeled, and diced",
                                "extra_comment": "pitted, peeled, and diced",
                                "position": 3,
                                "measurements": [
                                    {
                                        "id": 322207,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 478,
                                    "name": "mango",
                                    "display_singular": "mango",
                                    "display_plural": "mangoes",
                                    "created_at": 1495134656,
                                    "updated_at": 1509035257
                                }
                            },
                            {
                                "raw_text": "½ cup slivered almonds",
                                "extra_comment": "",
                                "position": 4,
                                "measurements": [
                                    {
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        },
                                        "id": 322217,
                                        "quantity": "70"
                                    },
                                    {
                                        "unit": {
                                            "system": "imperial",
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups"
                                        },
                                        "id": 322216,
                                        "quantity": "½"
                                    }
                                ],
                                "ingredient": {
                                    "id": 500,
                                    "name": "slivered almonds",
                                    "display_singular": "slivered almond",
                                    "display_plural": "slivered almonds",
                                    "created_at": 1495156187,
                                    "updated_at": 1509035255
                                },
                                "id": 39890
                            }
                        ],
                        "name": null,
                        "position": 1
                    },
                    {
                        "components": [
                            {
                                "extra_comment": "",
                                "position": 6,
                                "measurements": [
                                    {
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        },
                                        "id": 322211
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1495561272,
                                    "updated_at": 1509035246,
                                    "id": 625,
                                    "name": "fresh lemon juice",
                                    "display_singular": "fresh lemon juice",
                                    "display_plural": "fresh lemon juices"
                                },
                                "id": 39892,
                                "raw_text": "2 tablespoons fresh lemon juice"
                            },
                            {
                                "extra_comment": "",
                                "position": 7,
                                "measurements": [
                                    {
                                        "id": 322215,
                                        "quantity": "1",
                                        "unit": {
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 197,
                                    "name": "water",
                                    "display_singular": "water",
                                    "display_plural": "waters",
                                    "created_at": 1494124627,
                                    "updated_at": 1509035280
                                },
                                "id": 39893,
                                "raw_text": "1 tablespoon water"
                            },
                            {
                                "position": 8,
                                "measurements": [
                                    {
                                        "id": 322214,
                                        "quantity": "1",
                                        "unit": {
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 52,
                                    "name": "honey",
                                    "display_singular": "honey",
                                    "display_plural": "honeys",
                                    "created_at": 1493430363,
                                    "updated_at": 1509035286
                                },
                                "id": 39894,
                                "raw_text": "1 tablespoon honey",
                                "extra_comment": ""
                            },
                            {
                                "ingredient": {
                                    "id": 22,
                                    "name": "salt",
                                    "display_singular": "salt",
                                    "display_plural": "salts",
                                    "created_at": 1493314644,
                                    "updated_at": 1509035288
                                },
                                "id": 39895,
                                "raw_text": "Salt, to taste",
                                "extra_comment": "to taste",
                                "position": 9,
                                "measurements": [
                                    {
                                        "id": 322218,
                                        "quantity": "0",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ]
                            },
                            {
                                "id": 39896,
                                "raw_text": "Pepper, to taste",
                                "extra_comment": "to taste",
                                "position": 10,
                                "measurements": [
                                    {
                                        "id": 322208,
                                        "quantity": "0",
                                        "unit": {
                                            "system": "none",
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "pepper",
                                    "display_plural": "peppers",
                                    "created_at": 1493314935,
                                    "updated_at": 1509035287,
                                    "id": 29,
                                    "name": "pepper"
                                }
                            },
                            {
                                "id": 39897,
                                "raw_text": "2 tablespoons extra-virgin olive oil",
                                "extra_comment": "",
                                "position": 11,
                                "measurements": [
                                    {
                                        "id": 322213,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035259,
                                    "id": 452,
                                    "name": "extra virgin olive oil",
                                    "display_singular": "extra virgin olive oil",
                                    "display_plural": "extra virgin olive oils",
                                    "created_at": 1495076759
                                }
                            }
                        ],
                        "name": "Lemon Vinaigrette",
                        "position": 2
                    }
                ],
                "user_ratings": {
                    "count_negative": 8,
                    "score": 0.925234,
                    "count_positive": 99
                },
                "renditions": [
                    {
                        "maximum_bit_rate": null,
                        "aspect": "square",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/96222/square_720/1527619829",
                        "bit_rate": 2000,
                        "width": 720,
                        "duration": 156619,
                        "file_size": 39138966,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/96222/square_720/1527619829_00001.png",
                        "name": "mp4_720x720",
                        "height": 720
                    },
                    {
                        "duration": 156619,
                        "bit_rate": 699,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "square",
                        "url": "https://vid.tasty.co/output/96222/square_320/1527619829",
                        "width": 320,
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/96222/square_320/1527619829_00001.png",
                        "file_size": 13679043,
                        "container": "mp4",
                        "name": "mp4_320x320",
                        "height": 320
                    },
                    {
                        "bit_rate": 2001,
                        "minimum_bit_rate": null,
                        "aspect": "square",
                        "container": "mp4",
                        "name": "mp4_720x720",
                        "height": 720,
                        "duration": 156619,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "url": "https://vid.tasty.co/output/96222/landscape_720/1527619829",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/96222/landscape_720/1527619829_00001.png",
                        "width": 720,
                        "file_size": 39164902
                    },
                    {
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/96222/landscape_480/1527619829",
                        "width": 480,
                        "duration": 156619,
                        "bit_rate": 1158,
                        "maximum_bit_rate": null,
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/96222/landscape_480/1527619829_00001.png",
                        "name": "mp4_480x480",
                        "height": 480,
                        "file_size": 22665939,
                        "aspect": "square"
                    },
                    {
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/96222/1445289064805-h2exzu/1527619829_00001.png",
                        "name": "low",
                        "height": 1080,
                        "duration": 156615,
                        "bit_rate": null,
                        "minimum_bit_rate": 276,
                        "container": "ts",
                        "url": "https://vid.tasty.co/output/96222/hls24_1527619829.m3u8",
                        "width": 1080,
                        "file_size": null,
                        "maximum_bit_rate": 3416,
                        "content_type": "application/vnd.apple.mpegurl",
                        "aspect": "square"
                    }
                ],
                "_id": 3945,
                "language": "eng",
                "prep_time_minutes": null,
                "servings_noun_singular": "serving",
                "slug": "watermelon-salad-with-spinach-and-mango",
                "total_time_tier": {
                    "tier": "under_30_minutes",
                    "display_tier": "Under 30 minutes"
                },
                "buzz_id": null,
                "description": null,
                "show_id": 34,
                "canonical_id": "recipe:3945",
                "facebook_posts": [],
                "show": {
                    "id": 34,
                    "name": "Goodful"
                },
                "approved_at": 1527798861,
                "brand_id": null,
                "created_at": 1527614254,
                "name": "Watermelon Salad With Spinach And Mango",
                "seo_title": null,
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/0883c280d72f444686fe7d01aa00af38/BFV43452_WatermelonSalads3Ways-FB.jpg",
                "nutrition_visibility": "auto"
            },
            {
                "_index": "recipes-20200519201145",
                "_type": "recipe",
                "buzz_id": null,
                "promotion": "full",
                "sections": [
                    {
                        "components": [
                            {
                                "id": 17682,
                                "raw_text": "3 bananas, sliced",
                                "extra_comment": "sliced",
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 150472,
                                        "quantity": "3",
                                        "unit": {
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none",
                                            "name": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "banana",
                                    "display_singular": "banana",
                                    "display_plural": "bananas",
                                    "created_at": 1493430017,
                                    "updated_at": 1509035287,
                                    "id": 38
                                }
                            },
                            {
                                "ingredient": {
                                    "id": 587,
                                    "name": "fresh strawberry ",
                                    "display_singular": "fresh strawberry ",
                                    "display_plural": "fresh strawberries ",
                                    "created_at": 1495411470,
                                    "updated_at": 1511888083
                                },
                                "id": 17683,
                                "raw_text": "12 ounces strawberries, quartered",
                                "extra_comment": "quartered",
                                "position": 2,
                                "measurements": [
                                    {
                                        "id": 150476,
                                        "quantity": "12",
                                        "unit": {
                                            "name": "ounce",
                                            "abbreviation": "oz",
                                            "display_singular": "oz",
                                            "display_plural": "oz",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "id": 150475,
                                        "quantity": "340",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        }
                                    }
                                ]
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 150471,
                                        "quantity": "12",
                                        "unit": {
                                            "name": "ounce",
                                            "abbreviation": "oz",
                                            "display_singular": "oz",
                                            "display_plural": "oz",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "id": 150470,
                                        "quantity": "340",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "fresh raspberry",
                                    "display_plural": "fresh raspberries",
                                    "created_at": 1500667939,
                                    "updated_at": 1509035129,
                                    "id": 2415,
                                    "name": "fresh raspberry"
                                },
                                "id": 17684,
                                "raw_text": "12 ounces raspberries",
                                "extra_comment": "",
                                "position": 3
                            }
                        ],
                        "name": null,
                        "position": 1
                    },
                    {
                        "components": [
                            {
                                "measurements": [
                                    {
                                        "id": 150473,
                                        "quantity": "3",
                                        "unit": {
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 330,
                                    "name": "lime juice",
                                    "display_singular": "lime juice",
                                    "display_plural": "lime juices",
                                    "created_at": 1494878288,
                                    "updated_at": 1509035269
                                },
                                "id": 19670,
                                "raw_text": "n/a",
                                "extra_comment": "",
                                "position": 5
                            },
                            {
                                "id": 19671,
                                "raw_text": "n/a",
                                "extra_comment": "",
                                "position": 6,
                                "measurements": [
                                    {
                                        "quantity": "1",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        },
                                        "id": 150474
                                    }
                                ],
                                "ingredient": {
                                    "name": "maple syrup",
                                    "display_singular": "maple syrup",
                                    "display_plural": "maple syrups",
                                    "created_at": 1494966352,
                                    "updated_at": 1509035267,
                                    "id": 359
                                }
                            }
                        ],
                        "name": "Dressing",
                        "position": 2
                    }
                ],
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/7759bbe3ad534174968598a01f740a25/Tasty_-_Facebook_-_1080x1080_3.mp4",
                "renditions": [
                    {
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/29837/mp4_1280X720/1492801391",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_1280X720/1492801391_00001.png",
                        "name": "mp4_720x720",
                        "file_size": 71449818,
                        "bit_rate": 7277,
                        "duration": 78554,
                        "content_type": "video/mp4",
                        "aspect": "square",
                        "height": 720,
                        "width": 720
                    },
                    {
                        "width": 1080,
                        "maximum_bit_rate": 8246,
                        "minimum_bit_rate": 272,
                        "content_type": "application/vnd.apple.mpegurl",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/1445289064805-h2exzu/1492801391_00001.png",
                        "name": "low",
                        "height": 1080,
                        "duration": 78487,
                        "file_size": null,
                        "bit_rate": null,
                        "aspect": "square"
                    },
                    {
                        "container": "mp4",
                        "name": "mp4_640x640",
                        "width": 640,
                        "file_size": 71259012,
                        "bit_rate": 7258,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_640x640/1492801391_00001.png",
                        "height": 640,
                        "duration": 78554,
                        "minimum_bit_rate": null,
                        "aspect": "square",
                        "url": "https://vid.tasty.co/output/29837/mp4_640x640/1492801391"
                    },
                    {
                        "width": 720,
                        "bit_rate": 7295,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "square",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/29837/mp4_720x1280/1492801391",
                        "name": "mp4_720x720",
                        "height": 720,
                        "duration": 78554,
                        "file_size": 71629367,
                        "minimum_bit_rate": null,
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_720x1280/1492801391_00001.png"
                    }
                ],
                "total_time_minutes": null,
                "updated_at": 1560184060,
                "_op_type": "index",
                "aspect_ratio": "1:1",
                "beauty_url": null,
                "country": "ZZ",
                "slug": "banana-berry-fruit-salad",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/b67175f53c0e4a7483956d0098cb9542/Tasty_-_Facebook_-_1080x1080_3.jpg",
                "brand_id": null,
                "total_time_tier": {
                    "tier": "under_30_minutes",
                    "display_tier": "Under 30 minutes"
                },
                "instructions": [
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 14824,
                        "display_text": "Combine all the ingredients above in a large bowl.",
                        "position": 1,
                        "start_time": 26000,
                        "end_time": 28980
                    },
                    {
                        "position": 2,
                        "start_time": 30000,
                        "end_time": 35000,
                        "temperature": null,
                        "appliance": null,
                        "id": 14825,
                        "display_text": "Mix the dressing ingredients together and spread over fruit, mix well."
                    },
                    {
                        "display_text": "Enjoy!",
                        "position": 3,
                        "start_time": 36000,
                        "end_time": 39000,
                        "temperature": null,
                        "appliance": null,
                        "id": 14826
                    }
                ],
                "show": {
                    "name": "Tasty: Tasty Vegetarian",
                    "id": 49
                },
                "cook_time_minutes": null,
                "draft_status": "published",
                "id": 1718,
                "servings_noun_singular": "serving",
                "brand": null,
                "facebook_posts": [],
                "tips_and_ratings_enabled": true,
                "nutrition_visibility": "auto",
                "credits": [
                    {
                        "name": "Merle O'Neal",
                        "type": "internal"
                    }
                ],
                "canonical_id": "recipe:1718",
                "show_id": 49,
                "video_url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                "approved_at": 1500926522,
                "inspired_by_url": null,
                "is_shoppable": true,
                "language": "eng",
                "servings_noun_plural": "servings",
                "compilations": [
                    {
                        "aspect_ratio": "1:1",
                        "beauty_url": null,
                        "description": null,
                        "draft_status": "published",
                        "is_shoppable": false,
                        "keywords": null,
                        "language": "eng",
                        "promotion": "full",
                        "thumbnail_url": "https://img.buzzfeed.com/video-api-prod/assets/a21c99c4ea144b91953b4d02e0cb03e7/FB_Thumb.jpg",
                        "buzz_id": null,
                        "video_url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                        "id": 109,
                        "video_id": 15797,
                        "facebook_posts": [],
                        "approved_at": 1500926543,
                        "country": "ZZ",
                        "created_at": 1499830593,
                        "name": "Fruit Salad 4 Ways",
                        "slug": "fruit-salad-4-ways",
                        "canonical_id": "compilation:109",
                        "show": [
                            {
                                "id": 49,
                                "name": "Tasty: Tasty Vegetarian"
                            }
                        ]
                    },
                    {
                        "aspect_ratio": "1:1",
                        "keywords": null,
                        "language": "eng",
                        "promotion": "full",
                        "canonical_id": "compilation:292",
                        "approved_at": 1511810905,
                        "beauty_url": null,
                        "draft_status": "published",
                        "name": "7 Desserts That Are 5 Ingredients Or Less",
                        "slug": "7-desserts-that-are-5-ingredients-or-less",
                        "video_id": 31496,
                        "country": "US",
                        "id": 292,
                        "is_shoppable": false,
                        "buzz_id": null,
                        "created_at": 1511810436,
                        "description": null,
                        "thumbnail_url": "https://s3.amazonaws.com/video-api-prod/assets/f66349d148e845cea331fa2a8966f74f/BFV29781_7DessertsThatAre5IngredientsOrLessThumb.jpg",
                        "video_url": "https://vid.tasty.co/output/54712/low_1507056850.m3u8",
                        "facebook_posts": [],
                        "show": [
                            {
                                "id": 34,
                                "name": "Goodful"
                            }
                        ]
                    },
                    {
                        "description": null,
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/258777.jpg",
                        "buzz_id": null,
                        "country": "US",
                        "created_at": 1584513567,
                        "id": 1438,
                        "is_shoppable": false,
                        "name": "Refreshing Summer Fruit Salads",
                        "video_id": 102112,
                        "canonical_id": "compilation:1438",
                        "approved_at": 1589378001,
                        "aspect_ratio": "1:1",
                        "beauty_url": null,
                        "keywords": null,
                        "video_url": "https://vid.tasty.co/output/162722/hls24_1584513638.m3u8",
                        "facebook_posts": [],
                        "show": [
                            {
                                "id": 34,
                                "name": "Goodful"
                            }
                        ],
                        "draft_status": "published",
                        "language": "eng",
                        "promotion": "full",
                        "slug": "refreshing-summer-fruit-salads"
                    }
                ],
                "tags": [
                    {
                        "name": "dairy_free",
                        "display_name": "Dairy-Free",
                        "type": "dietary",
                        "id": 64463
                    },
                    {
                        "id": 64488,
                        "name": "kid_friendly",
                        "display_name": "Kid-Friendly",
                        "type": "dietary"
                    },
                    {
                        "id": 64469,
                        "name": "vegetarian",
                        "display_name": "Vegetarian",
                        "type": "dietary"
                    },
                    {
                        "display_name": "Vegan",
                        "type": "dietary",
                        "id": 64468,
                        "name": "vegan"
                    },
                    {
                        "id": 64466,
                        "name": "healthy",
                        "display_name": "Healthy",
                        "type": "dietary"
                    },
                    {
                        "display_name": "Easy",
                        "type": "difficulty",
                        "id": 64471,
                        "name": "easy"
                    },
                    {
                        "id": 64470,
                        "name": "5_ingredients_or_less",
                        "display_name": "5 Ingredients or Less",
                        "type": "difficulty"
                    },
                    {
                        "id": 64485,
                        "name": "desserts",
                        "display_name": "Desserts",
                        "type": "meal"
                    },
                    {
                        "display_name": "Sides",
                        "type": "meal",
                        "id": 64490,
                        "name": "sides"
                    },
                    {
                        "type": "meal",
                        "id": 64491,
                        "name": "snacks",
                        "display_name": "Snacks"
                    },
                    {
                        "display_name": "Brunch",
                        "type": "occasion",
                        "id": 64484,
                        "name": "brunch"
                    },
                    {
                        "id": 64483,
                        "name": "breakfast",
                        "display_name": "Breakfast",
                        "type": "meal"
                    },
                    {
                        "id": 64504,
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "occasion"
                    },
                    {
                        "id": 64465,
                        "name": "gluten_free",
                        "display_name": "Gluten-Free",
                        "type": "dietary"
                    },
                    {
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty",
                        "id": 64472,
                        "name": "under_30_minutes"
                    },
                    {
                        "id": 64503,
                        "name": "casual_party",
                        "display_name": "Casual Party",
                        "type": "occasion"
                    },
                    {
                        "id": 1247794,
                        "name": "wooden_spoon",
                        "display_name": "Wooden Spoon",
                        "type": "equipment"
                    },
                    {
                        "id": 1247785,
                        "name": "pyrex",
                        "display_name": "Pyrex",
                        "type": "equipment"
                    }
                ],
                "video_ad_content": "none",
                "name": "Banana Berry Fruit Salad",
                "num_servings": 4,
                "seo_title": null,
                "video_id": 15797,
                "yields": "Serves 4-5",
                "_id": 1718,
                "user_ratings": {
                    "count_positive": 234,
                    "count_negative": 10,
                    "score": 0.959016
                },
                "nutrition": {
                    "calories": 189,
                    "carbohydrates": 47,
                    "fat": 1,
                    "protein": 2,
                    "sugar": 24,
                    "fiber": 10,
                    "updated_at": "2019-05-29T16:00:00+02:00"
                },
                "created_at": 1499830592,
                "description": null,
                "is_one_top": false,
                "keywords": null,
                "prep_time_minutes": null
            },
            {
                "id": 1720,
                "seo_title": null,
                "total_time_tier": {
                    "display_tier": "Under 30 minutes",
                    "tier": "under_30_minutes"
                },
                "_op_type": "index",
                "buzz_id": null,
                "draft_status": "published",
                "is_shoppable": true,
                "name": "Melon Berry Fruit Salad",
                "num_servings": 4,
                "promotion": "full",
                "slug": "melon-berry-fruit-salad",
                "nutrition_visibility": "auto",
                "approved_at": 1500926080,
                "nutrition": {
                    "sugar": 35,
                    "fiber": 12,
                    "updated_at": "2019-05-29T16:00:00+02:00",
                    "calories": 219,
                    "carbohydrates": 54,
                    "fat": 1,
                    "protein": 3
                },
                "_id": 1720,
                "sections": [
                    {
                        "components": [
                            {
                                "extra_comment": "quartered",
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 340527,
                                        "quantity": "340",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        }
                                    },
                                    {
                                        "quantity": "12",
                                        "unit": {
                                            "name": "ounce",
                                            "abbreviation": "oz",
                                            "display_singular": "oz",
                                            "display_plural": "oz",
                                            "system": "imperial"
                                        },
                                        "id": 340526
                                    }
                                ],
                                "ingredient": {
                                    "id": 398,
                                    "name": "strawberry",
                                    "display_singular": "strawberry",
                                    "display_plural": "strawberries",
                                    "created_at": 1494983212,
                                    "updated_at": 1509035264
                                },
                                "id": 17692,
                                "raw_text": "12 ounces strawberries, quartered"
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 340529,
                                        "quantity": "340",
                                        "unit": {
                                            "display_plural": "g",
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g"
                                        }
                                    },
                                    {
                                        "id": 340528,
                                        "quantity": "12",
                                        "unit": {
                                            "name": "ounce",
                                            "abbreviation": "oz",
                                            "display_singular": "oz",
                                            "display_plural": "oz",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1495065307,
                                    "updated_at": 1509035261,
                                    "id": 431,
                                    "name": "raspberry",
                                    "display_singular": "raspberry",
                                    "display_plural": "raspberries"
                                },
                                "id": 17693,
                                "raw_text": "12 ounces raspberries",
                                "extra_comment": "",
                                "position": 2
                            },
                            {
                                "raw_text": "12 ounces blueberries",
                                "extra_comment": "",
                                "position": 3,
                                "measurements": [
                                    {
                                        "id": 340531,
                                        "quantity": "340",
                                        "unit": {
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric",
                                            "name": "gram"
                                        }
                                    },
                                    {
                                        "id": 340530,
                                        "quantity": "12",
                                        "unit": {
                                            "abbreviation": "oz",
                                            "display_singular": "oz",
                                            "display_plural": "oz",
                                            "system": "imperial",
                                            "name": "ounce"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035263,
                                    "id": 400,
                                    "name": "blueberry",
                                    "display_singular": "blueberry",
                                    "display_plural": "blueberries",
                                    "created_at": 1494983257
                                },
                                "id": 17694
                            },
                            {
                                "id": 17695,
                                "raw_text": "8 ounces blackberries",
                                "extra_comment": "",
                                "position": 4,
                                "measurements": [
                                    {
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        },
                                        "id": 340525,
                                        "quantity": "225"
                                    },
                                    {
                                        "id": 340524,
                                        "quantity": "8",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "ounce",
                                            "abbreviation": "oz",
                                            "display_singular": "oz",
                                            "display_plural": "oz"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 432,
                                    "name": "blackberry",
                                    "display_singular": "blackberry",
                                    "display_plural": "blackberries",
                                    "created_at": 1495065401,
                                    "updated_at": 1509035261
                                }
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 340523,
                                        "quantity": "455",
                                        "unit": {
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g"
                                        }
                                    },
                                    {
                                        "unit": {
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial",
                                            "name": "cup"
                                        },
                                        "id": 340522,
                                        "quantity": "3"
                                    }
                                ],
                                "ingredient": {
                                    "display_plural": "watermelons",
                                    "created_at": 1501540486,
                                    "updated_at": 1509035114,
                                    "id": 2720,
                                    "name": "watermelon",
                                    "display_singular": "watermelon"
                                },
                                "id": 17696,
                                "raw_text": "3 cups watermelon, cubed",
                                "extra_comment": "cubed",
                                "position": 5
                            }
                        ],
                        "name": null,
                        "position": 1
                    },
                    {
                        "components": [
                            {
                                "position": 7,
                                "measurements": [
                                    {
                                        "id": 340520,
                                        "quantity": "3",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 330,
                                    "name": "lime juice",
                                    "display_singular": "lime juice",
                                    "display_plural": "lime juices",
                                    "created_at": 1494878288,
                                    "updated_at": 1509035269
                                },
                                "id": 19664,
                                "raw_text": "n/a",
                                "extra_comment": ""
                            },
                            {
                                "id": 19665,
                                "raw_text": "n/a",
                                "extra_comment": "",
                                "position": 8,
                                "measurements": [
                                    {
                                        "id": 340521,
                                        "quantity": "1",
                                        "unit": {
                                            "system": "imperial",
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035267,
                                    "id": 359,
                                    "name": "maple syrup",
                                    "display_singular": "maple syrup",
                                    "display_plural": "maple syrups",
                                    "created_at": 1494966352
                                }
                            }
                        ],
                        "name": "Dressing",
                        "position": 2
                    }
                ],
                "brand": null,
                "video_ad_content": "none",
                "_type": "recipe",
                "language": "eng",
                "cook_time_minutes": null,
                "tips_and_ratings_enabled": true,
                "video_id": 15797,
                "yields": "Serves 4-5",
                "instructions": [
                    {
                        "id": 14830,
                        "display_text": "Combine all the ingredients above in a large bowl.",
                        "position": 1,
                        "start_time": 55000,
                        "end_time": 61000,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "id": 14831,
                        "display_text": "Mix the dressing ingredients together and spread over fruit, mix well.",
                        "position": 2,
                        "start_time": 62000,
                        "end_time": 66000,
                        "temperature": null,
                        "appliance": null
                    },
                    {
                        "id": 14832,
                        "display_text": "Enjoy!",
                        "position": 3,
                        "start_time": 68000,
                        "end_time": 71000,
                        "temperature": null,
                        "appliance": null
                    }
                ],
                "facebook_posts": [],
                "aspect_ratio": "1:1",
                "is_one_top": false,
                "servings_noun_plural": "servings",
                "servings_noun_singular": "serving",
                "total_time_minutes": null,
                "video_url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                "tags": [
                    {
                        "display_name": "Vegetarian",
                        "type": "dietary",
                        "id": 64469,
                        "name": "vegetarian"
                    },
                    {
                        "id": 64466,
                        "name": "healthy",
                        "display_name": "Healthy",
                        "type": "dietary"
                    },
                    {
                        "id": 64488,
                        "name": "kid_friendly",
                        "display_name": "Kid-Friendly",
                        "type": "dietary"
                    },
                    {
                        "id": 64465,
                        "name": "gluten_free",
                        "display_name": "Gluten-Free",
                        "type": "dietary"
                    },
                    {
                        "display_name": "Easy",
                        "type": "difficulty",
                        "id": 64471,
                        "name": "easy"
                    },
                    {
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty"
                    },
                    {
                        "display_name": "Appetizers",
                        "type": "meal",
                        "id": 64481,
                        "name": "appetizers"
                    },
                    {
                        "id": 64503,
                        "name": "casual_party",
                        "display_name": "Casual Party",
                        "type": "occasion"
                    },
                    {
                        "id": 64484,
                        "name": "brunch",
                        "display_name": "Brunch",
                        "type": "occasion"
                    },
                    {
                        "id": 1247794,
                        "name": "wooden_spoon",
                        "display_name": "Wooden Spoon",
                        "type": "equipment"
                    },
                    {
                        "id": 1280507,
                        "name": "dry_measuring_cups",
                        "display_name": "Dry Measuring Cups",
                        "type": "equipment"
                    },
                    {
                        "name": "mixing_bowl",
                        "display_name": "Mixing Bowl",
                        "type": "equipment",
                        "id": 1280510
                    },
                    {
                        "id": 64444,
                        "name": "american",
                        "display_name": "American",
                        "type": "cuisine"
                    },
                    {
                        "id": 1280503,
                        "name": "cutting_board",
                        "display_name": "Cutting Board",
                        "type": "equipment"
                    },
                    {
                        "name": "chefs_knife",
                        "display_name": "Chef's Knife",
                        "type": "equipment",
                        "id": 1280501
                    }
                ],
                "credits": [
                    {
                        "name": "Merle O'Neal",
                        "type": "internal"
                    }
                ],
                "description": null,
                "canonical_id": "recipe:1720",
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/7759bbe3ad534174968598a01f740a25/Tasty_-_Facebook_-_1080x1080_3.mp4",
                "keywords": null,
                "prep_time_minutes": null,
                "updated_at": 1560184057,
                "user_ratings": {
                    "count_positive": 106,
                    "count_negative": 8,
                    "score": 0.929825
                },
                "renditions": [
                    {
                        "height": 720,
                        "width": 720,
                        "duration": 78554,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "square",
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/29837/mp4_1280X720/1492801391",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_1280X720/1492801391_00001.png",
                        "file_size": 71449818,
                        "bit_rate": 7277,
                        "name": "mp4_720x720"
                    },
                    {
                        "height": 1080,
                        "width": 1080,
                        "duration": 78487,
                        "file_size": null,
                        "minimum_bit_rate": 272,
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/1445289064805-h2exzu/1492801391_00001.png",
                        "name": "low",
                        "bit_rate": null,
                        "maximum_bit_rate": 8246,
                        "content_type": "application/vnd.apple.mpegurl",
                        "aspect": "square",
                        "url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8"
                    },
                    {
                        "duration": 78554,
                        "bit_rate": 7258,
                        "maximum_bit_rate": null,
                        "aspect": "square",
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_640x640/1492801391_00001.png",
                        "name": "mp4_640x640",
                        "height": 640,
                        "file_size": 71259012,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "url": "https://vid.tasty.co/output/29837/mp4_640x640/1492801391",
                        "width": 640
                    },
                    {
                        "width": 720,
                        "bit_rate": 7295,
                        "aspect": "square",
                        "url": "https://vid.tasty.co/output/29837/mp4_720x1280/1492801391",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_720x1280/1492801391_00001.png",
                        "name": "mp4_720x720",
                        "container": "mp4",
                        "height": 720,
                        "duration": 78554,
                        "file_size": 71629367,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4"
                    }
                ],
                "_index": "recipes-20200519201145",
                "beauty_url": null,
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/2e908097f9de4843922ea00f7b819563/Tasty_-_Facebook_-_1080x1080_3.jpg",
                "compilations": [
                    {
                        "aspect_ratio": "1:1",
                        "country": "ZZ",
                        "draft_status": "published",
                        "show": [
                            {
                                "id": 49,
                                "name": "Tasty: Tasty Vegetarian"
                            }
                        ],
                        "video_url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                        "description": null,
                        "id": 109,
                        "keywords": null,
                        "name": "Fruit Salad 4 Ways",
                        "canonical_id": "compilation:109",
                        "facebook_posts": [],
                        "approved_at": 1500926543,
                        "buzz_id": null,
                        "created_at": 1499830593,
                        "slug": "fruit-salad-4-ways",
                        "thumbnail_url": "https://img.buzzfeed.com/video-api-prod/assets/a21c99c4ea144b91953b4d02e0cb03e7/FB_Thumb.jpg",
                        "video_id": 15797,
                        "beauty_url": null,
                        "is_shoppable": false,
                        "language": "eng",
                        "promotion": "full"
                    },
                    {
                        "id": 1117,
                        "promotion": "full",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/231788.jpg",
                        "video_id": 89972,
                        "facebook_posts": [],
                        "created_at": 1567490418,
                        "buzz_id": null,
                        "is_shoppable": false,
                        "beauty_url": null,
                        "draft_status": "published",
                        "keywords": null,
                        "name": "7 Fruity Recipes To End The Summer",
                        "slug": "7-fruity-recipes-to-end-the-summer",
                        "canonical_id": "compilation:1117",
                        "show": [
                            {
                                "name": "Tasty",
                                "id": 17
                            }
                        ],
                        "approved_at": 1568171871,
                        "country": "US",
                        "description": null,
                        "language": "eng",
                        "video_url": "https://vid.tasty.co/output/143686/hls24_1567243509.m3u8",
                        "aspect_ratio": "1:1"
                    },
                    {
                        "is_shoppable": false,
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/258777.jpg",
                        "aspect_ratio": "1:1",
                        "created_at": 1584513567,
                        "description": null,
                        "draft_status": "published",
                        "id": 1438,
                        "language": "eng",
                        "name": "Refreshing Summer Fruit Salads",
                        "promotion": "full",
                        "approved_at": 1589378001,
                        "buzz_id": null,
                        "video_id": 102112,
                        "facebook_posts": [],
                        "show": [
                            {
                                "id": 34,
                                "name": "Goodful"
                            }
                        ],
                        "beauty_url": null,
                        "canonical_id": "compilation:1438",
                        "slug": "refreshing-summer-fruit-salads",
                        "video_url": "https://vid.tasty.co/output/162722/hls24_1584513638.m3u8",
                        "country": "US",
                        "keywords": null
                    }
                ],
                "country": "ZZ",
                "created_at": 1499830592,
                "inspired_by_url": null,
                "show_id": 49,
                "show": {
                    "id": 49,
                    "name": "Tasty: Tasty Vegetarian"
                },
                "brand_id": null
            },
            {
                "nutrition": {
                    "calories": 239,
                    "carbohydrates": 60,
                    "fat": 1,
                    "protein": 3,
                    "sugar": 45,
                    "fiber": 9,
                    "updated_at": "2019-05-29T16:00:00+02:00"
                },
                "language": "eng",
                "promotion": "full",
                "total_time_minutes": null,
                "nutrition_visibility": "auto",
                "compilations": [
                    {
                        "created_at": 1499830593,
                        "video_url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                        "canonical_id": "compilation:109",
                        "show": [
                            {
                                "id": 49,
                                "name": "Tasty: Tasty Vegetarian"
                            }
                        ],
                        "draft_status": "published",
                        "keywords": null,
                        "name": "Fruit Salad 4 Ways",
                        "promotion": "full",
                        "approved_at": 1500926543,
                        "aspect_ratio": "1:1",
                        "slug": "fruit-salad-4-ways",
                        "video_id": 15797,
                        "facebook_posts": [],
                        "thumbnail_url": "https://img.buzzfeed.com/video-api-prod/assets/a21c99c4ea144b91953b4d02e0cb03e7/FB_Thumb.jpg",
                        "beauty_url": null,
                        "buzz_id": null,
                        "country": "ZZ",
                        "description": null,
                        "id": 109,
                        "is_shoppable": false,
                        "language": "eng"
                    },
                    {
                        "beauty_url": null,
                        "country": "US",
                        "description": null,
                        "is_shoppable": false,
                        "language": "eng",
                        "approved_at": 1555264996,
                        "name": "Detox Foods",
                        "promotion": "full",
                        "slug": "detox-foods",
                        "video_id": 80537,
                        "video_url": "https://vid.tasty.co/output/129416/hls24_1554821489.m3u8",
                        "facebook_posts": [],
                        "show": [
                            {
                                "id": 17,
                                "name": "Tasty"
                            }
                        ],
                        "draft_status": "published",
                        "buzz_id": null,
                        "created_at": 1554467942,
                        "id": 912,
                        "keywords": null,
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/211157.jpg",
                        "aspect_ratio": "1:1",
                        "canonical_id": "compilation:912"
                    },
                    {
                        "approved_at": 1560999518,
                        "buzz_id": null,
                        "draft_status": "published",
                        "facebook_posts": [],
                        "created_at": 1560927249,
                        "keywords": null,
                        "name": "12 Creamy & Crunchy Salads To Enjoy This Summer",
                        "canonical_id": "compilation:974",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/221541.jpg",
                        "beauty_url": null,
                        "country": "US",
                        "description": null,
                        "id": 974,
                        "is_shoppable": false,
                        "language": "eng",
                        "promotion": "full",
                        "video_id": 85724,
                        "aspect_ratio": "1:1",
                        "slug": "12-creamy-crunchy-salads-to-enjoy-this-summer",
                        "video_url": "https://vid.tasty.co/output/136451/hls24_1560927664.m3u8",
                        "show": [
                            {
                                "id": 17,
                                "name": "Tasty"
                            }
                        ]
                    },
                    {
                        "description": null,
                        "slug": "10-power-salads-for-a-healthy-lifestyle",
                        "approved_at": 1571319624,
                        "aspect_ratio": "1:1",
                        "beauty_url": null,
                        "language": "eng",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/236293.jpg",
                        "video_url": "https://vid.tasty.co/output/146884/hls24_1569575543.m3u8",
                        "facebook_posts": [],
                        "buzz_id": null,
                        "created_at": 1569573694,
                        "id": 1143,
                        "is_shoppable": false,
                        "video_id": 91932,
                        "canonical_id": "compilation:1143",
                        "country": "US",
                        "draft_status": "published",
                        "keywords": null,
                        "name": "10 Power Salads For A Healthy Lifestyle",
                        "promotion": "full",
                        "show": [
                            {
                                "id": 34,
                                "name": "Goodful"
                            }
                        ]
                    },
                    {
                        "draft_status": "published",
                        "keywords": null,
                        "language": "eng",
                        "canonical_id": "compilation:1438",
                        "created_at": 1584513567,
                        "facebook_posts": [],
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/258777.jpg",
                        "video_id": 102112,
                        "approved_at": 1589378001,
                        "beauty_url": null,
                        "buzz_id": null,
                        "id": 1438,
                        "name": "Refreshing Summer Fruit Salads",
                        "promotion": "full",
                        "slug": "refreshing-summer-fruit-salads",
                        "video_url": "https://vid.tasty.co/output/162722/hls24_1584513638.m3u8",
                        "aspect_ratio": "1:1",
                        "country": "US",
                        "description": null,
                        "is_shoppable": false,
                        "show": [
                            {
                                "id": 34,
                                "name": "Goodful"
                            }
                        ]
                    }
                ],
                "instructions": [
                    {
                        "start_time": 0,
                        "end_time": 5040,
                        "temperature": null,
                        "appliance": null,
                        "id": 14827,
                        "display_text": "Combine all the ingredients above in a large bowl.",
                        "position": 1
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 14828,
                        "display_text": "Mix the dressing ingredients together and spread over fruit, mix well.",
                        "position": 2,
                        "start_time": 6000,
                        "end_time": 21000
                    },
                    {
                        "start_time": 22000,
                        "end_time": 25000,
                        "temperature": null,
                        "appliance": null,
                        "id": 14829,
                        "display_text": "Enjoy!",
                        "position": 3
                    }
                ],
                "draft_status": "published",
                "keywords": null,
                "tips_and_ratings_enabled": true,
                "_type": "recipe",
                "beauty_url": null,
                "show_id": 49,
                "video_id": 15797,
                "sections": [
                    {
                        "components": [
                            {
                                "id": 17685,
                                "raw_text": "2 oranges, peeled and halved",
                                "extra_comment": "peeled and halved",
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 447318,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 420,
                                    "name": "orange",
                                    "display_singular": "orange",
                                    "display_plural": "oranges",
                                    "created_at": 1494989685,
                                    "updated_at": 1509035262
                                }
                            },
                            {
                                "id": 17686,
                                "raw_text": "12 ounces strawberries, quartered",
                                "extra_comment": "quartered",
                                "position": 2,
                                "measurements": [
                                    {
                                        "id": 447316,
                                        "quantity": "12",
                                        "unit": {
                                            "name": "ounce",
                                            "abbreviation": "oz",
                                            "display_singular": "oz",
                                            "display_plural": "oz",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "quantity": "340",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        },
                                        "id": 447315
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1511888083,
                                    "id": 587,
                                    "name": "fresh strawberry ",
                                    "display_singular": "fresh strawberry ",
                                    "display_plural": "fresh strawberries ",
                                    "created_at": 1495411470
                                }
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 447317,
                                        "quantity": "2",
                                        "unit": {
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none",
                                            "name": ""
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 478,
                                    "name": "mango",
                                    "display_singular": "mango",
                                    "display_plural": "mangoes",
                                    "created_at": 1495134656,
                                    "updated_at": 1509035257
                                },
                                "id": 17687,
                                "raw_text": "2 mangoes, chopped",
                                "extra_comment": "chopped",
                                "position": 3
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 447320,
                                        "quantity": "4",
                                        "unit": {
                                            "name": "",
                                            "abbreviation": "",
                                            "display_singular": "",
                                            "display_plural": "",
                                            "system": "none"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "id": 671,
                                    "name": "kiwi",
                                    "display_singular": "kiwi",
                                    "display_plural": "kiwis",
                                    "created_at": 1495585919,
                                    "updated_at": 1509035242
                                },
                                "id": 17688,
                                "raw_text": "4 kiwis, peeled and chopped or sliced",
                                "extra_comment": "peeled and chopped, or sliced",
                                "position": 4
                            }
                        ],
                        "name": null,
                        "position": 1
                    },
                    {
                        "name": "Dressing",
                        "position": 2,
                        "components": [
                            {
                                "extra_comment": "",
                                "position": 6,
                                "measurements": [
                                    {
                                        "id": 447319,
                                        "quantity": "3",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1494878288,
                                    "updated_at": 1509035269,
                                    "id": 330,
                                    "name": "lime juice",
                                    "display_singular": "lime juice",
                                    "display_plural": "lime juices"
                                },
                                "id": 17690,
                                "raw_text": "3 tablespoons lime juice"
                            },
                            {
                                "raw_text": "1 tablespoon maple syrup",
                                "extra_comment": "",
                                "position": 7,
                                "measurements": [
                                    {
                                        "id": 447321,
                                        "quantity": "1",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035267,
                                    "id": 359,
                                    "name": "maple syrup",
                                    "display_singular": "maple syrup",
                                    "display_plural": "maple syrups",
                                    "created_at": 1494966352
                                },
                                "id": 17691
                            }
                        ]
                    }
                ],
                "canonical_id": "recipe:1719",
                "_id": 1719,
                "description": null,
                "servings_noun_plural": "servings",
                "servings_noun_singular": "serving",
                "yields": "Serves 4-5",
                "total_time_tier": {
                    "display_tier": "Under 30 minutes",
                    "tier": "under_30_minutes"
                },
                "credits": [
                    {
                        "type": "internal",
                        "name": "Merle O'Neal"
                    }
                ],
                "buzz_id": null,
                "country": "ZZ",
                "id": 1719,
                "is_shoppable": true,
                "show": {
                    "name": "Tasty: Tasty Vegetarian",
                    "id": 49
                },
                "tags": [
                    {
                        "id": 64466,
                        "name": "healthy",
                        "display_name": "Healthy",
                        "type": "dietary"
                    },
                    {
                        "id": 64469,
                        "name": "vegetarian",
                        "display_name": "Vegetarian",
                        "type": "dietary"
                    },
                    {
                        "id": 64468,
                        "name": "vegan",
                        "display_name": "Vegan",
                        "type": "dietary"
                    },
                    {
                        "id": 64463,
                        "name": "dairy_free",
                        "display_name": "Dairy-Free",
                        "type": "dietary"
                    },
                    {
                        "id": 64488,
                        "name": "kid_friendly",
                        "display_name": "Kid-Friendly",
                        "type": "dietary"
                    },
                    {
                        "id": 64471,
                        "name": "easy",
                        "display_name": "Easy",
                        "type": "difficulty"
                    },
                    {
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes",
                        "type": "difficulty"
                    },
                    {
                        "name": "snacks",
                        "display_name": "Snacks",
                        "type": "meal",
                        "id": 64491
                    },
                    {
                        "id": 64484,
                        "name": "brunch",
                        "display_name": "Brunch",
                        "type": "occasion"
                    },
                    {
                        "id": 64504,
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "occasion"
                    },
                    {
                        "id": 64465,
                        "name": "gluten_free",
                        "display_name": "Gluten-Free",
                        "type": "dietary"
                    },
                    {
                        "type": "meal",
                        "id": 64490,
                        "name": "sides",
                        "display_name": "Sides"
                    },
                    {
                        "id": 1280503,
                        "name": "cutting_board",
                        "display_name": "Cutting Board",
                        "type": "equipment"
                    },
                    {
                        "display_name": "Chef's Knife",
                        "type": "equipment",
                        "id": 1280501,
                        "name": "chefs_knife"
                    },
                    {
                        "name": "measuring_spoons",
                        "display_name": "Measuring Spoons",
                        "type": "equipment",
                        "id": 1280508
                    },
                    {
                        "id": 1247794,
                        "name": "wooden_spoon",
                        "display_name": "Wooden Spoon",
                        "type": "equipment"
                    },
                    {
                        "id": 1247785,
                        "name": "pyrex",
                        "display_name": "Pyrex",
                        "type": "equipment"
                    },
                    {
                        "id": 1280510,
                        "name": "mixing_bowl",
                        "display_name": "Mixing Bowl",
                        "type": "equipment"
                    }
                ],
                "user_ratings": {
                    "count_positive": 141,
                    "count_negative": 6,
                    "score": 0.959184
                },
                "inspired_by_url": null,
                "is_one_top": false,
                "slug": "tropical-fruit-salad",
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/f389b71a91e3476aa3b8e1d1a1c96039/Tasty_-_Facebook_-_1080x1080_3.jpg",
                "updated_at": 1560184059,
                "brand": null,
                "_index": "recipes-20200519201145",
                "approved_at": 1500926350,
                "cook_time_minutes": null,
                "num_servings": 4,
                "seo_title": null,
                "facebook_posts": [],
                "renditions": [
                    {
                        "height": 720,
                        "width": 720,
                        "bit_rate": 7277,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "aspect": "square",
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_1280X720/1492801391_00001.png",
                        "duration": 78554,
                        "file_size": 71449818,
                        "content_type": "video/mp4",
                        "url": "https://vid.tasty.co/output/29837/mp4_1280X720/1492801391",
                        "name": "mp4_720x720"
                    },
                    {
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/1445289064805-h2exzu/1492801391_00001.png",
                        "height": 1080,
                        "duration": 78487,
                        "url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                        "maximum_bit_rate": 8246,
                        "minimum_bit_rate": 272,
                        "content_type": "application/vnd.apple.mpegurl",
                        "aspect": "square",
                        "container": "mp4",
                        "width": 1080,
                        "file_size": null,
                        "bit_rate": null,
                        "name": "low"
                    },
                    {
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_640x640/1492801391_00001.png",
                        "name": "mp4_640x640",
                        "width": 640,
                        "duration": 78554,
                        "file_size": 71259012,
                        "bit_rate": 7258,
                        "content_type": "video/mp4",
                        "url": "https://vid.tasty.co/output/29837/mp4_640x640/1492801391",
                        "height": 640,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "aspect": "square",
                        "container": "mp4"
                    },
                    {
                        "aspect": "square",
                        "url": "https://vid.tasty.co/output/29837/mp4_720x1280/1492801391",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_720x1280/1492801391_00001.png",
                        "width": 720,
                        "duration": 78554,
                        "bit_rate": 7295,
                        "minimum_bit_rate": null,
                        "container": "mp4",
                        "name": "mp4_720x720",
                        "height": 720,
                        "file_size": 71629367,
                        "maximum_bit_rate": null,
                        "content_type": "video/mp4"
                    }
                ],
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/7759bbe3ad534174968598a01f740a25/Tasty_-_Facebook_-_1080x1080_3.mp4",
                "video_ad_content": "none",
                "aspect_ratio": "1:1",
                "brand_id": null,
                "created_at": 1499830592,
                "name": "Tropical Fruit Salad",
                "prep_time_minutes": null,
                "video_url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                "_op_type": "index"
            },
            {
                "updated_at": 1560184056,
                "video_url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                "canonical_id": "recipe:1721",
                "brand_id": null,
                "inspired_by_url": null,
                "is_shoppable": true,
                "slug": "mango-melon-fruit-salad",
                "total_time_minutes": null,
                "_id": 1721,
                "tags": [
                    {
                        "id": 64488,
                        "name": "kid_friendly",
                        "display_name": "Kid-Friendly",
                        "type": "dietary"
                    },
                    {
                        "id": 64468,
                        "name": "vegan",
                        "display_name": "Vegan",
                        "type": "dietary"
                    },
                    {
                        "display_name": "Vegetarian",
                        "type": "dietary",
                        "id": 64469,
                        "name": "vegetarian"
                    },
                    {
                        "display_name": "Healthy",
                        "type": "dietary",
                        "id": 64466,
                        "name": "healthy"
                    },
                    {
                        "id": 64463,
                        "name": "dairy_free",
                        "display_name": "Dairy-Free",
                        "type": "dietary"
                    },
                    {
                        "id": 64471,
                        "name": "easy",
                        "display_name": "Easy",
                        "type": "difficulty"
                    },
                    {
                        "type": "difficulty",
                        "id": 64472,
                        "name": "under_30_minutes",
                        "display_name": "Under 30 Minutes"
                    },
                    {
                        "id": 64485,
                        "name": "desserts",
                        "display_name": "Desserts",
                        "type": "meal"
                    },
                    {
                        "name": "bbq",
                        "display_name": "BBQ",
                        "type": "occasion",
                        "id": 64504
                    },
                    {
                        "id": 64484,
                        "name": "brunch",
                        "display_name": "Brunch",
                        "type": "occasion"
                    },
                    {
                        "id": 64465,
                        "name": "gluten_free",
                        "display_name": "Gluten-Free",
                        "type": "dietary"
                    },
                    {
                        "id": 64483,
                        "name": "breakfast",
                        "display_name": "Breakfast",
                        "type": "meal"
                    },
                    {
                        "id": 64491,
                        "name": "snacks",
                        "display_name": "Snacks",
                        "type": "meal"
                    },
                    {
                        "name": "wooden_spoon",
                        "display_name": "Wooden Spoon",
                        "type": "equipment",
                        "id": 1247794
                    },
                    {
                        "id": 1247785,
                        "name": "pyrex",
                        "display_name": "Pyrex",
                        "type": "equipment"
                    },
                    {
                        "id": 1280503,
                        "name": "cutting_board",
                        "display_name": "Cutting Board",
                        "type": "equipment"
                    },
                    {
                        "id": 1280501,
                        "name": "chefs_knife",
                        "display_name": "Chef's Knife",
                        "type": "equipment"
                    }
                ],
                "credits": [
                    {
                        "name": "Merle O'Neal",
                        "type": "internal"
                    }
                ],
                "nutrition": {
                    "carbohydrates": 51,
                    "fat": 1,
                    "protein": 3,
                    "sugar": 45,
                    "fiber": 5,
                    "updated_at": "2019-05-29T16:00:00+02:00",
                    "calories": 204
                },
                "approved_at": 1500926460,
                "cook_time_minutes": null,
                "keywords": null,
                "prep_time_minutes": null,
                "tips_and_ratings_enabled": true,
                "_index": "recipes-20200519201145",
                "_type": "recipe",
                "language": "eng",
                "name": "Mango Melon Fruit Salad",
                "show_id": 49,
                "total_time_tier": {
                    "tier": "under_30_minutes",
                    "display_tier": "Under 30 minutes"
                },
                "_op_type": "index",
                "servings_noun_singular": "serving",
                "yields": "Serves 4-5",
                "video_id": 15797,
                "nutrition_visibility": "auto",
                "brand": null,
                "aspect_ratio": "1:1",
                "buzz_id": null,
                "created_at": 1499830593,
                "description": null,
                "servings_noun_plural": "servings",
                "instructions": [
                    {
                        "start_time": 39390,
                        "end_time": 44000,
                        "temperature": null,
                        "appliance": null,
                        "id": 14833,
                        "display_text": "Combine all the ingredients above in a large bowl.",
                        "position": 1
                    },
                    {
                        "end_time": 50000,
                        "temperature": null,
                        "appliance": null,
                        "id": 14834,
                        "display_text": "Mix the dressing ingredients together and spread over fruit, mix well.",
                        "position": 2,
                        "start_time": 45000
                    },
                    {
                        "temperature": null,
                        "appliance": null,
                        "id": 14835,
                        "display_text": "Enjoy!",
                        "position": 3,
                        "start_time": 51000,
                        "end_time": 54000
                    }
                ],
                "show": {
                    "id": 49,
                    "name": "Tasty: Tasty Vegetarian"
                },
                "facebook_posts": [],
                "num_servings": 4,
                "video_ad_content": "none",
                "renditions": [
                    {
                        "aspect": "square",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_1280X720/1492801391_00001.png",
                        "height": 720,
                        "file_size": 71449818,
                        "content_type": "video/mp4",
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "container": "mp4",
                        "url": "https://vid.tasty.co/output/29837/mp4_1280X720/1492801391",
                        "name": "mp4_720x720",
                        "width": 720,
                        "duration": 78554,
                        "bit_rate": 7277
                    },
                    {
                        "height": 1080,
                        "width": 1080,
                        "duration": 78487,
                        "file_size": null,
                        "bit_rate": null,
                        "maximum_bit_rate": 8246,
                        "container": "mp4",
                        "name": "low",
                        "minimum_bit_rate": 272,
                        "content_type": "application/vnd.apple.mpegurl",
                        "aspect": "square",
                        "url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/1445289064805-h2exzu/1492801391_00001.png"
                    },
                    {
                        "file_size": 71259012,
                        "bit_rate": 7258,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "content_type": "video/mp4",
                        "aspect": "square",
                        "height": 640,
                        "width": 640,
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_640x640/1492801391_00001.png",
                        "name": "mp4_640x640",
                        "duration": 78554,
                        "url": "https://vid.tasty.co/output/29837/mp4_640x640/1492801391"
                    },
                    {
                        "content_type": "video/mp4",
                        "url": "https://vid.tasty.co/output/29837/mp4_720x1280/1492801391",
                        "height": 720,
                        "width": 720,
                        "duration": 78554,
                        "bit_rate": 7295,
                        "maximum_bit_rate": null,
                        "minimum_bit_rate": null,
                        "name": "mp4_720x720",
                        "file_size": 71629367,
                        "aspect": "square",
                        "container": "mp4",
                        "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/29837/mp4_720x1280/1492801391_00001.png"
                    }
                ],
                "draft_status": "published",
                "is_one_top": false,
                "promotion": "full",
                "compilations": [
                    {
                        "buzz_id": null,
                        "keywords": null,
                        "slug": "fruit-salad-4-ways",
                        "facebook_posts": [],
                        "aspect_ratio": "1:1",
                        "language": "eng",
                        "name": "Fruit Salad 4 Ways",
                        "promotion": "full",
                        "show": [
                            {
                                "id": 49,
                                "name": "Tasty: Tasty Vegetarian"
                            }
                        ],
                        "approved_at": 1500926543,
                        "country": "ZZ",
                        "created_at": 1499830593,
                        "description": null,
                        "video_url": "https://vid.tasty.co/output/29837/low_1492801391.m3u8",
                        "canonical_id": "compilation:109",
                        "beauty_url": null,
                        "draft_status": "published",
                        "id": 109,
                        "is_shoppable": false,
                        "thumbnail_url": "https://img.buzzfeed.com/video-api-prod/assets/a21c99c4ea144b91953b4d02e0cb03e7/FB_Thumb.jpg",
                        "video_id": 15797
                    },
                    {
                        "language": "eng",
                        "name": "Refreshing Summer Fruit Salads",
                        "video_id": 102112,
                        "show": [
                            {
                                "id": 34,
                                "name": "Goodful"
                            }
                        ],
                        "country": "US",
                        "is_shoppable": false,
                        "approved_at": 1589378001,
                        "created_at": 1584513567,
                        "description": null,
                        "draft_status": "published",
                        "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/258777.jpg",
                        "canonical_id": "compilation:1438",
                        "facebook_posts": [],
                        "beauty_url": null,
                        "buzz_id": null,
                        "id": 1438,
                        "keywords": null,
                        "promotion": "full",
                        "slug": "refreshing-summer-fruit-salads",
                        "video_url": "https://vid.tasty.co/output/162722/hls24_1584513638.m3u8",
                        "aspect_ratio": "1:1"
                    }
                ],
                "original_video_url": "https://s3.amazonaws.com/video-api-prod/assets/7759bbe3ad534174968598a01f740a25/Tasty_-_Facebook_-_1080x1080_3.mp4",
                "sections": [
                    {
                        "position": 1,
                        "components": [
                            {
                                "id": 17697,
                                "raw_text": "2 cups pineapple, cubed",
                                "extra_comment": "cubed",
                                "position": 1,
                                "measurements": [
                                    {
                                        "id": 261120,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "id": 261118,
                                        "quantity": "490",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "name": "pineapple",
                                    "display_singular": "pineapple",
                                    "display_plural": "pineapples",
                                    "created_at": 1495585901,
                                    "updated_at": 1509035242,
                                    "id": 670
                                }
                            },
                            {
                                "id": 17698,
                                "raw_text": "2 cups cantaloupe, cubed",
                                "extra_comment": "cubed",
                                "position": 2,
                                "measurements": [
                                    {
                                        "unit": {
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial"
                                        },
                                        "id": 261115,
                                        "quantity": "2"
                                    },
                                    {
                                        "id": 261114,
                                        "quantity": "320",
                                        "unit": {
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "cantaloupe",
                                    "display_plural": "cantaloupes",
                                    "created_at": 1501703159,
                                    "updated_at": 1509035107,
                                    "id": 2818,
                                    "name": "cantaloupe"
                                }
                            },
                            {
                                "id": 17699,
                                "raw_text": "2 cups honeydew, cubed",
                                "extra_comment": "cubed",
                                "position": 3,
                                "measurements": [
                                    {
                                        "id": 261117,
                                        "quantity": "2",
                                        "unit": {
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial",
                                            "name": "cup"
                                        }
                                    },
                                    {
                                        "id": 261116,
                                        "quantity": "320",
                                        "unit": {
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric",
                                            "name": "gram",
                                            "abbreviation": "g"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1502371930,
                                    "updated_at": 1509035104,
                                    "id": 2855,
                                    "name": "honeydew melon",
                                    "display_singular": "honeydew melon",
                                    "display_plural": "honeydew melons"
                                }
                            },
                            {
                                "measurements": [
                                    {
                                        "id": 261112,
                                        "quantity": "2",
                                        "unit": {
                                            "name": "cup",
                                            "abbreviation": "c",
                                            "display_singular": "cup",
                                            "display_plural": "cups",
                                            "system": "imperial"
                                        }
                                    },
                                    {
                                        "id": 261111,
                                        "quantity": "330",
                                        "unit": {
                                            "name": "gram",
                                            "abbreviation": "g",
                                            "display_singular": "g",
                                            "display_plural": "g",
                                            "system": "metric"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "created_at": 1495134656,
                                    "updated_at": 1509035257,
                                    "id": 478,
                                    "name": "mango",
                                    "display_singular": "mango",
                                    "display_plural": "mangoes"
                                },
                                "id": 17700,
                                "raw_text": "2 cups mangoes, cubed",
                                "extra_comment": "cubed",
                                "position": 4
                            }
                        ],
                        "name": null
                    },
                    {
                        "components": [
                            {
                                "measurements": [
                                    {
                                        "id": 261113,
                                        "quantity": "3",
                                        "unit": {
                                            "name": "tablespoon",
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "updated_at": 1509035269,
                                    "id": 330,
                                    "name": "lime juice",
                                    "display_singular": "lime juice",
                                    "display_plural": "lime juices",
                                    "created_at": 1494878288
                                },
                                "id": 19667,
                                "raw_text": "n/a",
                                "extra_comment": "",
                                "position": 6
                            },
                            {
                                "id": 19668,
                                "raw_text": "n/a",
                                "extra_comment": "",
                                "position": 7,
                                "measurements": [
                                    {
                                        "id": 261119,
                                        "quantity": "1",
                                        "unit": {
                                            "abbreviation": "tbsp",
                                            "display_singular": "tablespoon",
                                            "display_plural": "tablespoons",
                                            "system": "imperial",
                                            "name": "tablespoon"
                                        }
                                    }
                                ],
                                "ingredient": {
                                    "display_singular": "maple syrup",
                                    "display_plural": "maple syrups",
                                    "created_at": 1494966352,
                                    "updated_at": 1509035267,
                                    "id": 359,
                                    "name": "maple syrup"
                                }
                            }
                        ],
                        "name": "Dressing",
                        "position": 2
                    }
                ],
                "user_ratings": {
                    "count_negative": 8,
                    "score": 0.84,
                    "count_positive": 42
                },
                "beauty_url": null,
                "country": "ZZ",
                "id": 1721,
                "seo_title": null,
                "thumbnail_url": "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/c531fd01ff5b4767a8f22fe05e77cd0a/Tasty_-_Facebook_-_1080x1080_3.jpg"
            }
        ],
        "country": "US",
        "tags": [
            {
                "display_name": "Easy",
                "type": "difficulty",
                "id": 64471,
                "name": "easy"
            },
            {
                "id": 64466,
                "name": "healthy",
                "display_name": "Healthy",
                "type": "dietary"
            },
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "id": 64469,
                "name": "vegetarian",
                "display_name": "Vegetarian",
                "type": "dietary"
            }
        ],
        "slug": "refreshing-summer-fruit-salads",
        "renditions": [
            {
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "square",
                "container": "mp4",
                "name": "mp4_720x720",
                "duration": 198113,
                "file_size": 51490435,
                "maximum_bit_rate": null,
                "url": "https://vid.tasty.co/output/162722/square_720/1584513638",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/square_720/1584513638_00001.png",
                "height": 720,
                "width": 720,
                "bit_rate": 2080
            },
            {
                "content_type": "video/mp4",
                "name": "mp4_720x720",
                "height": 720,
                "duration": 198113,
                "file_size": 51490435,
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/square_720/1584513638_00001.png",
                "width": 720,
                "bit_rate": 2080,
                "aspect": "square",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/162722/square_720/1584513638"
            },
            {
                "file_size": 17897639,
                "bit_rate": 723,
                "aspect": "square",
                "container": "mp4",
                "url": "https://vid.tasty.co/output/162722/square_320/1584513638",
                "height": 320,
                "width": 320,
                "duration": 198113,
                "name": "mp4_320x320",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/square_320/1584513638_00001.png",
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4"
            },
            {
                "width": 320,
                "minimum_bit_rate": null,
                "container": "mp4",
                "aspect": "square",
                "url": "https://vid.tasty.co/output/162722/square_320/1584513638",
                "height": 320,
                "duration": 198113,
                "file_size": 17897639,
                "bit_rate": 723,
                "maximum_bit_rate": null,
                "content_type": "video/mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/square_320/1584513638_00001.png",
                "name": "mp4_320x320"
            },
            {
                "height": 720,
                "file_size": 51465999,
                "maximum_bit_rate": null,
                "url": "https://vid.tasty.co/output/162722/landscape_720/1584513638",
                "name": "mp4_720x720",
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/landscape_720/1584513638_00001.png",
                "width": 720,
                "duration": 198113,
                "bit_rate": 2079,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "aspect": "square"
            },
            {
                "file_size": 51465999,
                "maximum_bit_rate": null,
                "url": "https://vid.tasty.co/output/162722/landscape_720/1584513638",
                "name": "mp4_720x720",
                "height": 720,
                "width": 720,
                "duration": 198113,
                "aspect": "square",
                "container": "mp4",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/landscape_720/1584513638_00001.png",
                "bit_rate": 2079,
                "minimum_bit_rate": null,
                "content_type": "video/mp4"
            },
            {
                "file_size": 29872482,
                "aspect": "square",
                "maximum_bit_rate": null,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4",
                "height": 480,
                "width": 480,
                "duration": 198113,
                "bit_rate": 1207,
                "url": "https://vid.tasty.co/output/162722/landscape_480/1584513638",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/landscape_480/1584513638_00001.png",
                "name": "mp4_480x480"
            },
            {
                "maximum_bit_rate": null,
                "aspect": "square",
                "url": "https://vid.tasty.co/output/162722/landscape_480/1584513638",
                "height": 480,
                "width": 480,
                "duration": 198113,
                "file_size": 29872482,
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/landscape_480/1584513638_00001.png",
                "name": "mp4_480x480",
                "bit_rate": 1207,
                "minimum_bit_rate": null,
                "content_type": "video/mp4",
                "container": "mp4"
            },
            {
                "file_size": null,
                "maximum_bit_rate": 3296,
                "minimum_bit_rate": 275,
                "content_type": "application/vnd.apple.mpegurl",
                "aspect": "square",
                "container": "ts",
                "height": 1080,
                "duration": 198115,
                "url": "https://vid.tasty.co/output/162722/hls24_1584513638.m3u8",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/1445289064805-h2exzu/1584513638_00001.png",
                "name": "low",
                "width": 1080,
                "bit_rate": null
            },
            {
                "file_size": null,
                "content_type": "application/vnd.apple.mpegurl",
                "aspect": "square",
                "container": "ts",
                "poster_url": "https://img.buzzfeed.com/video-transcoder-prod/output/162722/1445289064805-h2exzu/1584513638_00001.png",
                "name": "low",
                "height": 1080,
                "duration": 198115,
                "maximum_bit_rate": 3296,
                "minimum_bit_rate": 275,
                "url": "https://vid.tasty.co/output/162722/hls24_1584513638.m3u8",
                "width": 1080,
                "bit_rate": null
            }
        ],
        "canonical_id": "compilation:1438",
        "approved_at": 1589378001,
        "draft_status": "published",
        "id": 1438,
        "show": {
            "id": 34,
            "name": "Goodful"
        },
        "beauty_url": null,
        "is_shoppable": false,
        "keywords": null,
        "name": "Refreshing Summer Fruit Salads",
        "description": null,
        "language": "eng",
        "promotion": "full",
        "video_url": "https://vid.tasty.co/output/162722/hls24_1584513638.m3u8",
        "credits": [
            {
                "name": "Madhumita Kannan",
                "type": "internal"
            }
        ],
        "aspect_ratio": "1:1",
        "buzz_id": null,
        "updated_at": 1589378001
    },
    {
        "beauty_url": null,
        "updated_at": 1589377362,
        "video_id": null,
        "sections": [
            {
                "components": [
                    {
                        "id": 67806,
                        "raw_text": "1 large carrot",
                        "extra_comment": "",
                        "position": 2,
                        "measurements": [
                            {
                                "unit": {
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": ""
                                },
                                "id": 565161,
                                "quantity": "1"
                            }
                        ],
                        "ingredient": {
                            "id": 755,
                            "name": "large carrot",
                            "display_singular": "large carrot",
                            "display_plural": "large carrots",
                            "created_at": 1495688206,
                            "updated_at": 1509035236
                        }
                    },
                    {
                        "ingredient": {
                            "display_plural": "large cucumbers",
                            "created_at": 1497125172,
                            "updated_at": 1509035164,
                            "id": 1808,
                            "name": "large cucumber",
                            "display_singular": "large cucumber"
                        },
                        "id": 67807,
                        "raw_text": "1 large cucumber",
                        "extra_comment": "",
                        "position": 3,
                        "measurements": [
                            {
                                "id": 565160,
                                "quantity": "1",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                }
                            }
                        ]
                    },
                    {
                        "ingredient": {
                            "id": 227,
                            "name": "red bell pepper",
                            "display_singular": "red bell pepper",
                            "display_plural": "red bell peppers",
                            "created_at": 1494292131,
                            "updated_at": 1509035277
                        },
                        "id": 67808,
                        "raw_text": "1 red bell pepper",
                        "extra_comment": "",
                        "position": 4,
                        "measurements": [
                            {
                                "quantity": "1",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                },
                                "id": 565163
                            }
                        ]
                    },
                    {
                        "position": 5,
                        "measurements": [
                            {
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                },
                                "id": 565166,
                                "quantity": "10"
                            },
                            {
                                "quantity": "¼",
                                "unit": {
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup"
                                },
                                "id": 565164
                            }
                        ],
                        "ingredient": {
                            "display_singular": "fresh basil leaf",
                            "display_plural": "fresh basil leaves",
                            "created_at": 1527025774,
                            "updated_at": 1527025774,
                            "id": 4158,
                            "name": "fresh basil leaves"
                        },
                        "id": 67809,
                        "raw_text": "¼ cup fresh basil leaves",
                        "extra_comment": ""
                    },
                    {
                        "measurements": [
                            {
                                "id": 565165,
                                "quantity": "10",
                                "unit": {
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram"
                                }
                            },
                            {
                                "unit": {
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup",
                                    "abbreviation": "c"
                                },
                                "id": 565162,
                                "quantity": "¼"
                            }
                        ],
                        "ingredient": {
                            "name": "fresh mint leaf",
                            "display_singular": "fresh mint leaf",
                            "display_plural": "fresh mint leaves",
                            "created_at": 1500886630,
                            "updated_at": 1509035122,
                            "id": 2546
                        },
                        "id": 67810,
                        "raw_text": "¼ cup fresh mint leaves",
                        "extra_comment": "",
                        "position": 6
                    },
                    {
                        "extra_comment": "",
                        "position": 7,
                        "measurements": [
                            {
                                "id": 565169,
                                "quantity": "10",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            },
                            {
                                "id": 565167,
                                "quantity": "¼",
                                "unit": {
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 4163,
                            "name": "fresh cilantro leaves",
                            "display_singular": "fresh cilantro leaf",
                            "display_plural": "fresh cilantro leaves",
                            "created_at": 1527199111,
                            "updated_at": 1527199111
                        },
                        "id": 67811,
                        "raw_text": "¼ cup fresh cilantro leaves"
                    },
                    {
                        "measurements": [
                            {
                                "id": 565172,
                                "quantity": "1",
                                "unit": {
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": ""
                                }
                            }
                        ],
                        "ingredient": {
                            "updated_at": 1509035288,
                            "id": 18,
                            "name": "jalapeño",
                            "display_singular": "jalapeño",
                            "display_plural": "jalapeñoes",
                            "created_at": 1493314613
                        },
                        "id": 67812,
                        "raw_text": "1 jalapeño",
                        "extra_comment": "",
                        "position": 8
                    },
                    {
                        "id": 67813,
                        "raw_text": "7 ounces rice vermicelli noodles",
                        "extra_comment": "",
                        "position": 9,
                        "measurements": [
                            {
                                "id": 565171,
                                "quantity": "200",
                                "unit": {
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric"
                                }
                            },
                            {
                                "id": 565168,
                                "quantity": "7",
                                "unit": {
                                    "name": "ounce",
                                    "abbreviation": "oz",
                                    "display_singular": "oz",
                                    "display_plural": "oz",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "fine rice vermicelli noodle",
                            "display_plural": "fine rice vermicelli noodles",
                            "created_at": 1561739733,
                            "updated_at": 1561739733,
                            "id": 5547,
                            "name": "fine rice vermicelli noodles"
                        }
                    },
                    {
                        "extra_comment": "",
                        "position": 10,
                        "measurements": [
                            {
                                "quantity": "1",
                                "unit": {
                                    "display_plural": "",
                                    "system": "none",
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": ""
                                },
                                "id": 565170
                            }
                        ],
                        "ingredient": {
                            "name": "avocado",
                            "display_singular": "avocado",
                            "display_plural": "avocados",
                            "created_at": 1496185911,
                            "updated_at": 1509035215,
                            "id": 1005
                        },
                        "id": 67814,
                        "raw_text": "1 avocado"
                    },
                    {
                        "measurements": [
                            {
                                "id": 565174,
                                "quantity": "100",
                                "unit": {
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram",
                                    "abbreviation": "g",
                                    "display_singular": "g"
                                }
                            },
                            {
                                "id": 565173,
                                "quantity": "½",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1495762588,
                            "updated_at": 1509035230,
                            "id": 820,
                            "name": "bean sprout",
                            "display_singular": "bean sprout",
                            "display_plural": "bean sprouts"
                        },
                        "id": 67815,
                        "raw_text": "½ cup bean sprouts",
                        "extra_comment": "",
                        "position": 11
                    }
                ],
                "name": "Spring Roll Bowl",
                "position": 1
            },
            {
                "components": [
                    {
                        "id": 67817,
                        "raw_text": "Canola or vegetable oil, for frying",
                        "extra_comment": "or vegetable oil for frying",
                        "position": 13,
                        "measurements": [
                            {
                                "id": 565175,
                                "quantity": "0",
                                "unit": {
                                    "name": "",
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "canola oils",
                            "created_at": 1495764700,
                            "updated_at": 1509035230,
                            "id": 825,
                            "name": "canola oil",
                            "display_singular": "canola oil"
                        }
                    },
                    {
                        "ingredient": {
                            "id": 6422,
                            "name": "round rice paper wrappers",
                            "display_singular": "round rice paper wrapper",
                            "display_plural": "round rice paper wrappers",
                            "created_at": 1589376934,
                            "updated_at": 1589376934
                        },
                        "id": 67818,
                        "raw_text": "4 6-inch round rice paper wrappers",
                        "extra_comment": "6 in (15 cm) wrappers",
                        "position": 14,
                        "measurements": [
                            {
                                "unit": {
                                    "abbreviation": "",
                                    "display_singular": "",
                                    "display_plural": "",
                                    "system": "none",
                                    "name": ""
                                },
                                "id": 565179,
                                "quantity": "4"
                            }
                        ]
                    }
                ],
                "name": "Rice Paper Chips",
                "position": 2
            },
            {
                "name": "Sauce",
                "position": 3,
                "components": [
                    {
                        "position": 16,
                        "measurements": [
                            {
                                "id": 565176,
                                "quantity": "4",
                                "unit": {
                                    "display_plural": "cloves",
                                    "system": "none",
                                    "name": "clove",
                                    "abbreviation": "clove",
                                    "display_singular": "clove"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1493744766,
                            "updated_at": 1509035285,
                            "id": 95,
                            "name": "garlic",
                            "display_singular": "garlic",
                            "display_plural": "garlics"
                        },
                        "id": 67820,
                        "raw_text": "4 cloves garlic",
                        "extra_comment": ""
                    },
                    {
                        "measurements": [
                            {
                                "id": 565178,
                                "quantity": "2",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "created_at": 1495141670,
                            "updated_at": 1509035256,
                            "id": 486,
                            "name": "rice vinegar",
                            "display_singular": "rice vinegar",
                            "display_plural": "rice vinegars"
                        },
                        "id": 67821,
                        "raw_text": "2 tablespoons rice vinegar",
                        "extra_comment": "",
                        "position": 17
                    },
                    {
                        "id": 67822,
                        "raw_text": "2 tablespoons sesame oil",
                        "extra_comment": "",
                        "position": 18,
                        "measurements": [
                            {
                                "id": 565177,
                                "quantity": "2",
                                "unit": {
                                    "display_plural": "tablespoons",
                                    "system": "imperial",
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 443,
                            "name": "sesame oil",
                            "display_singular": "sesame oil",
                            "display_plural": "sesame oils",
                            "created_at": 1495072290,
                            "updated_at": 1509035260
                        }
                    },
                    {
                        "extra_comment": "",
                        "position": 19,
                        "measurements": [
                            {
                                "id": 565188,
                                "quantity": "50",
                                "unit": {
                                    "abbreviation": "g",
                                    "display_singular": "g",
                                    "display_plural": "g",
                                    "system": "metric",
                                    "name": "gram"
                                }
                            },
                            {
                                "id": 565187,
                                "quantity": "¼",
                                "unit": {
                                    "display_plural": "cups",
                                    "system": "imperial",
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "brown sugar",
                            "display_singular": "brown sugar",
                            "display_plural": "brown sugars",
                            "created_at": 1493307081,
                            "updated_at": 1509035289,
                            "id": 6
                        },
                        "id": 67823,
                        "raw_text": "¼ cup brown sugar"
                    },
                    {
                        "position": 20,
                        "measurements": [
                            {
                                "quantity": "60",
                                "unit": {
                                    "display_singular": "mL",
                                    "display_plural": "mL",
                                    "system": "metric",
                                    "name": "milliliter",
                                    "abbreviation": "mL"
                                },
                                "id": 565183
                            },
                            {
                                "id": 565181,
                                "quantity": "¼",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "id": 472,
                            "name": "warm water",
                            "display_singular": "warm water",
                            "display_plural": "warm waters",
                            "created_at": 1495132646,
                            "updated_at": 1509035257
                        },
                        "id": 67824,
                        "raw_text": "¼ cup warm water",
                        "extra_comment": ""
                    },
                    {
                        "position": 21,
                        "measurements": [
                            {
                                "id": 565184,
                                "quantity": "60",
                                "unit": {
                                    "name": "milliliter",
                                    "abbreviation": "mL",
                                    "display_singular": "mL",
                                    "display_plural": "mL",
                                    "system": "metric"
                                }
                            },
                            {
                                "id": 565182,
                                "quantity": "¼",
                                "unit": {
                                    "system": "imperial",
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_plural": "tamaris",
                            "created_at": 1531253457,
                            "updated_at": 1531253457,
                            "id": 4469,
                            "name": "tamari",
                            "display_singular": "tamari"
                        },
                        "id": 67825,
                        "raw_text": "¼ cup tamari",
                        "extra_comment": ""
                    },
                    {
                        "extra_comment": "",
                        "position": 22,
                        "measurements": [
                            {
                                "id": 565186,
                                "quantity": "80",
                                "unit": {
                                    "name": "milliliter",
                                    "abbreviation": "mL",
                                    "display_singular": "mL",
                                    "display_plural": "mL",
                                    "system": "metric"
                                }
                            },
                            {
                                "id": 565185,
                                "quantity": "⅓",
                                "unit": {
                                    "name": "cup",
                                    "abbreviation": "c",
                                    "display_singular": "cup",
                                    "display_plural": "cups",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "name": "lime juice",
                            "display_singular": "lime juice",
                            "display_plural": "lime juices",
                            "created_at": 1494878288,
                            "updated_at": 1509035269,
                            "id": 330
                        },
                        "id": 67826,
                        "raw_text": "⅓ cup lime juice"
                    },
                    {
                        "id": 67827,
                        "raw_text": "1 tablespoon chili sauce, such as Huy Fong Foods (optional)",
                        "extra_comment": "such as Huy Fong Foods (optional)",
                        "position": 23,
                        "measurements": [
                            {
                                "id": 565180,
                                "quantity": "1",
                                "unit": {
                                    "name": "tablespoon",
                                    "abbreviation": "tbsp",
                                    "display_singular": "tablespoon",
                                    "display_plural": "tablespoons",
                                    "system": "imperial"
                                }
                            }
                        ],
                        "ingredient": {
                            "display_singular": "chili sauce",
                            "display_plural": "chili sauces",
                            "created_at": 1500163736,
                            "updated_at": 1509035141,
                            "id": 2152,
                            "name": "chili sauce"
                        }
                    }
                ]
            }
        ],
        "is_one_top": false,
        "keywords": "",
        "total_time_tier": {
            "tier": "under_30_minutes",
            "display_tier": "Under 30 minutes"
        },
        "video_url": null,
        "show": {
            "id": 17,
            "name": "Tasty"
        },
        "servings_noun_singular": "serving",
        "slug": "vegan-spring-roll-in-a-bowl",
        "total_time_minutes": 22,
        "nutrition_visibility": "auto",
        "user_ratings": {
            "count_positive": 0,
            "count_negative": 0,
            "score": null
        },
        "canonical_id": "recipe:6172",
        "buzz_id": null,
        "cook_time_minutes": 2,
        "created_at": 1589302385,
        "is_shoppable": true,
        "prep_time_minutes": 20,
        "brand": null,
        "tags": [
            {
                "id": 1280501,
                "name": "chefs_knife",
                "display_name": "Chef's Knife",
                "type": "equipment"
            },
            {
                "id": 1280503,
                "name": "cutting_board",
                "display_name": "Cutting Board",
                "type": "equipment"
            },
            {
                "type": "dietary",
                "id": 64468,
                "name": "vegan",
                "display_name": "Vegan"
            },
            {
                "id": 1280508,
                "name": "measuring_spoons",
                "display_name": "Measuring Spoons",
                "type": "equipment"
            },
            {
                "id": 65410,
                "name": "fusion",
                "display_name": "Fusion",
                "type": "cuisine"
            },
            {
                "type": "cuisine",
                "id": 64444,
                "name": "american",
                "display_name": "American"
            },
            {
                "id": 65840,
                "name": "cast_iron_pan",
                "display_name": "Cast Iron Pan",
                "type": "appliance"
            },
            {
                "display_name": "Stove Top",
                "type": "appliance",
                "id": 65848,
                "name": "stove_top"
            },
            {
                "id": 64472,
                "name": "under_30_minutes",
                "display_name": "Under 30 Minutes",
                "type": "difficulty"
            },
            {
                "id": 64471,
                "name": "easy",
                "display_name": "Easy",
                "type": "difficulty"
            },
            {
                "id": 64469,
                "name": "vegetarian",
                "display_name": "Vegetarian",
                "type": "dietary"
            },
            {
                "id": 64486,
                "name": "dinner",
                "display_name": "Dinner",
                "type": "meal"
            },
            {
                "id": 64461,
                "name": "vietnamese",
                "display_name": "Vietnamese",
                "type": "cuisine"
            },
            {
                "display_name": "Asian Pacific American Heritage Month",
                "type": "holiday",
                "id": 3802077,
                "name": "asian_pacific_american_heritage_month"
            },
            {
                "id": 64489,
                "name": "lunch",
                "display_name": "Lunch",
                "type": "meal"
            },
            {
                "id": 1247789,
                "name": "strainer",
                "display_name": "Strainer",
                "type": "equipment"
            },
            {
                "id": 64493,
                "name": "deep_fry",
                "display_name": "Deep-Fry",
                "type": "method"
            }
        ],
        "video_ad_content": null,
        "renditions": [],
        "approved_at": 1589377459,
        "country": "US",
        "description": "Freshen up your daily routine with this vegan spring roll in a bowl. Full of vibrant vegetables and herbs, this dish will leave you smiling.",
        "draft_status": "published",
        "inspired_by_url": null,
        "seo_title": "",
        "tips_and_ratings_enabled": true,
        "yields": "Servings: 4",
        "compilations": [],
        "aspect_ratio": "16:9",
        "brand_id": null,
        "language": "eng",
        "servings_noun_plural": "servings",
        "show_id": 17,
        "original_video_url": null,
        "facebook_posts": [],
        "nutrition": {
            "calories": 453,
            "carbohydrates": 65,
            "fat": 19,
            "protein": 6,
            "sugar": 17,
            "fiber": 5,
            "updated_at": "2020-05-14T08:01:02+02:00"
        },
        "id": 6172,
        "name": "Vegan Spring Roll In A Bowl",
        "num_servings": 4,
        "promotion": "full",
        "thumbnail_url": "https://img.buzzfeed.com/tasty-app-user-assets-prod-us-east-1/recipes/4a2067db94ca4a0a8ecff27284739d26.jpeg",
        "instructions": [
            {
                "temperature": null,
                "appliance": null,
                "id": 55327,
                "display_text": "Prep the vegetables: Julienne the carrot, cucumber, and red bell pepper. Chop the basil, mint, and cilantro. Slice the jalapeño into rings.",
                "position": 1,
                "start_time": 0,
                "end_time": 0
            },
            {
                "appliance": null,
                "id": 55328,
                "display_text": "Make the rice paper chips: Heat 1 inch of vegetable oil in a large, high-sided skillet over medium heat until hot (test by inserting chopsticks into the oil; if bubbles form around them, the oil is hot enough).",
                "position": 2,
                "start_time": 0,
                "end_time": 0,
                "temperature": null
            },
            {
                "appliance": null,
                "id": 55329,
                "display_text": "Using scissors, cut the rice paper wrappers into quarters.",
                "position": 3,
                "start_time": 0,
                "end_time": 0,
                "temperature": null
            },
            {
                "display_text": "Working 1 or 2 at a time, gently drop the rice paper quarters into the hot oil and fry for just 1–2 seconds, until they are white and puffed. Use a slotted spoon to transfer to a paper towel-lined plate to drain. Repeat with the remaining rice paper wedges. Set aside until ready to use.",
                "position": 4,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null,
                "id": 55330
            },
            {
                "id": 55331,
                "display_text": "Make the sauce: Add the garlic, rice vinegar, sesame oil, brown sugar, warm water, tamari, lime juice, and chili sauce, if using, to a blender and blend until fully combined. Transfer to a bowl and set aside until ready to use. The sauce will keep in an airtight container in the refrigerator for up to 2 weeks.",
                "position": 5,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            },
            {
                "appliance": null,
                "id": 55332,
                "display_text": "Bring a medium pot of water to a rolling boil over medium-high heat. Add the rice noodles and stir to separate. Cook until barely tender, 2–3 minutes. Drain and rinse thoroughly with cool water to stop the cooking process and so the noodles don't stick together (toss a little bit of sesame oil if they start to stick).",
                "position": 6,
                "start_time": 0,
                "end_time": 0,
                "temperature": null
            },
            {
                "appliance": null,
                "id": 55333,
                "display_text": "Build the bowls: Thinly slice the avocado. Divide the bean sprouts and rice noodles among 4 bowls. Add some of the chopped herbs. Arrange the carrot, cucumber, bell pepper, jalapeño, avocado, and tofu, if using, on top. Drizzle each bowl with 3 tablespoons of the sauce. Serve with the rice paper chips and more sauce alongside.",
                "position": 7,
                "start_time": 0,
                "end_time": 0,
                "temperature": null
            },
            {
                "id": 55334,
                "display_text": "Enjoy!",
                "position": 8,
                "start_time": 0,
                "end_time": 0,
                "temperature": null,
                "appliance": null
            }
        ],
        "credits": [
            {
                "name": "Aleya Zenieris",
                "type": "internal"
            },
            {
                "name": "Camly Nguyen",
                "type": "internal"
            }
        ]
    }
]



export default List;