function searchRecipe(searchStr, data) {
    var string_to_search = searchStr.toUpperCase();
    var new_data = data.filter(function(item, index) {
        if(item.name.toUpperCase().includes(string_to_search)) {
            return item;
        }
    });
    return new_data;
}
function filterRecipe(values, data) {
    if(values.prep !== '' && values.cook !== '' && values.diff !== '') {
        var new_data = data.filter((item, index) => {
            console.log(item.prep_time);
            console.log(values.diff);
            if(item.prep_time === values.prep) {
                return item;
            }
            if(item.cook_time === values.cook) {
                return item;
            }
            if(item.difficulty === values.diff) {
                return item;
            }
        });
    return new_data;
    } else {
        return data;
    }
}

export {filterRecipe, searchRecipe};