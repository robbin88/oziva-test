import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { filterRecipe, searchRecipe } from "./services/filter";
// import Dialog from "./components/dialog";
import HeaderNav from './components/Header';
import './App.css';
import Home from './view/Home';

// class App extends React.Component {
//   state = {
//     text: '',
//     sideNav: true,
//     dialogStatus: false,
//     searchVisible: true,
//     showFilters: true,
//     filterValues: { prep: '', cook: '', diff: '' }
//   };
//   handleNewPage = (data) => {
//     localStorage.setItem('data', JSON.stringify(data));
//     this.props.history.push('/recipe/' + data.slug);
//   }
//   handleSearchBox = () => {
//     let new_status = this.state.searchVisible;
//     this.setState({
//       searchVisible: !new_status
//     });
//   }
//   handleSubmitSearch = (e, data) => {
//     let value = e.target.value;
//     this.setState({
//       text: value
//     }, () => {
//       var new_value = searchRecipe(value, data);
//       this.setState({
//         list: new_value
//       });
//     });
//   }
//   handleRecipeData = (data) => {
//     // let oldList = this.state.list;
//     // oldList.push(data);
//     let recipe = data;
//     recipe.sections[0]['components'] = recipe.ingredients;
//     let list = this.state.list;


//     list.push(recipe);
//     this.setState({
//       list: list,
//       dialogStatus: false
//     });
//   }
//   showFilters = () => {
//     let OldStatus = this.state.showFilters;
//     this.setState({
//       showFilters: !OldStatus
//     });
//   }
//   handleDiffLevel = (e) => {
//     let value = e.target.value;
//     let oldValues = this.state.filterValues;
//     oldValues.diff = value;
//     this.setState({
//       filterValues: oldValues
//     });
//   }
//   handlePrepTime = (e) => {
//     let value = e.target.value;
//     let oldValues = this.state.filterValues;
//     oldValues.prep = value;
//     this.setState({
//       filterValues: oldValues
//     });
//   }
//   handleCookTime = (e) => {
//     let value = e.target.value;
//     let oldValues = this.state.filterValues;
//     oldValues.cook = value;
//     this.setState({
//       filterValues: oldValues
//     });
//   }
//   applyFilter = () => {
//     let { filterValues, list } = this.state;
//     var new_list = filterRecipe(filterValues, list);

//     this.setState({
//       list: new_list
//     });
//   }
//   clearFilter = () => {
//     let values = this.state.filterValues;
//     values.prep = '';
//     values.cook = '';
//     values.diff = '';
//     this.setState({
//       filterValues: values
//     }, () => {
//       let options = [...document.getElementsByTagName('option')];

//       options.forEach((item, index) => {
//         if (item.value === '') {
//           ReactDOM.findDOMNode(item).setAttribute('selected', true);
//         }
//       });
//     });

//   }
//   handleRemoveItem = (index) => {
//     let { list } = this.state;
//     list.splice(index, 1);
//     this.setState({
//       list: list
//     });
//   }
//   handleSideNav = () => {
//     let { sideNav } = this.state;
//     this.setState({
//       sideNav: !sideNav
//     });
//   }
// }

const App = () => {
  const [showOverLay, setShowOverlay] = useState(false);
  return (
    <>
      <HeaderNav showOverLay={setShowOverlay}>
        <Home />
      </HeaderNav>
      {showOverLay ?
        <div className='overlay'>
        </div>
        : ''}
    </>
  );
}

export default App;
